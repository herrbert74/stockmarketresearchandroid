package com.babestudios.stockmarket;

import com.babestudios.stockmarket.network.converters.AdvancedGsonConverterFactory;
import com.babestudios.stockmarket.network.interfaces.IGetEarningsDatesService;
import com.babestudios.stockmarket.network.interfaces.IYahooIntraDayService;

import javax.inject.Named;
import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.mock.MockRetrofit;

@Module
public class TestApplicationModule {

	@Provides
	@Singleton
	@Named("YahooIntraDayRetrofit")
	MockRetrofit provideYahooIntraDayRetrofit() {
		return new MockRetrofit.Builder(new Retrofit.Builder().build())//
				.build();
	}

	@Provides
	@Singleton
	IYahooIntraDayService provideYahooIntraDayService(@Named("YahooIntraDayRetrofit") Retrofit retroFit) {
		return retroFit.create(IYahooIntraDayService.class);
	}

	@Provides
	@Singleton
	@Named("EarningsDatesRetrofit")
	Retrofit provideEarningsDatesRetrofit() {
		return new Retrofit.Builder()//
				.baseUrl(BuildConfig.STOCK_MARKET_RESEARCH_BASE_URL)//
				.addCallAdapterFactory(RxJavaCallAdapterFactory.create())//
				.addConverterFactory(AdvancedGsonConverterFactory.create())//
				.build();
	}

	@Provides
	@Singleton
	IGetEarningsDatesService provideEarningsDatesService(@Named("EarningsDatesRetrofit") Retrofit retroFit) {
		return retroFit.create(IGetEarningsDatesService.class);
	}
}
