package com.babestudios.stockmarket;

import com.babestudios.stockmarket.model.base.StockPriceSeriesItem;
import com.babestudios.stockmarket.model.dto.YahooIntraDayStockResponse;
import com.babestudios.stockmarket.model.dto.pojos.EarningsDateItem;
import com.babestudios.stockmarket.strategies.model.TradeResultObject;
import com.babestudios.stockmarket.strategies.multiclose.MultiCloseStrategy;
import com.babestudios.stockmarket.strategies.multiclose.MultiCloseStrategyParameters;
import com.google.gson.Gson;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;

import static com.babestudios.stockmarket.EarningStartControllerTest.mockFilteredEarningsDateResponse;
import static com.babestudios.stockmarket.EarningStartControllerTest.mockYahooDailyResponse;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

@RunWith(JUnit4.class)
public class EarningCloseControllerTest {

	private Gson gson = new Gson();

	@Test
	public void canCloseTest_no() {
		YahooIntraDayStockResponse yahooIntraDayResponse = gson.fromJson(mockYahooDailyResponse, YahooIntraDayStockResponse.class);
		EarningsDateItem[] earningsDateItem = gson.fromJson(mockFilteredEarningsDateResponse, EarningsDateItem[].class);
		TradeResultObject tradeResult = new TradeResultObject(MultiCloseStrategyParameters.STOP_PERCENTAGE);
		tradeResult.series = new ArrayList<>(Arrays.asList(yahooIntraDayResponse.series));
		tradeResult.earningsDateItem = earningsDateItem[0];
		tradeResult.openPriceItem = new StockPriceSeriesItem();
		tradeResult.openPriceItem.Timestamp = Calendar.getInstance().getTimeInMillis() / 1000;
		tradeResult.openPriceItem.close = 628d;
		MultiCloseStrategy m = new MultiCloseStrategy(tradeResult);
		TradeResultObject t = m.canCloseTrade();
		assertThat(t.closePriceItems.size() == 0, is(true));
	}

	@Test
	public void canCloseTest_limit_hit() {
		Gson g = new Gson();
		YahooIntraDayStockResponse yahooIntraDayResponse = g.fromJson(mockYahooDailyResponse, YahooIntraDayStockResponse.class);
		EarningsDateItem[] earningsDateItem = g.fromJson(mockFilteredEarningsDateResponse, EarningsDateItem[].class);
		TradeResultObject tradeResult = new TradeResultObject(MultiCloseStrategyParameters.STOP_PERCENTAGE);
		tradeResult.series = new ArrayList<>(Arrays.asList(yahooIntraDayResponse.series));
		tradeResult.earningsDateItem = earningsDateItem[0];
		tradeResult.openPriceItem = new StockPriceSeriesItem();
		tradeResult.openPriceItem.Timestamp = Calendar.getInstance().getTimeInMillis() / 1000;
		tradeResult.openPriceItem.close = 500d;
		MultiCloseStrategy m = new MultiCloseStrategy(tradeResult);
		TradeResultObject t = m.canCloseTrade();
		assertThat(t.closePriceItems.size() == 0, is(false));
	}

	@Test
	public void canCloseTest_limit_stop() {
		Gson g = new Gson();
		YahooIntraDayStockResponse yahooIntraDayResponse = g.fromJson(mockYahooDailyResponse, YahooIntraDayStockResponse.class);
		EarningsDateItem[] earningsDateItem = g.fromJson(mockFilteredEarningsDateResponse, EarningsDateItem[].class);
		TradeResultObject tradeResult = new TradeResultObject(MultiCloseStrategyParameters.STOP_PERCENTAGE);
		tradeResult.series = new ArrayList<>(Arrays.asList(yahooIntraDayResponse.series));
		tradeResult.earningsDateItem = earningsDateItem[0];
		tradeResult.openPriceItem = new StockPriceSeriesItem();
		tradeResult.openPriceItem.Timestamp = Calendar.getInstance().getTimeInMillis() / 1000;
		tradeResult.openPriceItem.close = 500d;
		tradeResult.closePriceItems = new ArrayList<>();
		StockPriceSeriesItem stockPriceSeriesItem = new StockPriceSeriesItem();
		stockPriceSeriesItem.close = 510;
		tradeResult.closePriceItems.add(stockPriceSeriesItem);
		stockPriceSeriesItem = new StockPriceSeriesItem();
		stockPriceSeriesItem.close = 520;
		tradeResult.closePriceItems.add(stockPriceSeriesItem);
		stockPriceSeriesItem = new StockPriceSeriesItem();
		stockPriceSeriesItem.close = 530;
		tradeResult.closePriceItems.add(stockPriceSeriesItem);
		MultiCloseStrategy m = new MultiCloseStrategy(tradeResult);
		TradeResultObject t = m.canCloseTrade();
		assertThat(t.isClosed, is(true));
	}



	@Test
	public void canCloseTest_stop() {
		Gson g = new Gson();
		YahooIntraDayStockResponse yahooIntraDayResponse = g.fromJson(mockYahooDailyResponse, YahooIntraDayStockResponse.class);
		EarningsDateItem[] earningsDateItem = g.fromJson(mockFilteredEarningsDateResponse, EarningsDateItem[].class);
		TradeResultObject tradeResult = new TradeResultObject(MultiCloseStrategyParameters.STOP_PERCENTAGE);
		tradeResult.series = new ArrayList<>(Arrays.asList(yahooIntraDayResponse.series));
		tradeResult.earningsDateItem = earningsDateItem[0];
		tradeResult.openPriceItem = new StockPriceSeriesItem();
		tradeResult.openPriceItem.Timestamp = Calendar.getInstance().getTimeInMillis() / 1000;
		tradeResult.openPriceItem.close = 700d;
		MultiCloseStrategy m = new MultiCloseStrategy(tradeResult);
		TradeResultObject t = m.canCloseTrade();
		assertThat(t.closePriceItems.size() == 0, is(false));
		assertThat(t.isClosed, is(true));
	}

	@Test
	public void canCloseTest_time_limit() {
		Gson g = new Gson();
		YahooIntraDayStockResponse yahooIntraDayResponse = g.fromJson(mockYahooDailyResponse, YahooIntraDayStockResponse.class);
		EarningsDateItem[] earningsDateItem = g.fromJson(mockFilteredEarningsDateResponse, EarningsDateItem[].class);
		TradeResultObject tradeResult = new TradeResultObject(MultiCloseStrategyParameters.STOP_PERCENTAGE);
		tradeResult.series = new ArrayList<>(Arrays.asList(yahooIntraDayResponse.series));
		tradeResult.earningsDateItem = earningsDateItem[0];
		tradeResult.openPriceItem = new StockPriceSeriesItem();
		tradeResult.openPriceItem.Timestamp = tradeResult.series.get(0).Timestamp -  MultiCloseStrategyParameters.MAX_HOLDING_PERIOD - 1000;
		tradeResult.openPriceItem.close = 500d;
		MultiCloseStrategy m = new MultiCloseStrategy(tradeResult);
		TradeResultObject t = m.canCloseTrade();
		assertThat(t.closePriceItems.size() == 0, is(false));
	}
}
