package com.babestudios.stockmarket;

import com.babestudios.stockmarket.model.dto.YahooIntraDayStockResponse;
import com.babestudios.stockmarket.model.dto.pojos.IndexChangeItem;
import com.babestudios.stockmarket.network.converters.AdvancedGsonConverterFactory;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import okhttp3.MediaType;
import okhttp3.ResponseBody;
import retrofit2.Retrofit;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

@RunWith(JUnit4.class)
public class GsonTest {

	@Test
	public void advancedGsonConverter_yahoo_converts() {
		AdvancedGsonConverterFactory c = AdvancedGsonConverterFactory.create();
		ResponseBody r = ResponseBody.create(MediaType.parse("application/json; charset=utf-8"), "finance_charts_json_callback({\"meta\" : null, \"TimeStamp-Ranges\" : null, \"Timestamp\" : {\"min\" :1445866200,\"max\" :1446843600 }, \"labels\" : null, \"ranges\" : null, \"series\": null})");

		Retrofit retrofit = new Retrofit.Builder()//
				.baseUrl(BuildConfig.YAHOO_INTRA_DAY_BASE_URL)//
				.addConverterFactory(AdvancedGsonConverterFactory.create())//
				.build();
		YahooIntraDayStockResponse response = new YahooIntraDayStockResponse();
		try {
			response = ((YahooIntraDayStockResponse) c.responseBodyConverter(YahooIntraDayStockResponse.class, null, retrofit).convert(r));
		} catch (Exception e) {
			e.printStackTrace();
		}
		assertThat(response.Timestamp.min == 1445866200, is(true));
	}

	@Test
	public void advancedGsonConverter_index_changes_numeric_boolean_converts() {
		AdvancedGsonConverterFactory c = AdvancedGsonConverterFactory.create();
		ResponseBody r = ResponseBody.create(MediaType.parse("application/json; charset=utf-8"), "[{\"id\":\"1\",\"ticker_id\":\"1\",\"date\":\"2015-01-01\",\"index_id\":\"1\",\"addition\":\"1\"},{\"id\":\"2\",\"ticker_id\":\"2\",\"date\":\"2015-01-01\",\"index_id\":\"1\",\"addition\":\"0\"},{\"id\":\"3\",\"ticker_id\":\"3\",\"date\":\"2015-01-01\",\"index_id\":\"1\",\"addition\":\"1\"},{\"id\":\"4\",\"ticker_id\":\"4\",\"date\":\"2015-01-01\",\"index_id\":\"1\",\"addition\":\"1\"},{\"id\":\"5\",\"ticker_id\":\"5\",\"date\":\"2015-01-01\",\"index_id\":\"1\",\"addition\":\"1\"},{\"id\":\"6\",\"ticker_id\":\"6\",\"date\":\"2015-01-01\",\"index_id\":\"1\",\"addition\":\"1\"},{\"id\":\"7\",\"ticker_id\":\"7\",\"date\":\"2015-01-01\",\"index_id\":\"1\",\"addition\":\"1\"},{\"id\":\"8\",\"ticker_id\":\"8\",\"date\":\"2015-01-01\",\"index_id\":\"1\",\"addition\":\"1\"},{\"id\":\"9\",\"ticker_id\":\"9\",\"date\":\"2015-01-01\",\"index_id\":\"1\",\"addition\":\"1\"}]");

		Retrofit retrofit = new Retrofit.Builder()//
				.baseUrl(BuildConfig.STOCK_MARKET_RESEARCH_BASE_URL)//
				.addConverterFactory(AdvancedGsonConverterFactory.create())//
				.build();
		IndexChangeItem[] response = new IndexChangeItem[0];
		try {
			response = ((IndexChangeItem[]) c.responseBodyConverter(IndexChangeItem[].class, null, retrofit).convert(r));
		} catch (Exception e) {
			e.printStackTrace();
		}
		assertThat(response.length == 9, is(true));
		assertThat(response[0].addition, is(true));
		assertThat(response[1].addition, is(false));
	}

	@Test
	public void advancedGsonConverter_index_changes_boolean_converts() {
		AdvancedGsonConverterFactory c = AdvancedGsonConverterFactory.create();
		ResponseBody r = ResponseBody.create(MediaType.parse("application/json; charset=utf-8"), "[{\"id\":\"1\",\"ticker_id\":\"1\",\"date\":\"2015-01-01\",\"index_id\":\"1\",\"addition\":true},{\"id\":\"2\",\"ticker_id\":\"2\",\"date\":\"2015-01-01\",\"index_id\":\"1\",\"addition\":false},{\"id\":\"3\",\"ticker_id\":\"3\",\"date\":\"2015-01-01\",\"index_id\":\"1\",\"addition\":true},{\"id\":\"4\",\"ticker_id\":\"4\",\"date\":\"2015-01-01\",\"index_id\":\"1\",\"addition\":true},{\"id\":\"5\",\"ticker_id\":\"5\",\"date\":\"2015-01-01\",\"index_id\":\"1\",\"addition\":true},{\"id\":\"6\",\"ticker_id\":\"6\",\"date\":\"2015-01-01\",\"index_id\":\"1\",\"addition\":true},{\"id\":\"7\",\"ticker_id\":\"7\",\"date\":\"2015-01-01\",\"index_id\":\"1\",\"addition\":true},{\"id\":\"8\",\"ticker_id\":\"8\",\"date\":\"2015-01-01\",\"index_id\":\"1\",\"addition\":true},{\"id\":\"9\",\"ticker_id\":\"9\",\"date\":\"2015-01-01\",\"index_id\":\"1\",\"addition\":true}]");

		Retrofit retrofit = new Retrofit.Builder()//
				.baseUrl(BuildConfig.STOCK_MARKET_RESEARCH_BASE_URL)//
				.addConverterFactory(AdvancedGsonConverterFactory.create())//
				.build();
		IndexChangeItem[] response = new IndexChangeItem[0];
		try {
			response = ((IndexChangeItem[]) c.responseBodyConverter(IndexChangeItem[].class, null, retrofit).convert(r));
		} catch (Exception e) {
			e.printStackTrace();
		}
		assertThat(response.length == 9, is(true));
		assertThat(response[0].addition, is(true));
		assertThat(response[1].addition, is(false));
	}
}
