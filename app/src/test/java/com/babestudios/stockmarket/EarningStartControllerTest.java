package com.babestudios.stockmarket;

import com.babestudios.stockmarket.activities.earningsstart.EarningsStartActivity;
import com.babestudios.stockmarket.activities.earningsstart.EarningsStartController;
import com.babestudios.stockmarket.model.base.StockPriceSeriesItem;
import com.babestudios.stockmarket.model.dto.YahooIntraDayStockResponse;
import com.babestudios.stockmarket.model.dto.pojos.EarningsDateItem;
import com.babestudios.stockmarket.network.interfaces.IGetEarningsDatesService;
import com.babestudios.stockmarket.network.interfaces.IYahooIntraDayService;
import com.babestudios.stockmarket.strategies.model.TradeResultObject;
import com.babestudios.stockmarket.strategies.multiclose.MultiCloseStrategy;
import com.babestudios.stockmarket.strategies.multiclose.MultiCloseStrategyParameters;
import com.google.gson.Gson;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;

import rx.Observable;
import rx.observers.TestSubscriber;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class EarningStartControllerTest {

	private Gson gson = new Gson();

	@Mock
	IGetEarningsDatesService mockEarningsDatesService;

	@Mock
	IYahooIntraDayService mockYahooIntraDayService;

	@Before
	public void initMocks() {
		mockEarningsDatesService = createMockEarningsDatesService();
		mockYahooIntraDayService = createMockYahooIntraDayService();
	}

	private IYahooIntraDayService createMockYahooIntraDayService() {
		YahooIntraDayStockResponse response = gson.fromJson(mockYahooDailyResponse, YahooIntraDayStockResponse.class);
		when(mockYahooIntraDayService.getIntraDayStockPrices("icp.l", "3")).thenReturn(
				Observable.just(response));
		/*YahooIntraDayStockResponse responseCanOpen = gson.fromJson(mockYahooDailyResponseCanOpen, YahooIntraDayStockResponse.class);
		when(mockYahooIntraDayService.getIntraDayStockPrices("icp.l", "4")).thenReturn(
				Observable.just(responseCanOpen));*/
		return mockYahooIntraDayService;
	}

	private IGetEarningsDatesService createMockEarningsDatesService() {
		EarningsDateItem[] items = gson.fromJson(mockFilteredEarningsDateResponse, EarningsDateItem[].class);
		when(mockEarningsDatesService.getEarningsDates()).thenReturn(
				Observable.just(items));
		return mockEarningsDatesService;

	}

	@Test
	public void earningsStartController_isToday() {
		EarningsStartController c = new EarningsStartController(new EarningsStartActivity());
		c.earningsDatesService = mockEarningsDatesService;
		EarningsDateItem i = new EarningsDateItem();
		i.date = new Date();
		assertThat(c.isToday(i), is(true));
	}

	@Test
	public void downloadEarningsDatesTest() {
		TestSubscriber<String> testSubscriber = new TestSubscriber<>();
		Observable<EarningsDateItem[]> items = mockEarningsDatesService.getEarningsDates();
		items.map(earningsDateItems -> earningsDateItems[0].ticker)
				.subscribe(testSubscriber);
		testSubscriber.assertValue("icp.l");

	}

	@Test
	public void downloadYahooIntraDayTest() {
		TestSubscriber<Double> testSubscriber = new TestSubscriber<>();
		Observable<YahooIntraDayStockResponse> response = mockYahooIntraDayService.getIntraDayStockPrices("icp.l", "3");
		response.map(resp -> resp.series[0].close)
				.subscribe(testSubscriber);
		testSubscriber.assertValue(637.5d);
	}

	@Test
	public void canOpenTest_false() {
		YahooIntraDayStockResponse yahooIntraDayResponse = gson.fromJson(mockYahooDailyResponse, YahooIntraDayStockResponse.class);
		EarningsDateItem[] earningsDateItem = gson.fromJson(mockFilteredEarningsDateResponse, EarningsDateItem[].class);
		TradeResultObject tradeResult = new TradeResultObject(MultiCloseStrategyParameters.STOP_PERCENTAGE);
		tradeResult.series = new ArrayList<>(Arrays.asList(yahooIntraDayResponse.series));
		tradeResult.earningsDateItem = earningsDateItem[0];
		MultiCloseStrategy m = new MultiCloseStrategy(tradeResult);
		assertThat(m.canOpenTrade().openPriceItem != null, is(false));
	}

	/*TODO find a true one*/
	@Test
	public void canOpenTest_true() {
		YahooIntraDayStockResponse yahooIntraDayResponse = gson.fromJson(mockYahooDailyResponseCanOpen, YahooIntraDayStockResponse.class);
		EarningsDateItem[] earningsDateItem = gson.fromJson(mockFilteredEarningsDateResponse, EarningsDateItem[].class);
		TradeResultObject tradeResult = new TradeResultObject(MultiCloseStrategyParameters.STOP_PERCENTAGE);
		tradeResult.series = new ArrayList<>(Arrays.asList(yahooIntraDayResponse.series));
		tradeResult.earningsDateItem = earningsDateItem[0];
		MultiCloseStrategy m = new MultiCloseStrategy(tradeResult);
		StockPriceSeriesItem openPriceItem = m.canOpenTrade().openPriceItem;
		assertThat(openPriceItem != null, is(true));
		Calendar c = Calendar.getInstance();
		Date date = new Date(openPriceItem.Timestamp * 1000);
		c.setTime(date);
		assertThat(c.get(Calendar.HOUR_OF_DAY) < 12, is(true));
	}

	static String mockFilteredEarningsDateResponse = "[{\"ResultDate\":\"2016-04-22\",\"Ticker\":\"icp.l\",\"Country\":\"UK\"},{\"ResultDate\":\"2015-04-30\",\"Ticker\":\"spx.l\",\"Country\":\"UK\"}]";

	static String mockYahooDailyResponse = "{ \"meta\" :\n" +
			" {\n" +
			"  \"uri\" :\"/instrument/1.0/icp.l/chartdata;type=quote;range=1d/json\",\n" +
			"  \"ticker\" : \"icp.l\",\n" +
			"  \"Company-Name\" : \"INTERMEDIATE CAPITAL GROUP PLC \",\n" +
			"  \"Exchange-Name\" : \"LSE\",\n" +
			"  \"unit\" : \"MIN\",\n" +
			"  \"timezone\" : \"BST\",\n" +
			"  \"currency\" : \"GBp\",\n" +
			"  \"gmtoffset\" : 3600,\n" +
			"  \"previous_close\" : 625.0000\n" +
			" }\n" +
			" ,\n" +
			" \"Timestamp\" : {\"min\" :1461308400,\"max\" :1461339000 }\n" +
			" ,\n" +
			" \"labels\" : [1461308400,1461312000,1461315600,1461319200,1461322800,1461326400,1461330000,1461333600,1461337200 ]\n" +
			" ,\n" +
			" \"ranges\" : {\"close\" : {\"min\" :620.0000,\"max\" :630.0200 },\"high\" : {\"min\" :620.9900,\"max\" :630.0200 },\"low\" : {\"min\" :620.0000,\"max\" :630.0200 },\"open\" : {\"min\" :620.9900,\"max\" :630.0200 },\"volume\" : {\"min\" :0,\"max\" :28100 } }\n" +
			" ,\n" +
			" \"series\" : [\n" +
			"{ \"Timestamp\" :1460963051,\"close\" :637.5000,\"high\" :640.0000,\"low\" :637.5000,\"open\" :638.5000,\"volume\" :4500 }  \n" +
			", { \"Timestamp\" :1460963259,\"close\" :637.0000,\"high\" :637.0000,\"low\" :637.0000,\"open\" :637.0000,\"volume\" :500 }  \n" +
			", { \"Timestamp\" :1460963651,\"close\" :640.5000,\"high\" :640.5000,\"low\" :639.5000,\"open\" :639.5000,\"volume\" :2900 }  \n" +
			", { \"Timestamp\" :1460963877,\"close\" :639.5000,\"high\" :639.5000,\"low\" :639.5000,\"open\" :639.5000,\"volume\" :700 }  \n" +
			", { \"Timestamp\" :1460964096,\"close\" :639.5000,\"high\" :639.5000,\"low\" :639.5000,\"open\" :639.5000,\"volume\" :400 }  \n" +
			", { \"Timestamp\" :1460964521,\"close\" :640.5000,\"high\" :640.5000,\"low\" :639.0000,\"open\" :639.0000,\"volume\" :1600 }  \n" +
			", { \"Timestamp\" :1460964809,\"close\" :640.0000,\"high\" :641.0000,\"low\" :640.0000,\"open\" :640.0000,\"volume\" :2800 }  \n" +
			", { \"Timestamp\" :1460965083,\"close\" :641.0000,\"high\" :642.5000,\"low\" :640.5000,\"open\" :640.5000,\"volume\" :3400 }  \n" +
			", { \"Timestamp\" :1460965339,\"close\" :641.0000,\"high\" :641.0000,\"low\" :641.0000,\"open\" :641.0000,\"volume\" :1700 }  \n" +
			", { \"Timestamp\" :1460965562,\"close\" :641.0000,\"high\" :641.0000,\"low\" :640.0000,\"open\" :640.0000,\"volume\" :1600 }  \n" +
			", { \"Timestamp\" :1460966044,\"close\" :642.5000,\"high\" :643.0000,\"low\" :642.5000,\"open\" :643.0000,\"volume\" :3000 }  \n" +
			", { \"Timestamp\" :1460966139,\"close\" :642.5000,\"high\" :642.5000,\"low\" :642.5000,\"open\" :642.5000,\"volume\" :600 }  \n" +
			", { \"Timestamp\" :1460966651,\"close\" :643.5000,\"high\" :643.5000,\"low\" :642.5000,\"open\" :642.5000,\"volume\" :3000 }  \n" +
			", { \"Timestamp\" :1460966944,\"close\" :642.5000,\"high\" :643.5000,\"low\" :642.0000,\"open\" :643.5000,\"volume\" :6900 }  \n" +
			", { \"Timestamp\" :1460967288,\"close\" :644.0000,\"high\" :644.0000,\"low\" :642.5000,\"open\" :643.0000,\"volume\" :3400 }  \n" +
			", { \"Timestamp\" :1460967592,\"close\" :645.5000,\"high\" :645.5000,\"low\" :644.5000,\"open\" :644.5000,\"volume\" :700 }  \n" +
			", { \"Timestamp\" :1460967863,\"close\" :644.0000,\"high\" :644.0000,\"low\" :644.0000,\"open\" :644.0000,\"volume\" :3500 }  \n" +
			", { \"Timestamp\" :1460968142,\"close\" :643.0000,\"high\" :644.0000,\"low\" :643.0000,\"open\" :643.5000,\"volume\" :3900 }  \n" +
			", { \"Timestamp\" :1460968445,\"close\" :644.0000,\"high\" :644.0000,\"low\" :643.5000,\"open\" :643.5000,\"volume\" :10100 }  \n" +
			", { \"Timestamp\" :1460968708,\"close\" :643.5000,\"high\" :645.0000,\"low\" :643.5000,\"open\" :644.0000,\"volume\" :0 }  \n" +
			", { \"Timestamp\" :1460969045,\"close\" :645.0000,\"high\" :645.5000,\"low\" :644.0000,\"open\" :644.0000,\"volume\" :3800 }  \n" +
			", { \"Timestamp\" :1460969351,\"close\" :645.5000,\"high\" :646.0000,\"low\" :645.5000,\"open\" :645.5000,\"volume\" :1600 }  \n" +
			", { \"Timestamp\" :1460969686,\"close\" :646.0000,\"high\" :647.0000,\"low\" :645.0000,\"open\" :645.0000,\"volume\" :3800 }  \n" +
			", { \"Timestamp\" :1460969780,\"close\" :645.5000,\"high\" :646.5000,\"low\" :645.5000,\"open\" :646.0000,\"volume\" :3200 }  \n" +
			", { \"Timestamp\" :1460970092,\"close\" :645.0000,\"high\" :646.0000,\"low\" :645.0000,\"open\" :645.5000,\"volume\" :3300 }  \n" +
			", { \"Timestamp\" :1460970566,\"close\" :644.5000,\"high\" :645.0000,\"low\" :644.5000,\"open\" :645.0000,\"volume\" :1600 }  \n" +
			", { \"Timestamp\" :1460971158,\"close\" :643.8182,\"high\" :644.5000,\"low\" :643.0000,\"open\" :644.5000,\"volume\" :16500 }  \n" +
			", { \"Timestamp\" :1460971487,\"close\" :644.5000,\"high\" :644.5000,\"low\" :644.5000,\"open\" :644.5000,\"volume\" :0 }  \n" +
			", { \"Timestamp\" :1460971510,\"close\" :644.0000,\"high\" :644.0000,\"low\" :644.0000,\"open\" :644.0000,\"volume\" :700 }  \n" +
			", { \"Timestamp\" :1460972015,\"close\" :644.5000,\"high\" :644.5000,\"low\" :644.0000,\"open\" :644.0000,\"volume\" :5000 }  \n" +
			", { \"Timestamp\" :1460972214,\"close\" :645.0000,\"high\" :645.0000,\"low\" :645.0000,\"open\" :645.0000,\"volume\" :6000 }  \n" +
			", { \"Timestamp\" :1460972663,\"close\" :645.0000,\"high\" :645.0000,\"low\" :644.5000,\"open\" :644.5000,\"volume\" :2500 }  \n" +
			", { \"Timestamp\" :1460972810,\"close\" :644.0000,\"high\" :644.0000,\"low\" :644.0000,\"open\" :644.0000,\"volume\" :2700 }  \n" +
			", { \"Timestamp\" :1460973080,\"close\" :643.5000,\"high\" :643.5000,\"low\" :643.5000,\"open\" :643.5000,\"volume\" :200 }  \n" +
			", { \"Timestamp\" :1460973588,\"close\" :642.0000,\"high\" :643.0000,\"low\" :642.0000,\"open\" :643.0000,\"volume\" :4500 }  \n" +
			", { \"Timestamp\" :1460974035,\"close\" :641.5000,\"high\" :642.0000,\"low\" :641.0000,\"open\" :642.0000,\"volume\" :15100 }  \n" +
			", { \"Timestamp\" :1460974488,\"close\" :640.0000,\"high\" :641.5000,\"low\" :640.0000,\"open\" :641.5000,\"volume\" :11900 }  \n" +
			", { \"Timestamp\" :1460974531,\"close\" :640.0000,\"high\" :640.0000,\"low\" :640.0000,\"open\" :640.0000,\"volume\" :11200 }  \n" +
			", { \"Timestamp\" :1460975040,\"close\" :639.0000,\"high\" :639.5000,\"low\" :639.0000,\"open\" :639.5000,\"volume\" :0 }  \n" +
			", { \"Timestamp\" :1460975348,\"close\" :639.0000,\"high\" :639.0000,\"low\" :639.0000,\"open\" :639.0000,\"volume\" :600 }  \n" +
			", { \"Timestamp\" :1460975580,\"close\" :639.0000,\"high\" :639.0000,\"low\" :639.0000,\"open\" :639.0000,\"volume\" :1400 }  \n" +
			", { \"Timestamp\" :1460975810,\"close\" :639.0000,\"high\" :639.0000,\"low\" :639.0000,\"open\" :639.0000,\"volume\" :800 }  \n" +
			", { \"Timestamp\" :1460976035,\"close\" :640.0000,\"high\" :640.0000,\"low\" :639.5000,\"open\" :639.5000,\"volume\" :2200 }  \n" +
			", { \"Timestamp\" :1460976794,\"close\" :639.5000,\"high\" :640.0000,\"low\" :639.5000,\"open\" :640.0000,\"volume\" :4900 }  \n" +
			", { \"Timestamp\" :1460976978,\"close\" :639.5000,\"high\" :639.5000,\"low\" :639.5000,\"open\" :639.5000,\"volume\" :1300 }  \n" +
			", { \"Timestamp\" :1460977341,\"close\" :637.5000,\"high\" :637.5000,\"low\" :637.5000,\"open\" :637.5000,\"volume\" :3600 }  \n" +
			", { \"Timestamp\" :1460977646,\"close\" :641.0000,\"high\" :641.0000,\"low\" :640.0000,\"open\" :640.0000,\"volume\" :1500 }  \n" +
			", { \"Timestamp\" :1460978397,\"close\" :641.0000,\"high\" :641.0000,\"low\" :641.0000,\"open\" :641.0000,\"volume\" :0 }  \n" +
			", { \"Timestamp\" :1460978916,\"close\" :641.0000,\"high\" :641.0000,\"low\" :641.0000,\"open\" :641.0000,\"volume\" :1500 }  \n" +
			", { \"Timestamp\" :1460979094,\"close\" :641.0000,\"high\" :641.5000,\"low\" :641.0000,\"open\" :641.5000,\"volume\" :700 }  \n" +
			", { \"Timestamp\" :1460979341,\"close\" :641.5000,\"high\" :641.5000,\"low\" :641.5000,\"open\" :641.5000,\"volume\" :1000 }  \n" +
			", { \"Timestamp\" :1460980224,\"close\" :640.5000,\"high\" :640.5000,\"low\" :640.5000,\"open\" :640.5000,\"volume\" :1200 }  \n" +
			", { \"Timestamp\" :1460980778,\"close\" :641.0000,\"high\" :641.0000,\"low\" :641.0000,\"open\" :641.0000,\"volume\" :1100 }  \n" +
			", { \"Timestamp\" :1460981418,\"close\" :641.0000,\"high\" :641.0000,\"low\" :641.0000,\"open\" :641.0000,\"volume\" :1700 }  \n" +
			", { \"Timestamp\" :1460981905,\"close\" :640.5000,\"high\" :640.5000,\"low\" :640.5000,\"open\" :640.5000,\"volume\" :2700 }  \n" +
			", { \"Timestamp\" :1460982111,\"close\" :640.5000,\"high\" :640.5000,\"low\" :640.5000,\"open\" :640.5000,\"volume\" :0 }  \n" +
			", { \"Timestamp\" :1460982365,\"close\" :640.5000,\"high\" :640.5000,\"low\" :640.5000,\"open\" :640.5000,\"volume\" :1500 }  \n" +
			", { \"Timestamp\" :1460982631,\"close\" :640.5000,\"high\" :640.5000,\"low\" :640.5000,\"open\" :640.5000,\"volume\" :11200 }  \n" +
			", { \"Timestamp\" :1460983784,\"close\" :639.0000,\"high\" :640.0000,\"low\" :639.0000,\"open\" :640.0000,\"volume\" :0 }  \n" +
			", { \"Timestamp\" :1460984040,\"close\" :638.5000,\"high\" :638.5000,\"low\" :637.5000,\"open\" :638.5000,\"volume\" :18800 }  \n" +
			", { \"Timestamp\" :1460984393,\"close\" :637.5000,\"high\" :637.5000,\"low\" :637.5000,\"open\" :637.5000,\"volume\" :0 }  \n" +
			", { \"Timestamp\" :1460984601,\"close\" :637.5000,\"high\" :638.0000,\"low\" :637.0000,\"open\" :638.0000,\"volume\" :5300 }  \n" +
			", { \"Timestamp\" :1460984944,\"close\" :637.0000,\"high\" :637.5000,\"low\" :637.0000,\"open\" :637.0000,\"volume\" :2300 }  \n" +
			", { \"Timestamp\" :1460985548,\"close\" :637.5000,\"high\" :637.5000,\"low\" :637.0000,\"open\" :637.0000,\"volume\" :4500 }  \n" +
			", { \"Timestamp\" :1460985782,\"close\" :636.0000,\"high\" :637.0000,\"low\" :636.0000,\"open\" :637.0000,\"volume\" :3600 }  \n" +
			", { \"Timestamp\" :1460986347,\"close\" :636.5000,\"high\" :637.0000,\"low\" :636.5000,\"open\" :637.0000,\"volume\" :5700 }  \n" +
			", { \"Timestamp\" :1460986530,\"close\" :636.0000,\"high\" :636.0000,\"low\" :636.0000,\"open\" :636.0000,\"volume\" :28700 }  \n" +
			", { \"Timestamp\" :1460987294,\"close\" :636.0000,\"high\" :636.0000,\"low\" :635.5000,\"open\" :636.0000,\"volume\" :0 }  \n" +
			", { \"Timestamp\" :1460987662,\"close\" :636.0000,\"high\" :637.0000,\"low\" :636.0000,\"open\" :636.0000,\"volume\" :8900 }  \n" +
			", { \"Timestamp\" :1460987732,\"close\" :636.0000,\"high\" :636.0000,\"low\" :636.0000,\"open\" :636.0000,\"volume\" :900 }  \n" +
			", { \"Timestamp\" :1460988266,\"close\" :636.5000,\"high\" :636.5000,\"low\" :635.5000,\"open\" :636.5000,\"volume\" :11400 }  \n" +
			", { \"Timestamp\" :1460988451,\"close\" :637.0000,\"high\" :637.0000,\"low\" :636.5000,\"open\" :636.5000,\"volume\" :1400 }  \n" +
			", { \"Timestamp\" :1460988864,\"close\" :637.0000,\"high\" :637.0000,\"low\" :636.5000,\"open\" :636.5000,\"volume\" :1800 }  \n" +
			", { \"Timestamp\" :1460989182,\"close\" :637.0000,\"high\" :637.5000,\"low\" :637.0000,\"open\" :637.0000,\"volume\" :5300 }  \n" +
			", { \"Timestamp\" :1460989262,\"close\" :637.0000,\"high\" :637.5000,\"low\" :637.0000,\"open\" :637.5000,\"volume\" :2100 }  \n" +
			", { \"Timestamp\" :1460989716,\"close\" :637.5000,\"high\" :638.0000,\"low\" :637.5000,\"open\" :638.0000,\"volume\" :600 }  \n" +
			", { \"Timestamp\" :1460990213,\"close\" :638.0000,\"high\" :638.0000,\"low\" :638.0000,\"open\" :638.0000,\"volume\" :600 }  \n" +
			", { \"Timestamp\" :1460990530,\"close\" :638.5000,\"high\" :638.5000,\"low\" :638.5000,\"open\" :638.5000,\"volume\" :1300 }  \n" +
			", { \"Timestamp\" :1460990977,\"close\" :639.5000,\"high\" :639.5000,\"low\" :639.0000,\"open\" :639.0000,\"volume\" :2700 }  \n" +
			", { \"Timestamp\" :1460991281,\"close\" :639.5000,\"high\" :639.5000,\"low\" :639.0000,\"open\" :639.0000,\"volume\" :5200 }  \n" +
			", { \"Timestamp\" :1460991357,\"close\" :639.5000,\"high\" :639.5000,\"low\" :639.5000,\"open\" :639.5000,\"volume\" :500 }  \n" +
			", { \"Timestamp\" :1460991789,\"close\" :639.5000,\"high\" :640.0000,\"low\" :639.5000,\"open\" :640.0000,\"volume\" :5700 }  \n" +
			", { \"Timestamp\" :1460992161,\"close\" :639.0000,\"high\" :639.0000,\"low\" :639.0000,\"open\" :639.0000,\"volume\" :0 }  \n" +
			", { \"Timestamp\" :1460992460,\"close\" :639.5000,\"high\" :639.5000,\"low\" :639.5000,\"open\" :639.5000,\"volume\" :1200 }  \n" +
			", { \"Timestamp\" :1460992579,\"close\" :639.5000,\"high\" :639.5000,\"low\" :639.0000,\"open\" :639.0000,\"volume\" :5200 }  \n" +
			", { \"Timestamp\" :1460993360,\"close\" :638.5000,\"high\" :639.0000,\"low\" :638.5000,\"open\" :639.0000,\"volume\" :10900 }  \n" +
			", { \"Timestamp\" :1460993400,\"close\" :641.0000,\"high\" :641.0000,\"low\" :641.0000,\"open\" :641.0000,\"volume\" :0 }  \n" +
			", { \"Timestamp\" :1461049221,\"close\" :642.5000,\"high\" :642.5000,\"low\" :642.5000,\"open\" :642.5000,\"volume\" :0 }  \n" +
			", { \"Timestamp\" :1461049647,\"close\" :640.5000,\"high\" :640.5000,\"low\" :639.0000,\"open\" :639.0000,\"volume\" :1000 }  \n" +
			", { \"Timestamp\" :1461049924,\"close\" :642.0000,\"high\" :642.0000,\"low\" :641.0000,\"open\" :641.0000,\"volume\" :2100 }  \n" +
			", { \"Timestamp\" :1461050109,\"close\" :642.5000,\"high\" :642.5000,\"low\" :642.5000,\"open\" :642.5000,\"volume\" :1900 }  \n" +
			", { \"Timestamp\" :1461050582,\"close\" :641.0000,\"high\" :642.5000,\"low\" :641.0000,\"open\" :642.5000,\"volume\" :3500 }  \n" +
			", { \"Timestamp\" :1461050988,\"close\" :641.5000,\"high\" :642.0000,\"low\" :641.0000,\"open\" :641.0000,\"volume\" :4000 }  \n" +
			", { \"Timestamp\" :1461051199,\"close\" :641.0000,\"high\" :641.0000,\"low\" :640.5000,\"open\" :641.0000,\"volume\" :2900 }  \n" +
			", { \"Timestamp\" :1461051500,\"close\" :641.0000,\"high\" :641.0000,\"low\" :640.0000,\"open\" :641.0000,\"volume\" :8300 }  \n" +
			", { \"Timestamp\" :1461051843,\"close\" :641.0000,\"high\" :641.5000,\"low\" :640.5000,\"open\" :641.5000,\"volume\" :9800 }  \n" +
			", { \"Timestamp\" :1461051960,\"close\" :641.0000,\"high\" :641.5000,\"low\" :640.5000,\"open\" :641.0000,\"volume\" :3200 }  \n" +
			", { \"Timestamp\" :1461052463,\"close\" :641.5000,\"high\" :641.5000,\"low\" :641.5000,\"open\" :641.5000,\"volume\" :2500 }  \n" +
			", { \"Timestamp\" :1461053054,\"close\" :642.5000,\"high\" :644.5000,\"low\" :642.5000,\"open\" :643.5000,\"volume\" :12100 }  \n" +
			", { \"Timestamp\" :1461053192,\"close\" :642.0000,\"high\" :642.5000,\"low\" :642.0000,\"open\" :642.5000,\"volume\" :800 }  \n" +
			", { \"Timestamp\" :1461053548,\"close\" :642.5000,\"high\" :642.5000,\"low\" :642.5000,\"open\" :642.5000,\"volume\" :1100 }  \n" +
			", { \"Timestamp\" :1461053889,\"close\" :643.5000,\"high\" :643.5000,\"low\" :643.5000,\"open\" :643.5000,\"volume\" :1300 }  \n" +
			", { \"Timestamp\" :1461054245,\"close\" :644.5000,\"high\" :645.0000,\"low\" :644.5000,\"open\" :645.0000,\"volume\" :7000 }  \n" +
			", { \"Timestamp\" :1461054871,\"close\" :644.0000,\"high\" :644.0000,\"low\" :644.0000,\"open\" :644.0000,\"volume\" :2600 }  \n" +
			", { \"Timestamp\" :1461055125,\"close\" :642.5000,\"high\" :643.5000,\"low\" :642.5000,\"open\" :643.5000,\"volume\" :6800 }  \n" +
			", { \"Timestamp\" :1461055452,\"close\" :642.5000,\"high\" :643.0000,\"low\" :642.5000,\"open\" :643.0000,\"volume\" :500 }  \n" +
			", { \"Timestamp\" :1461055738,\"close\" :642.5000,\"high\" :643.0000,\"low\" :642.5000,\"open\" :643.0000,\"volume\" :1900 }  \n" +
			", { \"Timestamp\" :1461056044,\"close\" :643.0000,\"high\" :643.0000,\"low\" :642.5000,\"open\" :643.0000,\"volume\" :4400 }  \n" +
			", { \"Timestamp\" :1461056399,\"close\" :642.0000,\"high\" :643.0000,\"low\" :642.0000,\"open\" :643.0000,\"volume\" :2800 }  \n" +
			", { \"Timestamp\" :1461056582,\"close\" :642.0000,\"high\" :642.0000,\"low\" :641.5000,\"open\" :641.5000,\"volume\" :2100 }  \n" +
			", { \"Timestamp\" :1461056957,\"close\" :641.5000,\"high\" :642.0000,\"low\" :641.0000,\"open\" :642.0000,\"volume\" :2900 }  \n" +
			", { \"Timestamp\" :1461057241,\"close\" :641.5000,\"high\" :641.5000,\"low\" :641.5000,\"open\" :641.5000,\"volume\" :3300 }  \n" +
			", { \"Timestamp\" :1461057517,\"close\" :643.0000,\"high\" :643.0000,\"low\" :642.0000,\"open\" :642.0000,\"volume\" :1200 }  \n" +
			", { \"Timestamp\" :1461057859,\"close\" :643.0000,\"high\" :643.0000,\"low\" :642.5000,\"open\" :643.0000,\"volume\" :2200 }  \n" +
			", { \"Timestamp\" :1461058185,\"close\" :643.5000,\"high\" :643.5000,\"low\" :643.5000,\"open\" :643.5000,\"volume\" :1200 }  \n" +
			", { \"Timestamp\" :1461058270,\"close\" :643.5000,\"high\" :644.0000,\"low\" :643.5000,\"open\" :643.5000,\"volume\" :1800 }  \n" +
			", { \"Timestamp\" :1461058568,\"close\" :643.5000,\"high\" :643.5000,\"low\" :642.5000,\"open\" :643.5000,\"volume\" :4200 }  \n" +
			", { \"Timestamp\" :1461059082,\"close\" :643.0000,\"high\" :643.0000,\"low\" :643.0000,\"open\" :643.0000,\"volume\" :1500 }  \n" +
			", { \"Timestamp\" :1461059500,\"close\" :643.5000,\"high\" :643.5000,\"low\" :643.4000,\"open\" :643.4000,\"volume\" :600 }  \n" +
			", { \"Timestamp\" :1461059778,\"close\" :643.0000,\"high\" :643.0000,\"low\" :643.0000,\"open\" :643.0000,\"volume\" :1000 }  \n" +
			", { \"Timestamp\" :1461060202,\"close\" :644.0000,\"high\" :644.0000,\"low\" :644.0000,\"open\" :644.0000,\"volume\" :700 }  \n" +
			", { \"Timestamp\" :1461060464,\"close\" :644.0000,\"high\" :644.0000,\"low\" :644.0000,\"open\" :644.0000,\"volume\" :600 }  \n" +
			", { \"Timestamp\" :1461061038,\"close\" :643.0000,\"high\" :643.5000,\"low\" :643.0000,\"open\" :643.5000,\"volume\" :1300 }  \n" +
			", { \"Timestamp\" :1461061477,\"close\" :641.0000,\"high\" :641.5000,\"low\" :641.0000,\"open\" :641.5000,\"volume\" :1500 }  \n" +
			", { \"Timestamp\" :1461061596,\"close\" :641.0000,\"high\" :641.0000,\"low\" :641.0000,\"open\" :641.0000,\"volume\" :1500 }  \n" +
			", { \"Timestamp\" :1461061921,\"close\" :641.0000,\"high\" :641.0000,\"low\" :641.0000,\"open\" :641.0000,\"volume\" :600 }  \n" +
			", { \"Timestamp\" :1461062246,\"close\" :641.0000,\"high\" :641.0000,\"low\" :641.0000,\"open\" :641.0000,\"volume\" :400 }  \n" +
			", { \"Timestamp\" :1461062579,\"close\" :641.0000,\"high\" :641.0000,\"low\" :641.0000,\"open\" :641.0000,\"volume\" :300 }  \n" +
			", { \"Timestamp\" :1461062908,\"close\" :641.5000,\"high\" :641.5000,\"low\" :641.5000,\"open\" :641.5000,\"volume\" :400 }  \n" +
			", { \"Timestamp\" :1461063599,\"close\" :640.5000,\"high\" :642.0000,\"low\" :640.5000,\"open\" :641.5000,\"volume\" :1900 }  \n" +
			", { \"Timestamp\" :1461063744,\"close\" :641.0000,\"high\" :641.0000,\"low\" :641.0000,\"open\" :641.0000,\"volume\" :800 }  \n" +
			", { \"Timestamp\" :1461064031,\"close\" :641.0000,\"high\" :642.0000,\"low\" :641.0000,\"open\" :642.0000,\"volume\" :1200 }  \n" +
			", { \"Timestamp\" :1461064495,\"close\" :642.5000,\"high\" :642.5000,\"low\" :642.0000,\"open\" :642.0000,\"volume\" :1500 }  \n" +
			", { \"Timestamp\" :1461064610,\"close\" :642.5000,\"high\" :642.5000,\"low\" :642.0000,\"open\" :642.0000,\"volume\" :1000 }  \n" +
			", { \"Timestamp\" :1461064905,\"close\" :642.0000,\"high\" :642.0000,\"low\" :642.0000,\"open\" :642.0000,\"volume\" :2300 }  \n" +
			", { \"Timestamp\" :1461065305,\"close\" :642.0000,\"high\" :642.0000,\"low\" :641.5000,\"open\" :642.0000,\"volume\" :1700 }  \n" +
			", { \"Timestamp\" :1461065583,\"close\" :641.0000,\"high\" :641.5000,\"low\" :641.0000,\"open\" :641.5000,\"volume\" :2200 }  \n" +
			", { \"Timestamp\" :1461065966,\"close\" :640.5000,\"high\" :641.0000,\"low\" :640.5000,\"open\" :641.0000,\"volume\" :900 }  \n" +
			", { \"Timestamp\" :1461066260,\"close\" :640.5000,\"high\" :640.5000,\"low\" :639.5000,\"open\" :639.5000,\"volume\" :2600 }  \n" +
			", { \"Timestamp\" :1461066426,\"close\" :641.0000,\"high\" :641.0000,\"low\" :641.0000,\"open\" :641.0000,\"volume\" :300 }  \n" +
			", { \"Timestamp\" :1461066866,\"close\" :641.5000,\"high\" :641.5000,\"low\" :641.0000,\"open\" :641.0000,\"volume\" :300 }  \n" +
			", { \"Timestamp\" :1461066931,\"close\" :641.5000,\"high\" :641.5000,\"low\" :641.5000,\"open\" :641.5000,\"volume\" :700 }  \n" +
			", { \"Timestamp\" :1461067382,\"close\" :641.5000,\"high\" :641.5000,\"low\" :641.5000,\"open\" :641.5000,\"volume\" :0 }  \n" +
			", { \"Timestamp\" :1461067575,\"close\" :642.0000,\"high\" :642.0000,\"low\" :642.0000,\"open\" :642.0000,\"volume\" :0 }  \n" +
			", { \"Timestamp\" :1461068220,\"close\" :642.0000,\"high\" :642.0000,\"low\" :642.0000,\"open\" :642.0000,\"volume\" :400 }  \n" +
			", { \"Timestamp\" :1461068475,\"close\" :642.0000,\"high\" :642.0000,\"low\" :641.5000,\"open\" :641.5000,\"volume\" :4400 }  \n" +
			", { \"Timestamp\" :1461069195,\"close\" :641.5000,\"high\" :641.5000,\"low\" :641.5000,\"open\" :641.5000,\"volume\" :1400 }  \n" +
			", { \"Timestamp\" :1461069435,\"close\" :641.0000,\"high\" :641.0000,\"low\" :641.0000,\"open\" :641.0000,\"volume\" :1500 }  \n" +
			", { \"Timestamp\" :1461070155,\"close\" :641.0000,\"high\" :641.5000,\"low\" :640.5000,\"open\" :641.0000,\"volume\" :4100 }  \n" +
			", { \"Timestamp\" :1461070362,\"close\" :641.0000,\"high\" :641.0000,\"low\" :641.0000,\"open\" :641.0000,\"volume\" :0 }  \n" +
			", { \"Timestamp\" :1461070787,\"close\" :641.5000,\"high\" :641.5000,\"low\" :641.0000,\"open\" :641.0000,\"volume\" :2300 }  \n" +
			", { \"Timestamp\" :1461071091,\"close\" :641.0000,\"high\" :641.0000,\"low\" :641.0000,\"open\" :641.0000,\"volume\" :3100 }  \n" +
			", { \"Timestamp\" :1461071171,\"close\" :641.5000,\"high\" :641.5000,\"low\" :641.5000,\"open\" :641.5000,\"volume\" :600 }  \n" +
			", { \"Timestamp\" :1461071661,\"close\" :641.0000,\"high\" :641.0000,\"low\" :641.0000,\"open\" :641.0000,\"volume\" :1600 }  \n" +
			", { \"Timestamp\" :1461071701,\"close\" :641.0000,\"high\" :641.0000,\"low\" :641.0000,\"open\" :641.0000,\"volume\" :0 }  \n" +
			", { \"Timestamp\" :1461073637,\"close\" :639.5000,\"high\" :639.5000,\"low\" :639.5000,\"open\" :639.5000,\"volume\" :227500 }  \n" +
			", { \"Timestamp\" :1461073948,\"close\" :639.0000,\"high\" :639.0000,\"low\" :639.0000,\"open\" :639.0000,\"volume\" :0 }  \n" +
			", { \"Timestamp\" :1461074138,\"close\" :639.5000,\"high\" :639.5000,\"low\" :639.5000,\"open\" :639.5000,\"volume\" :9800 }  \n" +
			", { \"Timestamp\" :1461074646,\"close\" :639.0000,\"high\" :639.0000,\"low\" :639.0000,\"open\" :639.0000,\"volume\" :15200 }  \n" +
			", { \"Timestamp\" :1461074762,\"close\" :639.0000,\"high\" :639.0000,\"low\" :639.0000,\"open\" :639.0000,\"volume\" :500 }  \n" +
			", { \"Timestamp\" :1461075282,\"close\" :639.5000,\"high\" :639.5000,\"low\" :639.5000,\"open\" :639.5000,\"volume\" :5900 }  \n" +
			", { \"Timestamp\" :1461075561,\"close\" :639.0000,\"high\" :639.0000,\"low\" :639.0000,\"open\" :639.0000,\"volume\" :4700 }  \n" +
			", { \"Timestamp\" :1461075864,\"close\" :639.5000,\"high\" :639.5000,\"low\" :639.5000,\"open\" :639.5000,\"volume\" :1900 }  \n" +
			", { \"Timestamp\" :1461076157,\"close\" :639.5000,\"high\" :639.5000,\"low\" :639.5000,\"open\" :639.5000,\"volume\" :1100 }  \n" +
			", { \"Timestamp\" :1461076350,\"close\" :639.5000,\"high\" :639.5000,\"low\" :639.5000,\"open\" :639.5000,\"volume\" :9300 }  \n" +
			", { \"Timestamp\" :1461076708,\"close\" :639.0000,\"high\" :639.0000,\"low\" :639.0000,\"open\" :639.0000,\"volume\" :6200 }  \n" +
			", { \"Timestamp\" :1461077010,\"close\" :639.0000,\"high\" :639.0000,\"low\" :638.5500,\"open\" :638.5500,\"volume\" :100 }  \n" +
			", { \"Timestamp\" :1461077250,\"close\" :639.0000,\"high\" :639.5000,\"low\" :639.0000,\"open\" :639.0000,\"volume\" :3400 }  \n" +
			", { \"Timestamp\" :1461077580,\"close\" :639.0000,\"high\" :639.5000,\"low\" :639.0000,\"open\" :639.5000,\"volume\" :13900 }  \n" +
			", { \"Timestamp\" :1461077717,\"close\" :639.0000,\"high\" :639.0000,\"low\" :639.0000,\"open\" :639.0000,\"volume\" :3100 }  \n" +
			", { \"Timestamp\" :1461078235,\"close\" :639.0000,\"high\" :639.0000,\"low\" :638.8182,\"open\" :638.8182,\"volume\" :2000 }  \n" +
			", { \"Timestamp\" :1461078546,\"close\" :639.0000,\"high\" :639.0000,\"low\" :639.0000,\"open\" :639.0000,\"volume\" :2100 }  \n" +
			", { \"Timestamp\" :1461078847,\"close\" :637.5000,\"high\" :638.5000,\"low\" :637.5000,\"open\" :638.0000,\"volume\" :22300 }  \n" +
			", { \"Timestamp\" :1461079111,\"close\" :636.5000,\"high\" :637.5000,\"low\" :636.5000,\"open\" :637.5000,\"volume\" :5000 }  \n" +
			", { \"Timestamp\" :1461079448,\"close\" :637.0000,\"high\" :637.5000,\"low\" :637.0000,\"open\" :637.0000,\"volume\" :6200 }  \n" +
			", { \"Timestamp\" :1461079740,\"close\" :637.5000,\"high\" :638.0000,\"low\" :637.0000,\"open\" :637.5000,\"volume\" :10500 }  \n" +
			", { \"Timestamp\" :1461079800,\"close\" :639.5000,\"high\" :639.5000,\"low\" :639.5000,\"open\" :639.5000,\"volume\" :0 }  \n" +
			", { \"Timestamp\" :1461136158,\"close\" :635.0000,\"high\" :635.5000,\"low\" :634.5000,\"open\" :634.5000,\"volume\" :1900 }  \n" +
			", { \"Timestamp\" :1461136258,\"close\" :636.5000,\"high\" :636.5000,\"low\" :636.5000,\"open\" :636.5000,\"volume\" :100 }  \n" +
			", { \"Timestamp\" :1461136664,\"close\" :636.0000,\"high\" :636.0000,\"low\" :636.0000,\"open\" :636.0000,\"volume\" :400 }  \n" +
			", { \"Timestamp\" :1461136931,\"close\" :635.0000,\"high\" :635.0000,\"low\" :635.0000,\"open\" :635.0000,\"volume\" :300 }  \n" +
			", { \"Timestamp\" :1461137327,\"close\" :635.5000,\"high\" :635.5000,\"low\" :635.0000,\"open\" :635.0000,\"volume\" :800 }  \n" +
			", { \"Timestamp\" :1461137524,\"close\" :635.0000,\"high\" :635.0000,\"low\" :635.0000,\"open\" :635.0000,\"volume\" :1400 }  \n" +
			", { \"Timestamp\" :1461137902,\"close\" :635.0000,\"high\" :635.0000,\"low\" :635.0000,\"open\" :635.0000,\"volume\" :1300 }  \n" +
			", { \"Timestamp\" :1461138060,\"close\" :635.0000,\"high\" :635.0000,\"low\" :635.0000,\"open\" :635.0000,\"volume\" :700 }  \n" +
			", { \"Timestamp\" :1461138861,\"close\" :634.0000,\"high\" :634.5000,\"low\" :634.0000,\"open\" :634.5000,\"volume\" :1200 }  \n" +
			", { \"Timestamp\" :1461139303,\"close\" :635.5000,\"high\" :635.5000,\"low\" :635.5000,\"open\" :635.5000,\"volume\" :200 }  \n" +
			", { \"Timestamp\" :1461139541,\"close\" :635.5000,\"high\" :635.5000,\"low\" :635.5000,\"open\" :635.5000,\"volume\" :1100 }  \n" +
			", { \"Timestamp\" :1461140062,\"close\" :636.5000,\"high\" :636.5000,\"low\" :636.0000,\"open\" :636.0000,\"volume\" :0 }  \n" +
			", { \"Timestamp\" :1461140972,\"close\" :636.5000,\"high\" :637.5000,\"low\" :636.5000,\"open\" :637.0000,\"volume\" :2200 }  \n" +
			", { \"Timestamp\" :1461141409,\"close\" :635.5000,\"high\" :635.5000,\"low\" :635.0000,\"open\" :635.0000,\"volume\" :400 }  \n" +
			", { \"Timestamp\" :1461141932,\"close\" :635.0000,\"high\" :635.0000,\"low\" :635.0000,\"open\" :635.0000,\"volume\" :0 }  \n" +
			", { \"Timestamp\" :1461142616,\"close\" :633.5000,\"high\" :633.5000,\"low\" :633.5000,\"open\" :633.5000,\"volume\" :400 }  \n" +
			", { \"Timestamp\" :1461142954,\"close\" :633.5000,\"high\" :634.0000,\"low\" :633.5000,\"open\" :633.5000,\"volume\" :2000 }  \n" +
			", { \"Timestamp\" :1461143320,\"close\" :633.5000,\"high\" :633.5000,\"low\" :633.0000,\"open\" :633.0000,\"volume\" :100 }  \n" +
			", { \"Timestamp\" :1461143471,\"close\" :633.5000,\"high\" :633.5000,\"low\" :633.5000,\"open\" :633.5000,\"volume\" :200 }  \n" +
			", { \"Timestamp\" :1461143987,\"close\" :633.5000,\"high\" :633.5000,\"low\" :633.5000,\"open\" :633.5000,\"volume\" :0 }  \n" +
			", { \"Timestamp\" :1461144572,\"close\" :632.0000,\"high\" :633.5000,\"low\" :632.0000,\"open\" :633.5000,\"volume\" :1200 }  \n" +
			", { \"Timestamp\" :1461144966,\"close\" :632.0000,\"high\" :632.0000,\"low\" :632.0000,\"open\" :632.0000,\"volume\" :200 }  \n" +
			", { \"Timestamp\" :1461146280,\"close\" :632.4500,\"high\" :633.0000,\"low\" :632.4500,\"open\" :632.5000,\"volume\" :1600 }  \n" +
			", { \"Timestamp\" :1461147062,\"close\" :632.5000,\"high\" :632.5000,\"low\" :632.5000,\"open\" :632.5000,\"volume\" :0 }  \n" +
			", { \"Timestamp\" :1461147877,\"close\" :631.0000,\"high\" :632.5000,\"low\" :631.0000,\"open\" :632.5000,\"volume\" :2100 }  \n" +
			", { \"Timestamp\" :1461147930,\"close\" :631.5000,\"high\" :631.5000,\"low\" :631.5000,\"open\" :631.5000,\"volume\" :500 }  \n" +
			", { \"Timestamp\" :1461148353,\"close\" :630.5000,\"high\" :631.0000,\"low\" :629.5000,\"open\" :631.0000,\"volume\" :2200 }  \n" +
			", { \"Timestamp\" :1461148693,\"close\" :630.0000,\"high\" :630.5000,\"low\" :630.0000,\"open\" :630.0000,\"volume\" :1900 }  \n" +
			", { \"Timestamp\" :1461148949,\"close\" :630.0000,\"high\" :630.0000,\"low\" :630.0000,\"open\" :630.0000,\"volume\" :1200 }  \n" +
			", { \"Timestamp\" :1461149279,\"close\" :630.5000,\"high\" :630.5000,\"low\" :630.5000,\"open\" :630.5000,\"volume\" :300 }  \n" +
			", { \"Timestamp\" :1461149524,\"close\" :632.0000,\"high\" :632.0000,\"low\" :630.5000,\"open\" :631.0000,\"volume\" :3200 }  \n" +
			", { \"Timestamp\" :1461149911,\"close\" :632.5000,\"high\" :632.5000,\"low\" :631.5000,\"open\" :631.5000,\"volume\" :1200 }  \n" +
			", { \"Timestamp\" :1461150298,\"close\" :633.0000,\"high\" :633.0000,\"low\" :631.0000,\"open\" :631.0000,\"volume\" :1700 }  \n" +
			", { \"Timestamp\" :1461150543,\"close\" :633.0000,\"high\" :634.5000,\"low\" :633.0000,\"open\" :633.5000,\"volume\" :2800 }  \n" +
			", { \"Timestamp\" :1461150781,\"close\" :633.0000,\"high\" :633.0000,\"low\" :633.0000,\"open\" :633.0000,\"volume\" :300 }  \n" +
			", { \"Timestamp\" :1461151160,\"close\" :632.0000,\"high\" :632.5000,\"low\" :632.0000,\"open\" :632.5000,\"volume\" :1400 }  \n" +
			", { \"Timestamp\" :1461151475,\"close\" :631.0000,\"high\" :632.5000,\"low\" :631.0000,\"open\" :632.0000,\"volume\" :7500 }  \n" +
			", { \"Timestamp\" :1461151535,\"close\" :632.0000,\"high\" :632.0000,\"low\" :632.0000,\"open\" :632.0000,\"volume\" :2100 }  \n" +
			", { \"Timestamp\" :1461152048,\"close\" :630.5000,\"high\" :633.0000,\"low\" :630.5000,\"open\" :632.5000,\"volume\" :7600 }  \n" +
			", { \"Timestamp\" :1461152111,\"close\" :631.5000,\"high\" :631.5000,\"low\" :631.5000,\"open\" :631.5000,\"volume\" :7600 }  \n" +
			", { \"Timestamp\" :1461152951,\"close\" :630.5000,\"high\" :632.0000,\"low\" :630.5000,\"open\" :632.0000,\"volume\" :0 }  \n" +
			", { \"Timestamp\" :1461153297,\"close\" :630.5000,\"high\" :630.5000,\"low\" :630.5000,\"open\" :630.5000,\"volume\" :16600 }  \n" +
			", { \"Timestamp\" :1461153565,\"close\" :630.0000,\"high\" :630.0000,\"low\" :630.0000,\"open\" :630.0000,\"volume\" :0 }  \n" +
			", { \"Timestamp\" :1461153876,\"close\" :631.0000,\"high\" :631.0000,\"low\" :631.0000,\"open\" :631.0000,\"volume\" :2200 }  \n" +
			", { \"Timestamp\" :1461154000,\"close\" :630.5000,\"high\" :630.5000,\"low\" :630.5000,\"open\" :630.5000,\"volume\" :0 }  \n" +
			", { \"Timestamp\" :1461154470,\"close\" :630.5000,\"high\" :630.5000,\"low\" :630.5000,\"open\" :630.5000,\"volume\" :300 }  \n" +
			", { \"Timestamp\" :1461155393,\"close\" :630.0000,\"high\" :630.5000,\"low\" :630.0000,\"open\" :630.5000,\"volume\" :2600 }  \n" +
			", { \"Timestamp\" :1461155641,\"close\" :629.5000,\"high\" :629.5000,\"low\" :629.0000,\"open\" :629.0000,\"volume\" :800 }  \n" +
			", { \"Timestamp\" :1461155885,\"close\" :629.5000,\"high\" :629.5000,\"low\" :629.0000,\"open\" :629.0000,\"volume\" :200 }  \n" +
			", { \"Timestamp\" :1461156267,\"close\" :629.5000,\"high\" :629.5000,\"low\" :629.5000,\"open\" :629.5000,\"volume\" :0 }  \n" +
			", { \"Timestamp\" :1461156782,\"close\" :630.0000,\"high\" :630.0000,\"low\" :630.0000,\"open\" :630.0000,\"volume\" :2400 }  \n" +
			", { \"Timestamp\" :1461157117,\"close\" :630.0000,\"high\" :630.5000,\"low\" :630.0000,\"open\" :630.0000,\"volume\" :17400 }  \n" +
			", { \"Timestamp\" :1461157457,\"close\" :630.5500,\"high\" :630.5500,\"low\" :630.5000,\"open\" :630.5000,\"volume\" :17600 }  \n" +
			", { \"Timestamp\" :1461157780,\"close\" :630.5000,\"high\" :630.5000,\"low\" :630.5000,\"open\" :630.5000,\"volume\" :1100 }  \n" +
			", { \"Timestamp\" :1461158303,\"close\" :630.0000,\"high\" :630.0000,\"low\" :630.0000,\"open\" :630.0000,\"volume\" :0 }  \n" +
			", { \"Timestamp\" :1461158832,\"close\" :630.0000,\"high\" :630.0000,\"low\" :630.0000,\"open\" :630.0000,\"volume\" :0 }  \n" +
			", { \"Timestamp\" :1461159263,\"close\" :629.0000,\"high\" :629.0000,\"low\" :629.0000,\"open\" :629.0000,\"volume\" :1000 }  \n" +
			", { \"Timestamp\" :1461159802,\"close\" :628.5000,\"high\" :629.0000,\"low\" :628.5000,\"open\" :628.5000,\"volume\" :2400 }  \n" +
			", { \"Timestamp\" :1461160596,\"close\" :629.5000,\"high\" :629.5000,\"low\" :629.5000,\"open\" :629.5000,\"volume\" :19400 }  \n" +
			", { \"Timestamp\" :1461161372,\"close\" :629.0000,\"high\" :629.0000,\"low\" :629.0000,\"open\" :629.0000,\"volume\" :500 }  \n" +
			", { \"Timestamp\" :1461161971,\"close\" :628.5000,\"high\" :629.0000,\"low\" :628.5000,\"open\" :629.0000,\"volume\" :0 }  \n" +
			", { \"Timestamp\" :1461162219,\"close\" :629.0000,\"high\" :629.0000,\"low\" :628.5000,\"open\" :628.5000,\"volume\" :19200 }  \n" +
			", { \"Timestamp\" :1461162595,\"close\" :629.5000,\"high\" :629.5000,\"low\" :629.0000,\"open\" :629.0000,\"volume\" :0 }  \n" +
			", { \"Timestamp\" :1461162775,\"close\" :629.5000,\"high\" :629.5000,\"low\" :629.5000,\"open\" :629.5000,\"volume\" :2000 }  \n" +
			", { \"Timestamp\" :1461163419,\"close\" :629.0000,\"high\" :629.0000,\"low\" :629.0000,\"open\" :629.0000,\"volume\" :6500 }  \n" +
			", { \"Timestamp\" :1461163719,\"close\" :629.0000,\"high\" :629.0000,\"low\" :629.0000,\"open\" :629.0000,\"volume\" :3500 }  \n" +
			", { \"Timestamp\" :1461164019,\"close\" :629.0000,\"high\" :629.0500,\"low\" :629.0000,\"open\" :629.0500,\"volume\" :7000 }  \n" +
			", { \"Timestamp\" :1461164318,\"close\" :629.5000,\"high\" :629.5000,\"low\" :629.0000,\"open\" :629.0000,\"volume\" :7500 }  \n" +
			", { \"Timestamp\" :1461164644,\"close\" :629.5000,\"high\" :629.5000,\"low\" :629.0000,\"open\" :629.0000,\"volume\" :1300 }  \n" +
			", { \"Timestamp\" :1461164888,\"close\" :629.5000,\"high\" :630.0000,\"low\" :629.0000,\"open\" :629.0000,\"volume\" :8000 }  \n" +
			", { \"Timestamp\" :1461165219,\"close\" :630.0000,\"high\" :630.5000,\"low\" :629.5000,\"open\" :629.5000,\"volume\" :9700 }  \n" +
			", { \"Timestamp\" :1461165519,\"close\" :630.0000,\"high\" :630.5000,\"low\" :630.0000,\"open\" :630.5000,\"volume\" :2400 }  \n" +
			", { \"Timestamp\" :1461165841,\"close\" :631.5000,\"high\" :631.5000,\"low\" :631.5000,\"open\" :631.5000,\"volume\" :4200 }  \n" +
			", { \"Timestamp\" :1461166167,\"close\" :631.5000,\"high\" :632.0000,\"low\" :631.0000,\"open\" :631.0000,\"volume\" :7200 }  \n" +
			", { \"Timestamp\" :1461166200,\"close\" :632.0000,\"high\" :632.0000,\"low\" :632.0000,\"open\" :632.0000,\"volume\" :0 }  \n" +
			", { \"Timestamp\" :1461222233,\"close\" :632.5000,\"high\" :634.5000,\"low\" :632.0000,\"open\" :634.5000,\"volume\" :700 }  \n" +
			", { \"Timestamp\" :1461222797,\"close\" :633.5000,\"high\" :634.5000,\"low\" :633.5000,\"open\" :634.5000,\"volume\" :1000 }  \n" +
			", { \"Timestamp\" :1461223098,\"close\" :630.5000,\"high\" :631.5000,\"low\" :630.5000,\"open\" :631.5000,\"volume\" :2400 }  \n" +
			", { \"Timestamp\" :1461223245,\"close\" :630.0000,\"high\" :630.0000,\"low\" :630.0000,\"open\" :630.0000,\"volume\" :0 }  \n" +
			", { \"Timestamp\" :1461223621,\"close\" :627.5000,\"high\" :628.5000,\"low\" :627.5000,\"open\" :628.5000,\"volume\" :500 }  \n" +
			", { \"Timestamp\" :1461224027,\"close\" :630.0000,\"high\" :631.0000,\"low\" :628.5000,\"open\" :628.5000,\"volume\" :4400 }  \n" +
			", { \"Timestamp\" :1461224279,\"close\" :630.0000,\"high\" :630.0000,\"low\" :630.0000,\"open\" :630.0000,\"volume\" :500 }  \n" +
			", { \"Timestamp\" :1461224408,\"close\" :629.5000,\"high\" :629.5000,\"low\" :629.5000,\"open\" :629.5000,\"volume\" :100 }  \n" +
			", { \"Timestamp\" :1461224749,\"close\" :631.0000,\"high\" :631.0000,\"low\" :631.0000,\"open\" :631.0000,\"volume\" :1000 }  \n" +
			", { \"Timestamp\" :1461225195,\"close\" :630.0000,\"high\" :631.0000,\"low\" :630.0000,\"open\" :631.0000,\"volume\" :900 }  \n" +
			", { \"Timestamp\" :1461226337,\"close\" :631.0000,\"high\" :631.0000,\"low\" :631.0000,\"open\" :631.0000,\"volume\" :0 }  \n" +
			", { \"Timestamp\" :1461227014,\"close\" :630.0000,\"high\" :630.0000,\"low\" :629.5000,\"open\" :629.5000,\"volume\" :1400 }  \n" +
			", { \"Timestamp\" :1461227276,\"close\" :629.5000,\"high\" :629.5000,\"low\" :629.5000,\"open\" :629.5000,\"volume\" :0 }  \n" +
			", { \"Timestamp\" :1461227579,\"close\" :629.0000,\"high\" :629.0000,\"low\" :629.0000,\"open\" :629.0000,\"volume\" :0 }  \n" +
			", { \"Timestamp\" :1461227726,\"close\" :629.1000,\"high\" :629.1000,\"low\" :629.1000,\"open\" :629.1000,\"volume\" :1300 }  \n" +
			", { \"Timestamp\" :1461228565,\"close\" :630.0000,\"high\" :630.0000,\"low\" :630.0000,\"open\" :630.0000,\"volume\" :0 }  \n" +
			", { \"Timestamp\" :1461228712,\"close\" :629.1000,\"high\" :629.1000,\"low\" :629.1000,\"open\" :629.1000,\"volume\" :1800 }  \n" +
			", { \"Timestamp\" :1461229569,\"close\" :628.5000,\"high\" :628.5000,\"low\" :628.5000,\"open\" :628.5000,\"volume\" :100 }  \n" +
			", { \"Timestamp\" :1461230470,\"close\" :628.5000,\"high\" :628.5000,\"low\" :628.5000,\"open\" :628.5000,\"volume\" :1400 }  \n" +
			", { \"Timestamp\" :1461231122,\"close\" :628.0000,\"high\" :628.5800,\"low\" :628.0000,\"open\" :628.5800,\"volume\" :3900 }  \n" +
			", { \"Timestamp\" :1461231451,\"close\" :628.0000,\"high\" :628.0000,\"low\" :628.0000,\"open\" :628.0000,\"volume\" :4600 }  \n" +
			", { \"Timestamp\" :1461231884,\"close\" :628.0000,\"high\" :628.0000,\"low\" :628.0000,\"open\" :628.0000,\"volume\" :0 }  \n" +
			", { \"Timestamp\" :1461232446,\"close\" :628.0000,\"high\" :628.0000,\"low\" :627.5000,\"open\" :627.5000,\"volume\" :4400 }  \n" +
			", { \"Timestamp\" :1461232965,\"close\" :627.5000,\"high\" :627.5000,\"low\" :627.5000,\"open\" :627.5000,\"volume\" :4700 }  \n" +
			", { \"Timestamp\" :1461233344,\"close\" :627.0000,\"high\" :627.0000,\"low\" :627.0000,\"open\" :627.0000,\"volume\" :0 }  \n" +
			", { \"Timestamp\" :1461233616,\"close\" :627.0000,\"high\" :627.5000,\"low\" :627.0000,\"open\" :627.5000,\"volume\" :500 }  \n" +
			", { \"Timestamp\" :1461233821,\"close\" :625.5000,\"high\" :627.0000,\"low\" :625.5000,\"open\" :627.0000,\"volume\" :8600 }  \n" +
			", { \"Timestamp\" :1461234109,\"close\" :625.5000,\"high\" :625.5000,\"low\" :625.5000,\"open\" :625.5000,\"volume\" :2000 }  \n" +
			", { \"Timestamp\" :1461234485,\"close\" :627.0000,\"high\" :627.0000,\"low\" :626.0000,\"open\" :626.0000,\"volume\" :600 }  \n" +
			", { \"Timestamp\" :1461234613,\"close\" :627.0000,\"high\" :627.0000,\"low\" :627.0000,\"open\" :627.0000,\"volume\" :600 }  \n" +
			", { \"Timestamp\" :1461235035,\"close\" :627.0000,\"high\" :627.0000,\"low\" :626.5000,\"open\" :626.5000,\"volume\" :600 }  \n" +
			", { \"Timestamp\" :1461235440,\"close\" :627.0000,\"high\" :627.5000,\"low\" :627.0000,\"open\" :627.5000,\"volume\" :300 }  \n" +
			", { \"Timestamp\" :1461236149,\"close\" :627.5000,\"high\" :627.5000,\"low\" :627.5000,\"open\" :627.5000,\"volume\" :8000 }  \n" +
			", { \"Timestamp\" :1461236546,\"close\" :629.0000,\"high\" :629.0000,\"low\" :628.5000,\"open\" :629.0000,\"volume\" :0 }  \n" +
			", { \"Timestamp\" :1461236920,\"close\" :627.5000,\"high\" :628.5000,\"low\" :627.5000,\"open\" :628.5000,\"volume\" :2000 }  \n" +
			", { \"Timestamp\" :1461237057,\"close\" :628.0000,\"high\" :628.0000,\"low\" :628.0000,\"open\" :628.0000,\"volume\" :0 }  \n" +
			", { \"Timestamp\" :1461237336,\"close\" :627.5000,\"high\" :627.5000,\"low\" :627.5000,\"open\" :627.5000,\"volume\" :100 }  \n" +
			", { \"Timestamp\" :1461237873,\"close\" :628.0000,\"high\" :628.0000,\"low\" :628.0000,\"open\" :628.0000,\"volume\" :9500 }  \n" +
			", { \"Timestamp\" :1461238015,\"close\" :628.0000,\"high\" :628.0000,\"low\" :628.0000,\"open\" :628.0000,\"volume\" :0 }  \n" +
			", { \"Timestamp\" :1461238305,\"close\" :628.5000,\"high\" :628.5000,\"low\" :628.5000,\"open\" :628.5000,\"volume\" :200 }  \n" +
			", { \"Timestamp\" :1461238751,\"close\" :628.5000,\"high\" :628.5000,\"low\" :628.5000,\"open\" :628.5000,\"volume\" :2700 }  \n" +
			", { \"Timestamp\" :1461238920,\"close\" :629.0000,\"high\" :629.0000,\"low\" :628.5000,\"open\" :628.5000,\"volume\" :700 }  \n" +
			", { \"Timestamp\" :1461239352,\"close\" :628.5000,\"high\" :628.5000,\"low\" :628.5000,\"open\" :628.5000,\"volume\" :200 }  \n" +
			", { \"Timestamp\" :1461239674,\"close\" :627.5000,\"high\" :628.5000,\"low\" :627.5000,\"open\" :628.5000,\"volume\" :800 }  \n" +
			", { \"Timestamp\" :1461240181,\"close\" :629.0000,\"high\" :629.0000,\"low\" :628.0000,\"open\" :628.0000,\"volume\" :700 }  \n" +
			", { \"Timestamp\" :1461240412,\"close\" :628.5000,\"high\" :628.5000,\"low\" :628.5000,\"open\" :628.5000,\"volume\" :300 }  \n" +
			", { \"Timestamp\" :1461240794,\"close\" :629.5000,\"high\" :629.5000,\"low\" :629.0000,\"open\" :629.0000,\"volume\" :100 }  \n" +
			", { \"Timestamp\" :1461240903,\"close\" :629.0000,\"high\" :629.5000,\"low\" :629.0000,\"open\" :629.5000,\"volume\" :1500 }  \n" +
			", { \"Timestamp\" :1461241375,\"close\" :629.0000,\"high\" :629.0000,\"low\" :628.5000,\"open\" :628.5000,\"volume\" :2200 }  \n" +
			", { \"Timestamp\" :1461241796,\"close\" :629.5000,\"high\" :629.5000,\"low\" :628.5000,\"open\" :628.5000,\"volume\" :1900 }  \n" +
			", { \"Timestamp\" :1461241861,\"close\" :629.5000,\"high\" :629.5000,\"low\" :629.0000,\"open\" :629.0000,\"volume\" :2300 }  \n" +
			", { \"Timestamp\" :1461242384,\"close\" :630.0000,\"high\" :630.5000,\"low\" :630.0000,\"open\" :630.5000,\"volume\" :4000 }  \n" +
			", { \"Timestamp\" :1461242648,\"close\" :629.0000,\"high\" :630.0000,\"low\" :629.0000,\"open\" :630.0000,\"volume\" :4100 }  \n" +
			", { \"Timestamp\" :1461242984,\"close\" :628.5000,\"high\" :629.0000,\"low\" :628.5000,\"open\" :629.0000,\"volume\" :2300 }  \n" +
			", { \"Timestamp\" :1461243255,\"close\" :627.5000,\"high\" :627.5000,\"low\" :627.5000,\"open\" :627.5000,\"volume\" :1500 }  \n" +
			", { \"Timestamp\" :1461243856,\"close\" :627.5000,\"high\" :628.5000,\"low\" :627.5000,\"open\" :628.5000,\"volume\" :3200 }  \n" +
			", { \"Timestamp\" :1461244249,\"close\" :628.5000,\"high\" :628.5000,\"low\" :628.5000,\"open\" :628.5000,\"volume\" :1800 }  \n" +
			", { \"Timestamp\" :1461244767,\"close\" :628.5000,\"high\" :628.5000,\"low\" :628.5000,\"open\" :628.5000,\"volume\" :1900 }  \n" +
			", { \"Timestamp\" :1461244863,\"close\" :629.0000,\"high\" :629.0000,\"low\" :629.0000,\"open\" :629.0000,\"volume\" :130100 }  \n" +
			", { \"Timestamp\" :1461245374,\"close\" :627.5000,\"high\" :627.5000,\"low\" :627.5000,\"open\" :627.5000,\"volume\" :0 }  \n" +
			", { \"Timestamp\" :1461245622,\"close\" :628.0000,\"high\" :628.0000,\"low\" :627.5000,\"open\" :628.0000,\"volume\" :1600 }  \n" +
			", { \"Timestamp\" :1461245829,\"close\" :627.0000,\"high\" :627.0000,\"low\" :627.0000,\"open\" :627.0000,\"volume\" :2700 }  \n" +
			", { \"Timestamp\" :1461247761,\"close\" :626.5000,\"high\" :626.5000,\"low\" :626.5000,\"open\" :626.5000,\"volume\" :6900 }  \n" +
			", { \"Timestamp\" :1461248321,\"close\" :625.5000,\"high\" :626.0000,\"low\" :625.5000,\"open\" :626.0000,\"volume\" :4300 }  \n" +
			", { \"Timestamp\" :1461248649,\"close\" :627.0000,\"high\" :627.0000,\"low\" :626.5000,\"open\" :626.5000,\"volume\" :2800 }  \n" +
			", { \"Timestamp\" :1461249559,\"close\" :627.5000,\"high\" :628.0000,\"low\" :627.0000,\"open\" :627.0000,\"volume\" :12400 }  \n" +
			", { \"Timestamp\" :1461249680,\"close\" :626.5000,\"high\" :627.5000,\"low\" :626.5000,\"open\" :627.5000,\"volume\" :14200 }  \n" +
			", { \"Timestamp\" :1461250155,\"close\" :627.0000,\"high\" :627.0000,\"low\" :626.0000,\"open\" :626.0000,\"volume\" :2800 }  \n" +
			", { \"Timestamp\" :1461250754,\"close\" :624.5000,\"high\" :625.5000,\"low\" :624.5000,\"open\" :625.5000,\"volume\" :9700 }  \n" +
			", { \"Timestamp\" :1461250929,\"close\" :625.0000,\"high\" :625.0000,\"low\" :625.0000,\"open\" :625.0000,\"volume\" :3800 }  \n" +
			", { \"Timestamp\" :1461251644,\"close\" :626.0000,\"high\" :626.0000,\"low\" :624.5000,\"open\" :625.0000,\"volume\" :7800 }  \n" +
			", { \"Timestamp\" :1461251852,\"close\" :625.5000,\"high\" :625.5000,\"low\" :625.5000,\"open\" :625.5000,\"volume\" :8200 }  \n" +
			", { \"Timestamp\" :1461252229,\"close\" :624.0000,\"high\" :626.5000,\"low\" :624.0000,\"open\" :626.0000,\"volume\" :46600 }  \n" +
			", { \"Timestamp\" :1461252557,\"close\" :624.0000,\"high\" :624.5000,\"low\" :624.0000,\"open\" :624.0000,\"volume\" :21000 }  \n" +
			", { \"Timestamp\" :1461252600,\"close\" :625.0000,\"high\" :625.0000,\"low\" :625.0000,\"open\" :625.0000,\"volume\" :0 }  \n" +
			", { \"Timestamp\" :1461308400,\"close\" :623.0000,\"high\" :623.0000,\"low\" :623.0000,\"open\" :623.0000,\"volume\" :0 } \n" +
			", { \"Timestamp\" :1461308659,\"close\" :623.5000,\"high\" :625.0000,\"low\" :623.5000,\"open\" :625.0000,\"volume\" :3300 } \n" +
			", { \"Timestamp\" :1461308778,\"close\" :623.0000,\"high\" :623.0000,\"low\" :623.0000,\"open\" :623.0000,\"volume\" :200 } \n" +
			", { \"Timestamp\" :1461309078,\"close\" :622.0000,\"high\" :622.0000,\"low\" :622.0000,\"open\" :622.0000,\"volume\" :500 } \n" +
			", { \"Timestamp\" :1461309334,\"close\" :620.9900,\"high\" :620.9900,\"low\" :620.9900,\"open\" :620.9900,\"volume\" :10100 } \n" +
			", { \"Timestamp\" :1461309373,\"close\" :620.0000,\"high\" :621.5000,\"low\" :620.0000,\"open\" :621.5000,\"volume\" :600 } \n" +
			", { \"Timestamp\" :1461309640,\"close\" :621.5000,\"high\" :621.5000,\"low\" :621.0000,\"open\" :621.0000,\"volume\" :2100 } \n" +
			", { \"Timestamp\" :1461309750,\"close\" :621.0000,\"high\" :621.5000,\"low\" :621.0000,\"open\" :621.5000,\"volume\" :3100 } \n" +
			", { \"Timestamp\" :1461309861,\"close\" :621.0000,\"high\" :621.0000,\"low\" :621.0000,\"open\" :621.0000,\"volume\" :1400 } \n" +
			", { \"Timestamp\" :1461310151,\"close\" :622.0000,\"high\" :622.0000,\"low\" :621.5000,\"open\" :621.5000,\"volume\" :3000 } \n" +
			", { \"Timestamp\" :1461310215,\"close\" :622.5000,\"high\" :622.5000,\"low\" :622.5000,\"open\" :622.5000,\"volume\" :800 } \n" +
			", { \"Timestamp\" :1461310281,\"close\" :623.0000,\"high\" :623.5000,\"low\" :623.0000,\"open\" :623.0000,\"volume\" :3700 } \n" +
			", { \"Timestamp\" :1461310376,\"close\" :623.5000,\"high\" :623.5000,\"low\" :623.5000,\"open\" :623.5000,\"volume\" :200 } \n" +
			", { \"Timestamp\" :1461310391,\"close\" :622.5000,\"high\" :623.0000,\"low\" :622.5000,\"open\" :623.0000,\"volume\" :600 } \n" +
			", { \"Timestamp\" :1461310461,\"close\" :622.5000,\"high\" :622.5000,\"low\" :622.5000,\"open\" :622.5000,\"volume\" :500 } \n" +
			", { \"Timestamp\" :1461310546,\"close\" :622.5000,\"high\" :622.5000,\"low\" :622.5000,\"open\" :622.5000,\"volume\" :300 } \n" +
			", { \"Timestamp\" :1461310607,\"close\" :622.5000,\"high\" :622.5000,\"low\" :622.5000,\"open\" :622.5000,\"volume\" :600 } \n" +
			", { \"Timestamp\" :1461310734,\"close\" :621.5000,\"high\" :622.0000,\"low\" :621.5000,\"open\" :622.0000,\"volume\" :1900 } \n" +
			", { \"Timestamp\" :1461310827,\"close\" :622.0000,\"high\" :622.0000,\"low\" :622.0000,\"open\" :622.0000,\"volume\" :100 } \n" +
			", { \"Timestamp\" :1461311381,\"close\" :621.5000,\"high\" :621.5000,\"low\" :621.5000,\"open\" :621.5000,\"volume\" :100 } \n" +
			", { \"Timestamp\" :1461311460,\"close\" :621.5000,\"high\" :621.5000,\"low\" :621.5000,\"open\" :621.5000,\"volume\" :700 } \n" +
			", { \"Timestamp\" :1461311521,\"close\" :622.5000,\"high\" :622.5000,\"low\" :622.0000,\"open\" :622.0000,\"volume\" :700 } \n" +
			", { \"Timestamp\" :1461311704,\"close\" :622.5000,\"high\" :622.5000,\"low\" :622.5000,\"open\" :622.5000,\"volume\" :500 } \n" +
			", { \"Timestamp\" :1461311777,\"close\" :622.5000,\"high\" :622.5000,\"low\" :622.5000,\"open\" :622.5000,\"volume\" :200 } \n" +
			", { \"Timestamp\" :1461311861,\"close\" :622.0000,\"high\" :622.5000,\"low\" :622.0000,\"open\" :622.5000,\"volume\" :900 } \n" +
			", { \"Timestamp\" :1461312357,\"close\" :623.0000,\"high\" :623.0000,\"low\" :623.0000,\"open\" :623.0000,\"volume\" :800 } \n" +
			", { \"Timestamp\" :1461312557,\"close\" :623.5000,\"high\" :623.5000,\"low\" :623.5000,\"open\" :623.5000,\"volume\" :300 } \n" +
			", { \"Timestamp\" :1461312823,\"close\" :623.5000,\"high\" :623.5000,\"low\" :623.5000,\"open\" :623.5000,\"volume\" :200 } \n" +
			", { \"Timestamp\" :1461312924,\"close\" :623.5000,\"high\" :624.0000,\"low\" :623.5000,\"open\" :624.0000,\"volume\" :300 } \n" +
			", { \"Timestamp\" :1461313274,\"close\" :625.5000,\"high\" :625.5000,\"low\" :625.5000,\"open\" :625.5000,\"volume\" :300 } \n" +
			", { \"Timestamp\" :1461313470,\"close\" :625.0000,\"high\" :625.0000,\"low\" :625.0000,\"open\" :625.0000,\"volume\" :1400 } \n" +
			", { \"Timestamp\" :1461313599,\"close\" :625.5000,\"high\" :625.5000,\"low\" :625.5000,\"open\" :625.5000,\"volume\" :100 } \n" +
			", { \"Timestamp\" :1461314142,\"close\" :626.1000,\"high\" :626.5000,\"low\" :625.5000,\"open\" :625.5000,\"volume\" :10900 } \n" +
			", { \"Timestamp\" :1461314774,\"close\" :625.5000,\"high\" :626.5000,\"low\" :625.5000,\"open\" :626.5000,\"volume\" :0 } \n" +
			", { \"Timestamp\" :1461315031,\"close\" :625.0000,\"high\" :625.5000,\"low\" :625.0000,\"open\" :625.5000,\"volume\" :300 } \n" +
			", { \"Timestamp\" :1461315410,\"close\" :626.0000,\"high\" :626.0000,\"low\" :626.0000,\"open\" :626.0000,\"volume\" :400 } \n" +
			", { \"Timestamp\" :1461315660,\"close\" :626.0000,\"high\" :626.0000,\"low\" :626.0000,\"open\" :626.0000,\"volume\" :400 } \n" +
			", { \"Timestamp\" :1461316007,\"close\" :625.4000,\"high\" :625.4000,\"low\" :625.4000,\"open\" :625.4000,\"volume\" :14900 } \n" +
			", { \"Timestamp\" :1461316053,\"close\" :625.5000,\"high\" :625.5000,\"low\" :625.5000,\"open\" :625.5000,\"volume\" :0 } \n" +
			", { \"Timestamp\" :1461316120,\"close\" :626.0000,\"high\" :626.0000,\"low\" :625.5000,\"open\" :625.5000,\"volume\" :1500 } \n" +
			", { \"Timestamp\" :1461316170,\"close\" :626.0000,\"high\" :626.0000,\"low\" :626.0000,\"open\" :626.0000,\"volume\" :200 } \n" +
			", { \"Timestamp\" :1461316217,\"close\" :626.5000,\"high\" :626.5000,\"low\" :626.5000,\"open\" :626.5000,\"volume\" :600 } \n" +
			", { \"Timestamp\" :1461316274,\"close\" :626.5000,\"high\" :626.5000,\"low\" :626.5000,\"open\" :626.5000,\"volume\" :1100 } \n" +
			", { \"Timestamp\" :1461316412,\"close\" :626.5000,\"high\" :626.5000,\"low\" :626.5000,\"open\" :626.5000,\"volume\" :0 } \n" +
			", { \"Timestamp\" :1461316463,\"close\" :626.5000,\"high\" :626.5000,\"low\" :626.5000,\"open\" :626.5000,\"volume\" :700 } \n" +
			", { \"Timestamp\" :1461316763,\"close\" :628.5000,\"high\" :628.5000,\"low\" :628.5000,\"open\" :628.5000,\"volume\" :0 } \n" +
			", { \"Timestamp\" :1461316815,\"close\" :628.5000,\"high\" :628.5000,\"low\" :628.5000,\"open\" :628.5000,\"volume\" :0 } \n" +
			", { \"Timestamp\" :1461316948,\"close\" :629.0000,\"high\" :629.0000,\"low\" :629.0000,\"open\" :629.0000,\"volume\" :0 } \n" +
			", { \"Timestamp\" :1461317004,\"close\" :629.0000,\"high\" :629.0000,\"low\" :629.0000,\"open\" :629.0000,\"volume\" :100 } \n" +
			", { \"Timestamp\" :1461317128,\"close\" :629.0000,\"high\" :629.5000,\"low\" :629.0000,\"open\" :629.5000,\"volume\" :800 } \n" +
			", { \"Timestamp\" :1461317219,\"close\" :629.5000,\"high\" :629.5000,\"low\" :629.5000,\"open\" :629.5000,\"volume\" :800 } \n" +
			", { \"Timestamp\" :1461317422,\"close\" :629.5000,\"high\" :629.5000,\"low\" :629.5000,\"open\" :629.5000,\"volume\" :200 } \n" +
			", { \"Timestamp\" :1461317740,\"close\" :629.5000,\"high\" :629.5000,\"low\" :629.5000,\"open\" :629.5000,\"volume\" :1400 } \n" +
			", { \"Timestamp\" :1461317941,\"close\" :629.0000,\"high\" :629.0000,\"low\" :629.0000,\"open\" :629.0000,\"volume\" :300 } \n" +
			", { \"Timestamp\" :1461318414,\"close\" :628.0000,\"high\" :628.5000,\"low\" :628.0000,\"open\" :628.5000,\"volume\" :800 } \n" +
			", { \"Timestamp\" :1461318538,\"close\" :628.0000,\"high\" :628.0000,\"low\" :628.0000,\"open\" :628.0000,\"volume\" :500 } \n" +
			", { \"Timestamp\" :1461319215,\"close\" :628.0000,\"high\" :628.0000,\"low\" :628.0000,\"open\" :628.0000,\"volume\" :1500 } \n" +
			", { \"Timestamp\" :1461319425,\"close\" :629.0000,\"high\" :629.0000,\"low\" :629.0000,\"open\" :629.0000,\"volume\" :900 } \n" +
			", { \"Timestamp\" :1461319668,\"close\" :629.5000,\"high\" :629.5000,\"low\" :629.5000,\"open\" :629.5000,\"volume\" :900 } \n" +
			", { \"Timestamp\" :1461319897,\"close\" :630.0000,\"high\" :630.0000,\"low\" :630.0000,\"open\" :630.0000,\"volume\" :300 } \n" +
			", { \"Timestamp\" :1461319952,\"close\" :630.0000,\"high\" :630.0000,\"low\" :630.0000,\"open\" :630.0000,\"volume\" :200 } \n" +
			", { \"Timestamp\" :1461320353,\"close\" :629.0000,\"high\" :629.5000,\"low\" :629.0000,\"open\" :629.5000,\"volume\" :3500 } \n" +
			", { \"Timestamp\" :1461320431,\"close\" :629.0000,\"high\" :629.0000,\"low\" :629.0000,\"open\" :629.0000,\"volume\" :1000 } \n" +
			", { \"Timestamp\" :1461320833,\"close\" :629.0000,\"high\" :629.0000,\"low\" :629.0000,\"open\" :629.0000,\"volume\" :0 } \n" +
			", { \"Timestamp\" :1461320888,\"close\" :629.5000,\"high\" :629.5000,\"low\" :629.5000,\"open\" :629.5000,\"volume\" :100 } \n" +
			", { \"Timestamp\" :1461321051,\"close\" :630.0200,\"high\" :630.0200,\"low\" :630.0200,\"open\" :630.0200,\"volume\" :19100 } \n" +
			", { \"Timestamp\" :1461321197,\"close\" :629.5000,\"high\" :629.5000,\"low\" :629.5000,\"open\" :629.5000,\"volume\" :0 } \n" +
			", { \"Timestamp\" :1461321317,\"close\" :629.0000,\"high\" :629.0000,\"low\" :629.0000,\"open\" :629.0000,\"volume\" :200 } \n" +
			", { \"Timestamp\" :1461321365,\"close\" :628.5000,\"high\" :628.5000,\"low\" :628.5000,\"open\" :628.5000,\"volume\" :400 } \n" +
			", { \"Timestamp\" :1461321485,\"close\" :628.0000,\"high\" :628.0000,\"low\" :628.0000,\"open\" :628.0000,\"volume\" :500 } \n" +
			", { \"Timestamp\" :1461321709,\"close\" :628.0000,\"high\" :628.0000,\"low\" :628.0000,\"open\" :628.0000,\"volume\" :600 } \n" +
			", { \"Timestamp\" :1461321725,\"close\" :628.9000,\"high\" :628.9000,\"low\" :628.9000,\"open\" :628.9000,\"volume\" :19600 } \n" +
			", { \"Timestamp\" :1461321850,\"close\" :628.5000,\"high\" :628.5000,\"low\" :628.5000,\"open\" :628.5000,\"volume\" :0 } \n" +
			", { \"Timestamp\" :1461322250,\"close\" :628.5000,\"high\" :628.5000,\"low\" :628.5000,\"open\" :628.5000,\"volume\" :0 } \n" +
			", { \"Timestamp\" :1461322269,\"close\" :629.0000,\"high\" :629.0000,\"low\" :629.0000,\"open\" :629.0000,\"volume\" :0 } \n" +
			", { \"Timestamp\" :1461322373,\"close\" :629.0000,\"high\" :629.0000,\"low\" :629.0000,\"open\" :629.0000,\"volume\" :100 } \n" +
			", { \"Timestamp\" :1461322439,\"close\" :629.0000,\"high\" :629.0000,\"low\" :629.0000,\"open\" :629.0000,\"volume\" :100 } \n" +
			", { \"Timestamp\" :1461322441,\"close\" :630.0000,\"high\" :630.0000,\"low\" :629.5000,\"open\" :629.5000,\"volume\" :1800 } \n" +
			", { \"Timestamp\" :1461322647,\"close\" :630.0000,\"high\" :630.0000,\"low\" :629.5000,\"open\" :629.5000,\"volume\" :2200 } \n" +
			", { \"Timestamp\" :1461322716,\"close\" :629.0000,\"high\" :629.0000,\"low\" :629.0000,\"open\" :629.0000,\"volume\" :700 } \n" +
			", { \"Timestamp\" :1461322763,\"close\" :629.0000,\"high\" :629.0000,\"low\" :629.0000,\"open\" :629.0000,\"volume\" :0 } \n" +
			", { \"Timestamp\" :1461322942,\"close\" :625.5000,\"high\" :625.5000,\"low\" :625.5000,\"open\" :625.5000,\"volume\" :1000 } \n" +
			", { \"Timestamp\" :1461323003,\"close\" :627.5000,\"high\" :628.0000,\"low\" :627.5000,\"open\" :628.0000,\"volume\" :1300 } \n" +
			", { \"Timestamp\" :1461323133,\"close\" :628.5000,\"high\" :628.5000,\"low\" :628.5000,\"open\" :628.5000,\"volume\" :0 } \n" +
			", { \"Timestamp\" :1461323227,\"close\" :628.5000,\"high\" :628.5000,\"low\" :628.5000,\"open\" :628.5000,\"volume\" :400 } \n" +
			", { \"Timestamp\" :1461323641,\"close\" :629.0000,\"high\" :629.0000,\"low\" :629.0000,\"open\" :629.0000,\"volume\" :0 } \n" +
			", { \"Timestamp\" :1461323756,\"close\" :629.5000,\"high\" :629.5000,\"low\" :629.5000,\"open\" :629.5000,\"volume\" :300 } \n" +
			", { \"Timestamp\" :1461324864,\"close\" :629.5000,\"high\" :630.0000,\"low\" :629.5000,\"open\" :630.0000,\"volume\" :2600 } \n" +
			", { \"Timestamp\" :1461325035,\"close\" :629.5000,\"high\" :629.5000,\"low\" :629.2400,\"open\" :629.2400,\"volume\" :1100 } \n" +
			", { \"Timestamp\" :1461325340,\"close\" :629.0000,\"high\" :629.0000,\"low\" :629.0000,\"open\" :629.0000,\"volume\" :2500 } \n" +
			", { \"Timestamp\" :1461325466,\"close\" :629.5000,\"high\" :629.5000,\"low\" :629.5000,\"open\" :629.5000,\"volume\" :800 } \n" +
			", { \"Timestamp\" :1461326358,\"close\" :629.5000,\"high\" :629.5000,\"low\" :629.5000,\"open\" :629.5000,\"volume\" :700 } \n" +
			", { \"Timestamp\" :1461326645,\"close\" :629.5000,\"high\" :630.0000,\"low\" :629.5000,\"open\" :630.0000,\"volume\" :2800 } \n" +
			", { \"Timestamp\" :1461327155,\"close\" :629.5000,\"high\" :629.5000,\"low\" :629.5000,\"open\" :629.5000,\"volume\" :0 } \n" +
			", { \"Timestamp\" :1461327794,\"close\" :630.0000,\"high\" :630.0000,\"low\" :629.9000,\"open\" :629.9000,\"volume\" :200 } \n" +
			", { \"Timestamp\" :1461327899,\"close\" :630.0000,\"high\" :630.0000,\"low\" :630.0000,\"open\" :630.0000,\"volume\" :1300 } \n" +
			", { \"Timestamp\" :1461327900,\"close\" :630.0000,\"high\" :630.0000,\"low\" :630.0000,\"open\" :630.0000,\"volume\" :200 } \n" +
			", { \"Timestamp\" :1461328161,\"close\" :629.5000,\"high\" :629.5000,\"low\" :629.5000,\"open\" :629.5000,\"volume\" :800 } \n" +
			", { \"Timestamp\" :1461328601,\"close\" :630.0000,\"high\" :630.0000,\"low\" :630.0000,\"open\" :630.0000,\"volume\" :3700 } \n" +
			", { \"Timestamp\" :1461328626,\"close\" :629.5000,\"high\" :630.0000,\"low\" :629.5000,\"open\" :630.0000,\"volume\" :1300 } \n" +
			", { \"Timestamp\" :1461329001,\"close\" :630.0000,\"high\" :630.0000,\"low\" :630.0000,\"open\" :630.0000,\"volume\" :1200 } \n" +
			", { \"Timestamp\" :1461329081,\"close\" :630.0000,\"high\" :630.0000,\"low\" :630.0000,\"open\" :630.0000,\"volume\" :100 } \n" +
			", { \"Timestamp\" :1461329157,\"close\" :630.0000,\"high\" :630.0000,\"low\" :630.0000,\"open\" :630.0000,\"volume\" :300 } \n" +
			", { \"Timestamp\" :1461329257,\"close\" :630.0000,\"high\" :630.0000,\"low\" :630.0000,\"open\" :630.0000,\"volume\" :0 } \n" +
			", { \"Timestamp\" :1461329286,\"close\" :630.0000,\"high\" :630.0000,\"low\" :630.0000,\"open\" :630.0000,\"volume\" :100 } \n" +
			", { \"Timestamp\" :1461329349,\"close\" :630.0000,\"high\" :630.0000,\"low\" :630.0000,\"open\" :630.0000,\"volume\" :600 } \n" +
			", { \"Timestamp\" :1461329400,\"close\" :630.0000,\"high\" :630.0000,\"low\" :630.0000,\"open\" :630.0000,\"volume\" :400 } \n" +
			", { \"Timestamp\" :1461329486,\"close\" :630.0000,\"high\" :630.0000,\"low\" :630.0000,\"open\" :630.0000,\"volume\" :200 } \n" +
			", { \"Timestamp\" :1461329520,\"close\" :630.0000,\"high\" :630.0000,\"low\" :630.0000,\"open\" :630.0000,\"volume\" :600 } \n" +
			", { \"Timestamp\" :1461329767,\"close\" :630.0000,\"high\" :630.0000,\"low\" :630.0000,\"open\" :630.0000,\"volume\" :100 } \n" +
			", { \"Timestamp\" :1461329907,\"close\" :630.0000,\"high\" :630.0000,\"low\" :630.0000,\"open\" :630.0000,\"volume\" :400 } \n" +
			", { \"Timestamp\" :1461329968,\"close\" :630.0000,\"high\" :630.0000,\"low\" :630.0000,\"open\" :630.0000,\"volume\" :2300 } \n" +
			", { \"Timestamp\" :1461330123,\"close\" :629.5000,\"high\" :629.5000,\"low\" :629.5000,\"open\" :629.5000,\"volume\" :400 } \n" +
			", { \"Timestamp\" :1461330249,\"close\" :630.0000,\"high\" :630.0000,\"low\" :630.0000,\"open\" :630.0000,\"volume\" :500 } \n" +
			", { \"Timestamp\" :1461330325,\"close\" :629.5000,\"high\" :629.5000,\"low\" :629.5000,\"open\" :629.5000,\"volume\" :2100 } \n" +
			", { \"Timestamp\" :1461330469,\"close\" :629.0000,\"high\" :629.0000,\"low\" :629.0000,\"open\" :629.0000,\"volume\" :300 } \n" +
			", { \"Timestamp\" :1461330602,\"close\" :629.5000,\"high\" :629.5000,\"low\" :629.5000,\"open\" :629.5000,\"volume\" :100 } \n" +
			", { \"Timestamp\" :1461330804,\"close\" :628.5000,\"high\" :629.0000,\"low\" :628.5000,\"open\" :629.0000,\"volume\" :1700 } \n" +
			", { \"Timestamp\" :1461330872,\"close\" :627.5000,\"high\" :627.5000,\"low\" :627.5000,\"open\" :627.5000,\"volume\" :300 } \n" +
			", { \"Timestamp\" :1461331054,\"close\" :628.0000,\"high\" :628.0000,\"low\" :628.0000,\"open\" :628.0000,\"volume\" :0 } \n" +
			", { \"Timestamp\" :1461331130,\"close\" :628.0000,\"high\" :628.0000,\"low\" :628.0000,\"open\" :628.0000,\"volume\" :500 } \n" +
			", { \"Timestamp\" :1461331298,\"close\" :627.5000,\"high\" :627.5000,\"low\" :627.5000,\"open\" :627.5000,\"volume\" :700 } \n" +
			", { \"Timestamp\" :1461331432,\"close\" :627.5000,\"high\" :627.5000,\"low\" :627.5000,\"open\" :627.5000,\"volume\" :300 } \n" +
			", { \"Timestamp\" :1461331704,\"close\" :628.5000,\"high\" :628.5000,\"low\" :627.5000,\"open\" :627.5000,\"volume\" :400 } \n" +
			", { \"Timestamp\" :1461331755,\"close\" :628.5000,\"high\" :628.5000,\"low\" :628.5000,\"open\" :628.5000,\"volume\" :100 } \n" +
			", { \"Timestamp\" :1461331985,\"close\" :628.5000,\"high\" :628.5000,\"low\" :628.5000,\"open\" :628.5000,\"volume\" :5400 } \n" +
			", { \"Timestamp\" :1461332374,\"close\" :627.0000,\"high\" :627.0000,\"low\" :627.0000,\"open\" :627.0000,\"volume\" :1700 } \n" +
			", { \"Timestamp\" :1461332887,\"close\" :625.5000,\"high\" :625.5000,\"low\" :625.5000,\"open\" :625.5000,\"volume\" :3000 } \n" +
			", { \"Timestamp\" :1461334682,\"close\" :626.0000,\"high\" :626.0000,\"low\" :626.0000,\"open\" :626.0000,\"volume\" :13600 } \n" +
			", { \"Timestamp\" :1461334876,\"close\" :626.5000,\"high\" :626.5000,\"low\" :626.5000,\"open\" :626.5000,\"volume\" :2500 } \n" +
			", { \"Timestamp\" :1461334957,\"close\" :627.0000,\"high\" :627.0000,\"low\" :627.0000,\"open\" :627.0000,\"volume\" :1200 } \n" +
			", { \"Timestamp\" :1461335047,\"close\" :628.0000,\"high\" :628.0000,\"low\" :627.5000,\"open\" :627.5000,\"volume\" :5900 } \n" +
			", { \"Timestamp\" :1461335842,\"close\" :628.0000,\"high\" :628.0000,\"low\" :628.0000,\"open\" :628.0000,\"volume\" :5200 } \n" +
			", { \"Timestamp\" :1461335995,\"close\" :628.0000,\"high\" :628.0000,\"low\" :628.0000,\"open\" :628.0000,\"volume\" :200 } \n" +
			", { \"Timestamp\" :1461336183,\"close\" :627.5000,\"high\" :628.0000,\"low\" :627.5000,\"open\" :628.0000,\"volume\" :3700 } \n" +
			", { \"Timestamp\" :1461336376,\"close\" :627.0000,\"high\" :627.0000,\"low\" :627.0000,\"open\" :627.0000,\"volume\" :100 } \n" +
			", { \"Timestamp\" :1461336437,\"close\" :627.0000,\"high\" :627.0000,\"low\" :627.0000,\"open\" :627.0000,\"volume\" :28100 } \n" +
			", { \"Timestamp\" :1461336714,\"close\" :627.0000,\"high\" :627.0000,\"low\" :627.0000,\"open\" :627.0000,\"volume\" :0 } \n" +
			", { \"Timestamp\" :1461336829,\"close\" :627.5000,\"high\" :627.5000,\"low\" :627.5000,\"open\" :627.5000,\"volume\" :26700 } \n" +
			", { \"Timestamp\" :1461337030,\"close\" :628.0000,\"high\" :628.0000,\"low\" :628.0000,\"open\" :628.0000,\"volume\" :0 } \n" +
			", { \"Timestamp\" :1461337117,\"close\" :628.5000,\"high\" :628.5000,\"low\" :628.5000,\"open\" :628.5000,\"volume\" :0 } \n" +
			", { \"Timestamp\" :1461337198,\"close\" :628.0000,\"high\" :628.0000,\"low\" :628.0000,\"open\" :628.0000,\"volume\" :1100 } \n" +
			", { \"Timestamp\" :1461337387,\"close\" :628.0000,\"high\" :628.0000,\"low\" :628.0000,\"open\" :628.0000,\"volume\" :300 } \n" +
			", { \"Timestamp\" :1461337553,\"close\" :628.5000,\"high\" :629.0000,\"low\" :628.5000,\"open\" :628.5000,\"volume\" :2100 } \n" +
			", { \"Timestamp\" :1461337746,\"close\" :628.5000,\"high\" :629.0000,\"low\" :628.5000,\"open\" :629.0000,\"volume\" :2800 } \n" +
			", { \"Timestamp\" :1461338255,\"close\" :628.5000,\"high\" :628.5000,\"low\" :628.5000,\"open\" :628.5000,\"volume\" :3000 } \n" +
			", { \"Timestamp\" :1461338283,\"close\" :628.5000,\"high\" :628.5000,\"low\" :628.5000,\"open\" :628.5000,\"volume\" :300 } \n" +
			", { \"Timestamp\" :1461338470,\"close\" :629.0000,\"high\" :629.0000,\"low\" :628.5000,\"open\" :628.5000,\"volume\" :3300 } \n" +
			", { \"Timestamp\" :1461338573,\"close\" :628.5000,\"high\" :628.5000,\"low\" :628.5000,\"open\" :628.5000,\"volume\" :1100 } \n" +
			", { \"Timestamp\" :1461338607,\"close\" :628.5000,\"high\" :628.5000,\"low\" :628.0000,\"open\" :628.5000,\"volume\" :1000 } \n" +
			", { \"Timestamp\" :1461338880,\"close\" :629.0000,\"high\" :629.0000,\"low\" :629.0000,\"open\" :629.0000,\"volume\" :3200 } \n" +
			", { \"Timestamp\" :1461339000,\"close\" :629.0000,\"high\" :629.0000,\"low\" :629.0000,\"open\" :629.0000,\"volume\" :0 } \n" +
			"\n" +
			"]\n" +
			"}";

	private static String mockYahooDailyResponseCanOpen = "{ \"meta\" :\n" +
			" {\n" +
			"  \"uri\" :\"/instrument/1.0/icp.l/chartdata;type=quote;range=1d/json\",\n" +
			"  \"ticker\" : \"icp.l\",\n" +
			"  \"Company-Name\" : \"INTERMEDIATE CAPITAL GROUP PLC \",\n" +
			"  \"Exchange-Name\" : \"LSE\",\n" +
			"  \"unit\" : \"MIN\",\n" +
			"  \"timezone\" : \"BST\",\n" +
			"  \"currency\" : \"GBp\",\n" +
			"  \"gmtoffset\" : 3600,\n" +
			"  \"previous_close\" : 625.0000\n" +
			" }\n" +
			" ,\n" +
			" \"Timestamp\" : {\"min\" :1461308400,\"max\" :1461339000 }\n" +
			" ,\n" +
			" \"labels\" : [1461308400,1461312000,1461315600,1461319200,1461322800,1461326400,1461330000,1461333600,1461337200 ]\n" +
			" ,\n" +
			" \"ranges\" : {\"close\" : {\"min\" :620.0000,\"max\" :630.0200 },\"high\" : {\"min\" :620.9900,\"max\" :630.0200 },\"low\" : {\"min\" :620.0000,\"max\" :630.0200 },\"open\" : {\"min\" :620.9900,\"max\" :630.0200 },\"volume\" : {\"min\" :0,\"max\" :28100 } }\n" +
			" ,\n" +
			" \"series\" : [\n" +
			"{ \"Timestamp\" :1460963051,\"close\" :637.5000,\"high\" :640.0000,\"low\" :637.5000,\"open\" :638.5000,\"volume\" :4500 }  \n" +
			", { \"Timestamp\" :1460963259,\"close\" :637.0000,\"high\" :637.0000,\"low\" :637.0000,\"open\" :637.0000,\"volume\" :500 }  \n" +
			", { \"Timestamp\" :1460963651,\"close\" :640.5000,\"high\" :640.5000,\"low\" :639.5000,\"open\" :639.5000,\"volume\" :2900 }  \n" +
			", { \"Timestamp\" :1460963877,\"close\" :639.5000,\"high\" :639.5000,\"low\" :639.5000,\"open\" :639.5000,\"volume\" :700 }  \n" +
			", { \"Timestamp\" :1460964096,\"close\" :639.5000,\"high\" :639.5000,\"low\" :639.5000,\"open\" :639.5000,\"volume\" :400 }  \n" +
			", { \"Timestamp\" :1460964521,\"close\" :640.5000,\"high\" :640.5000,\"low\" :639.0000,\"open\" :639.0000,\"volume\" :1600 }  \n" +
			", { \"Timestamp\" :1460964809,\"close\" :640.0000,\"high\" :641.0000,\"low\" :640.0000,\"open\" :640.0000,\"volume\" :2800 }  \n" +
			", { \"Timestamp\" :1460965083,\"close\" :641.0000,\"high\" :642.5000,\"low\" :640.5000,\"open\" :640.5000,\"volume\" :3400 }  \n" +
			", { \"Timestamp\" :1460965339,\"close\" :641.0000,\"high\" :641.0000,\"low\" :641.0000,\"open\" :641.0000,\"volume\" :1700 }  \n" +
			", { \"Timestamp\" :1460965562,\"close\" :641.0000,\"high\" :641.0000,\"low\" :640.0000,\"open\" :640.0000,\"volume\" :1600 }  \n" +
			", { \"Timestamp\" :1460966044,\"close\" :642.5000,\"high\" :643.0000,\"low\" :642.5000,\"open\" :643.0000,\"volume\" :3000 }  \n" +
			", { \"Timestamp\" :1460966139,\"close\" :642.5000,\"high\" :642.5000,\"low\" :642.5000,\"open\" :642.5000,\"volume\" :600 }  \n" +
			", { \"Timestamp\" :1460966651,\"close\" :643.5000,\"high\" :643.5000,\"low\" :642.5000,\"open\" :642.5000,\"volume\" :3000 }  \n" +
			", { \"Timestamp\" :1460966944,\"close\" :642.5000,\"high\" :643.5000,\"low\" :642.0000,\"open\" :643.5000,\"volume\" :6900 }  \n" +
			", { \"Timestamp\" :1460967288,\"close\" :644.0000,\"high\" :644.0000,\"low\" :642.5000,\"open\" :643.0000,\"volume\" :3400 }  \n" +
			", { \"Timestamp\" :1460967592,\"close\" :645.5000,\"high\" :645.5000,\"low\" :644.5000,\"open\" :644.5000,\"volume\" :700 }  \n" +
			", { \"Timestamp\" :1460967863,\"close\" :644.0000,\"high\" :644.0000,\"low\" :644.0000,\"open\" :644.0000,\"volume\" :3500 }  \n" +
			", { \"Timestamp\" :1460968142,\"close\" :643.0000,\"high\" :644.0000,\"low\" :643.0000,\"open\" :643.5000,\"volume\" :3900 }  \n" +
			", { \"Timestamp\" :1460968445,\"close\" :644.0000,\"high\" :644.0000,\"low\" :643.5000,\"open\" :643.5000,\"volume\" :10100 }  \n" +
			", { \"Timestamp\" :1460968708,\"close\" :643.5000,\"high\" :645.0000,\"low\" :643.5000,\"open\" :644.0000,\"volume\" :0 }  \n" +
			", { \"Timestamp\" :1460969045,\"close\" :645.0000,\"high\" :645.5000,\"low\" :644.0000,\"open\" :644.0000,\"volume\" :3800 }  \n" +
			", { \"Timestamp\" :1460969351,\"close\" :645.5000,\"high\" :646.0000,\"low\" :645.5000,\"open\" :645.5000,\"volume\" :1600 }  \n" +
			", { \"Timestamp\" :1460969686,\"close\" :646.0000,\"high\" :647.0000,\"low\" :645.0000,\"open\" :645.0000,\"volume\" :3800 }  \n" +
			", { \"Timestamp\" :1460969780,\"close\" :645.5000,\"high\" :646.5000,\"low\" :645.5000,\"open\" :646.0000,\"volume\" :3200 }  \n" +
			", { \"Timestamp\" :1460970092,\"close\" :645.0000,\"high\" :646.0000,\"low\" :645.0000,\"open\" :645.5000,\"volume\" :3300 }  \n" +
			", { \"Timestamp\" :1460970566,\"close\" :644.5000,\"high\" :645.0000,\"low\" :644.5000,\"open\" :645.0000,\"volume\" :1600 }  \n" +
			", { \"Timestamp\" :1460971158,\"close\" :643.8182,\"high\" :644.5000,\"low\" :643.0000,\"open\" :644.5000,\"volume\" :16500 }  \n" +
			", { \"Timestamp\" :1460971487,\"close\" :644.5000,\"high\" :644.5000,\"low\" :644.5000,\"open\" :644.5000,\"volume\" :0 }  \n" +
			", { \"Timestamp\" :1460971510,\"close\" :644.0000,\"high\" :644.0000,\"low\" :644.0000,\"open\" :644.0000,\"volume\" :700 }  \n" +
			", { \"Timestamp\" :1460972015,\"close\" :644.5000,\"high\" :644.5000,\"low\" :644.0000,\"open\" :644.0000,\"volume\" :5000 }  \n" +
			", { \"Timestamp\" :1460972214,\"close\" :645.0000,\"high\" :645.0000,\"low\" :645.0000,\"open\" :645.0000,\"volume\" :6000 }  \n" +
			", { \"Timestamp\" :1460972663,\"close\" :645.0000,\"high\" :645.0000,\"low\" :644.5000,\"open\" :644.5000,\"volume\" :2500 }  \n" +
			", { \"Timestamp\" :1460972810,\"close\" :644.0000,\"high\" :644.0000,\"low\" :644.0000,\"open\" :644.0000,\"volume\" :2700 }  \n" +
			", { \"Timestamp\" :1460973080,\"close\" :643.5000,\"high\" :643.5000,\"low\" :643.5000,\"open\" :643.5000,\"volume\" :200 }  \n" +
			", { \"Timestamp\" :1460973588,\"close\" :642.0000,\"high\" :643.0000,\"low\" :642.0000,\"open\" :643.0000,\"volume\" :4500 }  \n" +
			", { \"Timestamp\" :1460974035,\"close\" :641.5000,\"high\" :642.0000,\"low\" :641.0000,\"open\" :642.0000,\"volume\" :15100 }  \n" +
			", { \"Timestamp\" :1460974488,\"close\" :640.0000,\"high\" :641.5000,\"low\" :640.0000,\"open\" :641.5000,\"volume\" :11900 }  \n" +
			", { \"Timestamp\" :1460974531,\"close\" :640.0000,\"high\" :640.0000,\"low\" :640.0000,\"open\" :640.0000,\"volume\" :11200 }  \n" +
			", { \"Timestamp\" :1460975040,\"close\" :639.0000,\"high\" :639.5000,\"low\" :639.0000,\"open\" :639.5000,\"volume\" :0 }  \n" +
			", { \"Timestamp\" :1460975348,\"close\" :639.0000,\"high\" :639.0000,\"low\" :639.0000,\"open\" :639.0000,\"volume\" :600 }  \n" +
			", { \"Timestamp\" :1460975580,\"close\" :639.0000,\"high\" :639.0000,\"low\" :639.0000,\"open\" :639.0000,\"volume\" :1400 }  \n" +
			", { \"Timestamp\" :1460975810,\"close\" :639.0000,\"high\" :639.0000,\"low\" :639.0000,\"open\" :639.0000,\"volume\" :800 }  \n" +
			", { \"Timestamp\" :1460976035,\"close\" :640.0000,\"high\" :640.0000,\"low\" :639.5000,\"open\" :639.5000,\"volume\" :2200 }  \n" +
			", { \"Timestamp\" :1460976794,\"close\" :639.5000,\"high\" :640.0000,\"low\" :639.5000,\"open\" :640.0000,\"volume\" :4900 }  \n" +
			", { \"Timestamp\" :1460976978,\"close\" :639.5000,\"high\" :639.5000,\"low\" :639.5000,\"open\" :639.5000,\"volume\" :1300 }  \n" +
			", { \"Timestamp\" :1460977341,\"close\" :637.5000,\"high\" :637.5000,\"low\" :637.5000,\"open\" :637.5000,\"volume\" :3600 }  \n" +
			", { \"Timestamp\" :1460977646,\"close\" :641.0000,\"high\" :641.0000,\"low\" :640.0000,\"open\" :640.0000,\"volume\" :1500 }  \n" +
			", { \"Timestamp\" :1460978397,\"close\" :641.0000,\"high\" :641.0000,\"low\" :641.0000,\"open\" :641.0000,\"volume\" :0 }  \n" +
			", { \"Timestamp\" :1460978916,\"close\" :641.0000,\"high\" :641.0000,\"low\" :641.0000,\"open\" :641.0000,\"volume\" :1500 }  \n" +
			", { \"Timestamp\" :1460979094,\"close\" :641.0000,\"high\" :641.5000,\"low\" :641.0000,\"open\" :641.5000,\"volume\" :700 }  \n" +
			", { \"Timestamp\" :1460979341,\"close\" :641.5000,\"high\" :641.5000,\"low\" :641.5000,\"open\" :641.5000,\"volume\" :1000 }  \n" +
			", { \"Timestamp\" :1460980224,\"close\" :640.5000,\"high\" :640.5000,\"low\" :640.5000,\"open\" :640.5000,\"volume\" :1200 }  \n" +
			", { \"Timestamp\" :1460980778,\"close\" :641.0000,\"high\" :641.0000,\"low\" :641.0000,\"open\" :641.0000,\"volume\" :1100 }  \n" +
			", { \"Timestamp\" :1460981418,\"close\" :641.0000,\"high\" :641.0000,\"low\" :641.0000,\"open\" :641.0000,\"volume\" :1700 }  \n" +
			", { \"Timestamp\" :1460981905,\"close\" :640.5000,\"high\" :640.5000,\"low\" :640.5000,\"open\" :640.5000,\"volume\" :2700 }  \n" +
			", { \"Timestamp\" :1460982111,\"close\" :640.5000,\"high\" :640.5000,\"low\" :640.5000,\"open\" :640.5000,\"volume\" :0 }  \n" +
			", { \"Timestamp\" :1460982365,\"close\" :640.5000,\"high\" :640.5000,\"low\" :640.5000,\"open\" :640.5000,\"volume\" :1500 }  \n" +
			", { \"Timestamp\" :1460982631,\"close\" :640.5000,\"high\" :640.5000,\"low\" :640.5000,\"open\" :640.5000,\"volume\" :11200 }  \n" +
			", { \"Timestamp\" :1460983784,\"close\" :639.0000,\"high\" :640.0000,\"low\" :639.0000,\"open\" :640.0000,\"volume\" :0 }  \n" +
			", { \"Timestamp\" :1460984040,\"close\" :638.5000,\"high\" :638.5000,\"low\" :637.5000,\"open\" :638.5000,\"volume\" :18800 }  \n" +
			", { \"Timestamp\" :1460984393,\"close\" :637.5000,\"high\" :637.5000,\"low\" :637.5000,\"open\" :637.5000,\"volume\" :0 }  \n" +
			", { \"Timestamp\" :1460984601,\"close\" :637.5000,\"high\" :638.0000,\"low\" :637.0000,\"open\" :638.0000,\"volume\" :5300 }  \n" +
			", { \"Timestamp\" :1460984944,\"close\" :637.0000,\"high\" :637.5000,\"low\" :637.0000,\"open\" :637.0000,\"volume\" :2300 }  \n" +
			", { \"Timestamp\" :1460985548,\"close\" :637.5000,\"high\" :637.5000,\"low\" :637.0000,\"open\" :637.0000,\"volume\" :4500 }  \n" +
			", { \"Timestamp\" :1460985782,\"close\" :636.0000,\"high\" :637.0000,\"low\" :636.0000,\"open\" :637.0000,\"volume\" :3600 }  \n" +
			", { \"Timestamp\" :1460986347,\"close\" :636.5000,\"high\" :637.0000,\"low\" :636.5000,\"open\" :637.0000,\"volume\" :5700 }  \n" +
			", { \"Timestamp\" :1460986530,\"close\" :636.0000,\"high\" :636.0000,\"low\" :636.0000,\"open\" :636.0000,\"volume\" :28700 }  \n" +
			", { \"Timestamp\" :1460987294,\"close\" :636.0000,\"high\" :636.0000,\"low\" :635.5000,\"open\" :636.0000,\"volume\" :0 }  \n" +
			", { \"Timestamp\" :1460987662,\"close\" :636.0000,\"high\" :637.0000,\"low\" :636.0000,\"open\" :636.0000,\"volume\" :8900 }  \n" +
			", { \"Timestamp\" :1460987732,\"close\" :636.0000,\"high\" :636.0000,\"low\" :636.0000,\"open\" :636.0000,\"volume\" :900 }  \n" +
			", { \"Timestamp\" :1460988266,\"close\" :636.5000,\"high\" :636.5000,\"low\" :635.5000,\"open\" :636.5000,\"volume\" :11400 }  \n" +
			", { \"Timestamp\" :1460988451,\"close\" :637.0000,\"high\" :637.0000,\"low\" :636.5000,\"open\" :636.5000,\"volume\" :1400 }  \n" +
			", { \"Timestamp\" :1460988864,\"close\" :637.0000,\"high\" :637.0000,\"low\" :636.5000,\"open\" :636.5000,\"volume\" :1800 }  \n" +
			", { \"Timestamp\" :1460989182,\"close\" :637.0000,\"high\" :637.5000,\"low\" :637.0000,\"open\" :637.0000,\"volume\" :5300 }  \n" +
			", { \"Timestamp\" :1460989262,\"close\" :637.0000,\"high\" :637.5000,\"low\" :637.0000,\"open\" :637.5000,\"volume\" :2100 }  \n" +
			", { \"Timestamp\" :1460989716,\"close\" :637.5000,\"high\" :638.0000,\"low\" :637.5000,\"open\" :638.0000,\"volume\" :600 }  \n" +
			", { \"Timestamp\" :1460990213,\"close\" :638.0000,\"high\" :638.0000,\"low\" :638.0000,\"open\" :638.0000,\"volume\" :600 }  \n" +
			", { \"Timestamp\" :1460990530,\"close\" :638.5000,\"high\" :638.5000,\"low\" :638.5000,\"open\" :638.5000,\"volume\" :1300 }  \n" +
			", { \"Timestamp\" :1460990977,\"close\" :639.5000,\"high\" :639.5000,\"low\" :639.0000,\"open\" :639.0000,\"volume\" :2700 }  \n" +
			", { \"Timestamp\" :1460991281,\"close\" :639.5000,\"high\" :639.5000,\"low\" :639.0000,\"open\" :639.0000,\"volume\" :5200 }  \n" +
			", { \"Timestamp\" :1460991357,\"close\" :639.5000,\"high\" :639.5000,\"low\" :639.5000,\"open\" :639.5000,\"volume\" :500 }  \n" +
			", { \"Timestamp\" :1460991789,\"close\" :639.5000,\"high\" :640.0000,\"low\" :639.5000,\"open\" :640.0000,\"volume\" :5700 }  \n" +
			", { \"Timestamp\" :1460992161,\"close\" :639.0000,\"high\" :639.0000,\"low\" :639.0000,\"open\" :639.0000,\"volume\" :0 }  \n" +
			", { \"Timestamp\" :1460992460,\"close\" :639.5000,\"high\" :639.5000,\"low\" :639.5000,\"open\" :639.5000,\"volume\" :1200 }  \n" +
			", { \"Timestamp\" :1460992579,\"close\" :639.5000,\"high\" :639.5000,\"low\" :639.0000,\"open\" :639.0000,\"volume\" :5200 }  \n" +
			", { \"Timestamp\" :1460993360,\"close\" :638.5000,\"high\" :639.0000,\"low\" :638.5000,\"open\" :639.0000,\"volume\" :10900 }  \n" +
			", { \"Timestamp\" :1460993400,\"close\" :641.0000,\"high\" :641.0000,\"low\" :641.0000,\"open\" :641.0000,\"volume\" :0 }  \n" +
			", { \"Timestamp\" :1461049221,\"close\" :642.5000,\"high\" :642.5000,\"low\" :642.5000,\"open\" :642.5000,\"volume\" :0 }  \n" +
			", { \"Timestamp\" :1461049647,\"close\" :640.5000,\"high\" :640.5000,\"low\" :639.0000,\"open\" :639.0000,\"volume\" :1000 }  \n" +
			", { \"Timestamp\" :1461049924,\"close\" :642.0000,\"high\" :642.0000,\"low\" :641.0000,\"open\" :641.0000,\"volume\" :2100 }  \n" +
			", { \"Timestamp\" :1461050109,\"close\" :642.5000,\"high\" :642.5000,\"low\" :642.5000,\"open\" :642.5000,\"volume\" :1900 }  \n" +
			", { \"Timestamp\" :1461050582,\"close\" :641.0000,\"high\" :642.5000,\"low\" :641.0000,\"open\" :642.5000,\"volume\" :3500 }  \n" +
			", { \"Timestamp\" :1461050988,\"close\" :641.5000,\"high\" :642.0000,\"low\" :641.0000,\"open\" :641.0000,\"volume\" :4000 }  \n" +
			", { \"Timestamp\" :1461051199,\"close\" :641.0000,\"high\" :641.0000,\"low\" :640.5000,\"open\" :641.0000,\"volume\" :2900 }  \n" +
			", { \"Timestamp\" :1461051500,\"close\" :641.0000,\"high\" :641.0000,\"low\" :640.0000,\"open\" :641.0000,\"volume\" :8300 }  \n" +
			", { \"Timestamp\" :1461051843,\"close\" :641.0000,\"high\" :641.5000,\"low\" :640.5000,\"open\" :641.5000,\"volume\" :9800 }  \n" +
			", { \"Timestamp\" :1461051960,\"close\" :641.0000,\"high\" :641.5000,\"low\" :640.5000,\"open\" :641.0000,\"volume\" :3200 }  \n" +
			", { \"Timestamp\" :1461052463,\"close\" :641.5000,\"high\" :641.5000,\"low\" :641.5000,\"open\" :641.5000,\"volume\" :2500 }  \n" +
			", { \"Timestamp\" :1461053054,\"close\" :642.5000,\"high\" :644.5000,\"low\" :642.5000,\"open\" :643.5000,\"volume\" :12100 }  \n" +
			", { \"Timestamp\" :1461053192,\"close\" :642.0000,\"high\" :642.5000,\"low\" :642.0000,\"open\" :642.5000,\"volume\" :800 }  \n" +
			", { \"Timestamp\" :1461053548,\"close\" :642.5000,\"high\" :642.5000,\"low\" :642.5000,\"open\" :642.5000,\"volume\" :1100 }  \n" +
			", { \"Timestamp\" :1461053889,\"close\" :643.5000,\"high\" :643.5000,\"low\" :643.5000,\"open\" :643.5000,\"volume\" :1300 }  \n" +
			", { \"Timestamp\" :1461054245,\"close\" :644.5000,\"high\" :645.0000,\"low\" :644.5000,\"open\" :645.0000,\"volume\" :7000 }  \n" +
			", { \"Timestamp\" :1461054871,\"close\" :644.0000,\"high\" :644.0000,\"low\" :644.0000,\"open\" :644.0000,\"volume\" :2600 }  \n" +
			", { \"Timestamp\" :1461055125,\"close\" :642.5000,\"high\" :643.5000,\"low\" :642.5000,\"open\" :643.5000,\"volume\" :6800 }  \n" +
			", { \"Timestamp\" :1461055452,\"close\" :642.5000,\"high\" :643.0000,\"low\" :642.5000,\"open\" :643.0000,\"volume\" :500 }  \n" +
			", { \"Timestamp\" :1461055738,\"close\" :642.5000,\"high\" :643.0000,\"low\" :642.5000,\"open\" :643.0000,\"volume\" :1900 }  \n" +
			", { \"Timestamp\" :1461056044,\"close\" :643.0000,\"high\" :643.0000,\"low\" :642.5000,\"open\" :643.0000,\"volume\" :4400 }  \n" +
			", { \"Timestamp\" :1461056399,\"close\" :642.0000,\"high\" :643.0000,\"low\" :642.0000,\"open\" :643.0000,\"volume\" :2800 }  \n" +
			", { \"Timestamp\" :1461056582,\"close\" :642.0000,\"high\" :642.0000,\"low\" :641.5000,\"open\" :641.5000,\"volume\" :2100 }  \n" +
			", { \"Timestamp\" :1461056957,\"close\" :641.5000,\"high\" :642.0000,\"low\" :641.0000,\"open\" :642.0000,\"volume\" :2900 }  \n" +
			", { \"Timestamp\" :1461057241,\"close\" :641.5000,\"high\" :641.5000,\"low\" :641.5000,\"open\" :641.5000,\"volume\" :3300 }  \n" +
			", { \"Timestamp\" :1461057517,\"close\" :643.0000,\"high\" :643.0000,\"low\" :642.0000,\"open\" :642.0000,\"volume\" :1200 }  \n" +
			", { \"Timestamp\" :1461057859,\"close\" :643.0000,\"high\" :643.0000,\"low\" :642.5000,\"open\" :643.0000,\"volume\" :2200 }  \n" +
			", { \"Timestamp\" :1461058185,\"close\" :643.5000,\"high\" :643.5000,\"low\" :643.5000,\"open\" :643.5000,\"volume\" :1200 }  \n" +
			", { \"Timestamp\" :1461058270,\"close\" :643.5000,\"high\" :644.0000,\"low\" :643.5000,\"open\" :643.5000,\"volume\" :1800 }  \n" +
			", { \"Timestamp\" :1461058568,\"close\" :643.5000,\"high\" :643.5000,\"low\" :642.5000,\"open\" :643.5000,\"volume\" :4200 }  \n" +
			", { \"Timestamp\" :1461059082,\"close\" :643.0000,\"high\" :643.0000,\"low\" :643.0000,\"open\" :643.0000,\"volume\" :1500 }  \n" +
			", { \"Timestamp\" :1461059500,\"close\" :643.5000,\"high\" :643.5000,\"low\" :643.4000,\"open\" :643.4000,\"volume\" :600 }  \n" +
			", { \"Timestamp\" :1461059778,\"close\" :643.0000,\"high\" :643.0000,\"low\" :643.0000,\"open\" :643.0000,\"volume\" :1000 }  \n" +
			", { \"Timestamp\" :1461060202,\"close\" :644.0000,\"high\" :644.0000,\"low\" :644.0000,\"open\" :644.0000,\"volume\" :700 }  \n" +
			", { \"Timestamp\" :1461060464,\"close\" :644.0000,\"high\" :644.0000,\"low\" :644.0000,\"open\" :644.0000,\"volume\" :600 }  \n" +
			", { \"Timestamp\" :1461061038,\"close\" :643.0000,\"high\" :643.5000,\"low\" :643.0000,\"open\" :643.5000,\"volume\" :1300 }  \n" +
			", { \"Timestamp\" :1461061477,\"close\" :641.0000,\"high\" :641.5000,\"low\" :641.0000,\"open\" :641.5000,\"volume\" :1500 }  \n" +
			", { \"Timestamp\" :1461061596,\"close\" :641.0000,\"high\" :641.0000,\"low\" :641.0000,\"open\" :641.0000,\"volume\" :1500 }  \n" +
			", { \"Timestamp\" :1461061921,\"close\" :641.0000,\"high\" :641.0000,\"low\" :641.0000,\"open\" :641.0000,\"volume\" :600 }  \n" +
			", { \"Timestamp\" :1461062246,\"close\" :641.0000,\"high\" :641.0000,\"low\" :641.0000,\"open\" :641.0000,\"volume\" :400 }  \n" +
			", { \"Timestamp\" :1461062579,\"close\" :641.0000,\"high\" :641.0000,\"low\" :641.0000,\"open\" :641.0000,\"volume\" :300 }  \n" +
			", { \"Timestamp\" :1461062908,\"close\" :641.5000,\"high\" :641.5000,\"low\" :641.5000,\"open\" :641.5000,\"volume\" :400 }  \n" +
			", { \"Timestamp\" :1461063599,\"close\" :640.5000,\"high\" :642.0000,\"low\" :640.5000,\"open\" :641.5000,\"volume\" :1900 }  \n" +
			", { \"Timestamp\" :1461063744,\"close\" :641.0000,\"high\" :641.0000,\"low\" :641.0000,\"open\" :641.0000,\"volume\" :800 }  \n" +
			", { \"Timestamp\" :1461064031,\"close\" :641.0000,\"high\" :642.0000,\"low\" :641.0000,\"open\" :642.0000,\"volume\" :1200 }  \n" +
			", { \"Timestamp\" :1461064495,\"close\" :642.5000,\"high\" :642.5000,\"low\" :642.0000,\"open\" :642.0000,\"volume\" :1500 }  \n" +
			", { \"Timestamp\" :1461064610,\"close\" :642.5000,\"high\" :642.5000,\"low\" :642.0000,\"open\" :642.0000,\"volume\" :1000 }  \n" +
			", { \"Timestamp\" :1461064905,\"close\" :642.0000,\"high\" :642.0000,\"low\" :642.0000,\"open\" :642.0000,\"volume\" :2300 }  \n" +
			", { \"Timestamp\" :1461065305,\"close\" :642.0000,\"high\" :642.0000,\"low\" :641.5000,\"open\" :642.0000,\"volume\" :1700 }  \n" +
			", { \"Timestamp\" :1461065583,\"close\" :641.0000,\"high\" :641.5000,\"low\" :641.0000,\"open\" :641.5000,\"volume\" :2200 }  \n" +
			", { \"Timestamp\" :1461065966,\"close\" :640.5000,\"high\" :641.0000,\"low\" :640.5000,\"open\" :641.0000,\"volume\" :900 }  \n" +
			", { \"Timestamp\" :1461066260,\"close\" :640.5000,\"high\" :640.5000,\"low\" :639.5000,\"open\" :639.5000,\"volume\" :2600 }  \n" +
			", { \"Timestamp\" :1461066426,\"close\" :641.0000,\"high\" :641.0000,\"low\" :641.0000,\"open\" :641.0000,\"volume\" :300 }  \n" +
			", { \"Timestamp\" :1461066866,\"close\" :641.5000,\"high\" :641.5000,\"low\" :641.0000,\"open\" :641.0000,\"volume\" :300 }  \n" +
			", { \"Timestamp\" :1461066931,\"close\" :641.5000,\"high\" :641.5000,\"low\" :641.5000,\"open\" :641.5000,\"volume\" :700 }  \n" +
			", { \"Timestamp\" :1461067382,\"close\" :641.5000,\"high\" :641.5000,\"low\" :641.5000,\"open\" :641.5000,\"volume\" :0 }  \n" +
			", { \"Timestamp\" :1461067575,\"close\" :642.0000,\"high\" :642.0000,\"low\" :642.0000,\"open\" :642.0000,\"volume\" :0 }  \n" +
			", { \"Timestamp\" :1461068220,\"close\" :642.0000,\"high\" :642.0000,\"low\" :642.0000,\"open\" :642.0000,\"volume\" :400 }  \n" +
			", { \"Timestamp\" :1461068475,\"close\" :642.0000,\"high\" :642.0000,\"low\" :641.5000,\"open\" :641.5000,\"volume\" :4400 }  \n" +
			", { \"Timestamp\" :1461069195,\"close\" :641.5000,\"high\" :641.5000,\"low\" :641.5000,\"open\" :641.5000,\"volume\" :1400 }  \n" +
			", { \"Timestamp\" :1461069435,\"close\" :641.0000,\"high\" :641.0000,\"low\" :641.0000,\"open\" :641.0000,\"volume\" :1500 }  \n" +
			", { \"Timestamp\" :1461070155,\"close\" :641.0000,\"high\" :641.5000,\"low\" :640.5000,\"open\" :641.0000,\"volume\" :4100 }  \n" +
			", { \"Timestamp\" :1461070362,\"close\" :641.0000,\"high\" :641.0000,\"low\" :641.0000,\"open\" :641.0000,\"volume\" :0 }  \n" +
			", { \"Timestamp\" :1461070787,\"close\" :641.5000,\"high\" :641.5000,\"low\" :641.0000,\"open\" :641.0000,\"volume\" :2300 }  \n" +
			", { \"Timestamp\" :1461071091,\"close\" :641.0000,\"high\" :641.0000,\"low\" :641.0000,\"open\" :641.0000,\"volume\" :3100 }  \n" +
			", { \"Timestamp\" :1461071171,\"close\" :641.5000,\"high\" :641.5000,\"low\" :641.5000,\"open\" :641.5000,\"volume\" :600 }  \n" +
			", { \"Timestamp\" :1461071661,\"close\" :641.0000,\"high\" :641.0000,\"low\" :641.0000,\"open\" :641.0000,\"volume\" :1600 }  \n" +
			", { \"Timestamp\" :1461071701,\"close\" :641.0000,\"high\" :641.0000,\"low\" :641.0000,\"open\" :641.0000,\"volume\" :0 }  \n" +
			", { \"Timestamp\" :1461073637,\"close\" :639.5000,\"high\" :639.5000,\"low\" :639.5000,\"open\" :639.5000,\"volume\" :227500 }  \n" +
			", { \"Timestamp\" :1461073948,\"close\" :639.0000,\"high\" :639.0000,\"low\" :639.0000,\"open\" :639.0000,\"volume\" :0 }  \n" +
			", { \"Timestamp\" :1461074138,\"close\" :639.5000,\"high\" :639.5000,\"low\" :639.5000,\"open\" :639.5000,\"volume\" :9800 }  \n" +
			", { \"Timestamp\" :1461074646,\"close\" :639.0000,\"high\" :639.0000,\"low\" :639.0000,\"open\" :639.0000,\"volume\" :15200 }  \n" +
			", { \"Timestamp\" :1461074762,\"close\" :639.0000,\"high\" :639.0000,\"low\" :639.0000,\"open\" :639.0000,\"volume\" :500 }  \n" +
			", { \"Timestamp\" :1461075282,\"close\" :639.5000,\"high\" :639.5000,\"low\" :639.5000,\"open\" :639.5000,\"volume\" :5900 }  \n" +
			", { \"Timestamp\" :1461075561,\"close\" :639.0000,\"high\" :639.0000,\"low\" :639.0000,\"open\" :639.0000,\"volume\" :4700 }  \n" +
			", { \"Timestamp\" :1461075864,\"close\" :639.5000,\"high\" :639.5000,\"low\" :639.5000,\"open\" :639.5000,\"volume\" :1900 }  \n" +
			", { \"Timestamp\" :1461076157,\"close\" :639.5000,\"high\" :639.5000,\"low\" :639.5000,\"open\" :639.5000,\"volume\" :1100 }  \n" +
			", { \"Timestamp\" :1461076350,\"close\" :639.5000,\"high\" :639.5000,\"low\" :639.5000,\"open\" :639.5000,\"volume\" :9300 }  \n" +
			", { \"Timestamp\" :1461076708,\"close\" :639.0000,\"high\" :639.0000,\"low\" :639.0000,\"open\" :639.0000,\"volume\" :6200 }  \n" +
			", { \"Timestamp\" :1461077010,\"close\" :639.0000,\"high\" :639.0000,\"low\" :638.5500,\"open\" :638.5500,\"volume\" :100 }  \n" +
			", { \"Timestamp\" :1461077250,\"close\" :639.0000,\"high\" :639.5000,\"low\" :639.0000,\"open\" :639.0000,\"volume\" :3400 }  \n" +
			", { \"Timestamp\" :1461077580,\"close\" :639.0000,\"high\" :639.5000,\"low\" :639.0000,\"open\" :639.5000,\"volume\" :13900 }  \n" +
			", { \"Timestamp\" :1461077717,\"close\" :639.0000,\"high\" :639.0000,\"low\" :639.0000,\"open\" :639.0000,\"volume\" :3100 }  \n" +
			", { \"Timestamp\" :1461078235,\"close\" :639.0000,\"high\" :639.0000,\"low\" :638.8182,\"open\" :638.8182,\"volume\" :2000 }  \n" +
			", { \"Timestamp\" :1461078546,\"close\" :639.0000,\"high\" :639.0000,\"low\" :639.0000,\"open\" :639.0000,\"volume\" :2100 }  \n" +
			", { \"Timestamp\" :1461078847,\"close\" :637.5000,\"high\" :638.5000,\"low\" :637.5000,\"open\" :638.0000,\"volume\" :22300 }  \n" +
			", { \"Timestamp\" :1461079111,\"close\" :636.5000,\"high\" :637.5000,\"low\" :636.5000,\"open\" :637.5000,\"volume\" :5000 }  \n" +
			", { \"Timestamp\" :1461079448,\"close\" :637.0000,\"high\" :637.5000,\"low\" :637.0000,\"open\" :637.0000,\"volume\" :6200 }  \n" +
			", { \"Timestamp\" :1461079740,\"close\" :637.5000,\"high\" :638.0000,\"low\" :637.0000,\"open\" :637.5000,\"volume\" :10500 }  \n" +
			", { \"Timestamp\" :1461079800,\"close\" :639.5000,\"high\" :639.5000,\"low\" :639.5000,\"open\" :639.5000,\"volume\" :0 }  \n" +
			", { \"Timestamp\" :1461136158,\"close\" :635.0000,\"high\" :635.5000,\"low\" :634.5000,\"open\" :634.5000,\"volume\" :1900 }  \n" +
			", { \"Timestamp\" :1461136258,\"close\" :636.5000,\"high\" :636.5000,\"low\" :636.5000,\"open\" :636.5000,\"volume\" :100 }  \n" +
			", { \"Timestamp\" :1461136664,\"close\" :636.0000,\"high\" :636.0000,\"low\" :636.0000,\"open\" :636.0000,\"volume\" :400 }  \n" +
			", { \"Timestamp\" :1461136931,\"close\" :635.0000,\"high\" :635.0000,\"low\" :635.0000,\"open\" :635.0000,\"volume\" :300 }  \n" +
			", { \"Timestamp\" :1461137327,\"close\" :635.5000,\"high\" :635.5000,\"low\" :635.0000,\"open\" :635.0000,\"volume\" :800 }  \n" +
			", { \"Timestamp\" :1461137524,\"close\" :635.0000,\"high\" :635.0000,\"low\" :635.0000,\"open\" :635.0000,\"volume\" :1400 }  \n" +
			", { \"Timestamp\" :1461137902,\"close\" :635.0000,\"high\" :635.0000,\"low\" :635.0000,\"open\" :635.0000,\"volume\" :1300 }  \n" +
			", { \"Timestamp\" :1461138060,\"close\" :635.0000,\"high\" :635.0000,\"low\" :635.0000,\"open\" :635.0000,\"volume\" :700 }  \n" +
			", { \"Timestamp\" :1461138861,\"close\" :634.0000,\"high\" :634.5000,\"low\" :634.0000,\"open\" :634.5000,\"volume\" :1200 }  \n" +
			", { \"Timestamp\" :1461139303,\"close\" :635.5000,\"high\" :635.5000,\"low\" :635.5000,\"open\" :635.5000,\"volume\" :200 }  \n" +
			", { \"Timestamp\" :1461139541,\"close\" :635.5000,\"high\" :635.5000,\"low\" :635.5000,\"open\" :635.5000,\"volume\" :1100 }  \n" +
			", { \"Timestamp\" :1461140062,\"close\" :636.5000,\"high\" :636.5000,\"low\" :636.0000,\"open\" :636.0000,\"volume\" :0 }  \n" +
			", { \"Timestamp\" :1461140972,\"close\" :636.5000,\"high\" :637.5000,\"low\" :636.5000,\"open\" :637.0000,\"volume\" :2200 }  \n" +
			", { \"Timestamp\" :1461141409,\"close\" :635.5000,\"high\" :635.5000,\"low\" :635.0000,\"open\" :635.0000,\"volume\" :400 }  \n" +
			", { \"Timestamp\" :1461141932,\"close\" :635.0000,\"high\" :635.0000,\"low\" :635.0000,\"open\" :635.0000,\"volume\" :0 }  \n" +
			", { \"Timestamp\" :1461142616,\"close\" :633.5000,\"high\" :633.5000,\"low\" :633.5000,\"open\" :633.5000,\"volume\" :400 }  \n" +
			", { \"Timestamp\" :1461142954,\"close\" :633.5000,\"high\" :634.0000,\"low\" :633.5000,\"open\" :633.5000,\"volume\" :2000 }  \n" +
			", { \"Timestamp\" :1461143320,\"close\" :633.5000,\"high\" :633.5000,\"low\" :633.0000,\"open\" :633.0000,\"volume\" :100 }  \n" +
			", { \"Timestamp\" :1461143471,\"close\" :633.5000,\"high\" :633.5000,\"low\" :633.5000,\"open\" :633.5000,\"volume\" :200 }  \n" +
			", { \"Timestamp\" :1461143987,\"close\" :633.5000,\"high\" :633.5000,\"low\" :633.5000,\"open\" :633.5000,\"volume\" :0 }  \n" +
			", { \"Timestamp\" :1461144572,\"close\" :632.0000,\"high\" :633.5000,\"low\" :632.0000,\"open\" :633.5000,\"volume\" :1200 }  \n" +
			", { \"Timestamp\" :1461144966,\"close\" :632.0000,\"high\" :632.0000,\"low\" :632.0000,\"open\" :632.0000,\"volume\" :200 }  \n" +
			", { \"Timestamp\" :1461146280,\"close\" :632.4500,\"high\" :633.0000,\"low\" :632.4500,\"open\" :632.5000,\"volume\" :1600 }  \n" +
			", { \"Timestamp\" :1461147062,\"close\" :632.5000,\"high\" :632.5000,\"low\" :632.5000,\"open\" :632.5000,\"volume\" :0 }  \n" +
			", { \"Timestamp\" :1461147877,\"close\" :631.0000,\"high\" :632.5000,\"low\" :631.0000,\"open\" :632.5000,\"volume\" :2100 }  \n" +
			", { \"Timestamp\" :1461147930,\"close\" :631.5000,\"high\" :631.5000,\"low\" :631.5000,\"open\" :631.5000,\"volume\" :500 }  \n" +
			", { \"Timestamp\" :1461148353,\"close\" :630.5000,\"high\" :631.0000,\"low\" :629.5000,\"open\" :631.0000,\"volume\" :2200 }  \n" +
			", { \"Timestamp\" :1461148693,\"close\" :630.0000,\"high\" :630.5000,\"low\" :630.0000,\"open\" :630.0000,\"volume\" :1900 }  \n" +
			", { \"Timestamp\" :1461148949,\"close\" :630.0000,\"high\" :630.0000,\"low\" :630.0000,\"open\" :630.0000,\"volume\" :1200 }  \n" +
			", { \"Timestamp\" :1461149279,\"close\" :630.5000,\"high\" :630.5000,\"low\" :630.5000,\"open\" :630.5000,\"volume\" :300 }  \n" +
			", { \"Timestamp\" :1461149524,\"close\" :632.0000,\"high\" :632.0000,\"low\" :630.5000,\"open\" :631.0000,\"volume\" :3200 }  \n" +
			", { \"Timestamp\" :1461149911,\"close\" :632.5000,\"high\" :632.5000,\"low\" :631.5000,\"open\" :631.5000,\"volume\" :1200 }  \n" +
			", { \"Timestamp\" :1461150298,\"close\" :633.0000,\"high\" :633.0000,\"low\" :631.0000,\"open\" :631.0000,\"volume\" :1700 }  \n" +
			", { \"Timestamp\" :1461150543,\"close\" :633.0000,\"high\" :634.5000,\"low\" :633.0000,\"open\" :633.5000,\"volume\" :2800 }  \n" +
			", { \"Timestamp\" :1461150781,\"close\" :633.0000,\"high\" :633.0000,\"low\" :633.0000,\"open\" :633.0000,\"volume\" :300 }  \n" +
			", { \"Timestamp\" :1461151160,\"close\" :632.0000,\"high\" :632.5000,\"low\" :632.0000,\"open\" :632.5000,\"volume\" :1400 }  \n" +
			", { \"Timestamp\" :1461151475,\"close\" :631.0000,\"high\" :632.5000,\"low\" :631.0000,\"open\" :632.0000,\"volume\" :7500 }  \n" +
			", { \"Timestamp\" :1461151535,\"close\" :632.0000,\"high\" :632.0000,\"low\" :632.0000,\"open\" :632.0000,\"volume\" :2100 }  \n" +
			", { \"Timestamp\" :1461152048,\"close\" :630.5000,\"high\" :633.0000,\"low\" :630.5000,\"open\" :632.5000,\"volume\" :7600 }  \n" +
			", { \"Timestamp\" :1461152111,\"close\" :631.5000,\"high\" :631.5000,\"low\" :631.5000,\"open\" :631.5000,\"volume\" :7600 }  \n" +
			", { \"Timestamp\" :1461152951,\"close\" :630.5000,\"high\" :632.0000,\"low\" :630.5000,\"open\" :632.0000,\"volume\" :0 }  \n" +
			", { \"Timestamp\" :1461153297,\"close\" :630.5000,\"high\" :630.5000,\"low\" :630.5000,\"open\" :630.5000,\"volume\" :16600 }  \n" +
			", { \"Timestamp\" :1461153565,\"close\" :630.0000,\"high\" :630.0000,\"low\" :630.0000,\"open\" :630.0000,\"volume\" :0 }  \n" +
			", { \"Timestamp\" :1461153876,\"close\" :631.0000,\"high\" :631.0000,\"low\" :631.0000,\"open\" :631.0000,\"volume\" :2200 }  \n" +
			", { \"Timestamp\" :1461154000,\"close\" :630.5000,\"high\" :630.5000,\"low\" :630.5000,\"open\" :630.5000,\"volume\" :0 }  \n" +
			", { \"Timestamp\" :1461154470,\"close\" :630.5000,\"high\" :630.5000,\"low\" :630.5000,\"open\" :630.5000,\"volume\" :300 }  \n" +
			", { \"Timestamp\" :1461155393,\"close\" :630.0000,\"high\" :630.5000,\"low\" :630.0000,\"open\" :630.5000,\"volume\" :2600 }  \n" +
			", { \"Timestamp\" :1461155641,\"close\" :629.5000,\"high\" :629.5000,\"low\" :629.0000,\"open\" :629.0000,\"volume\" :800 }  \n" +
			", { \"Timestamp\" :1461155885,\"close\" :629.5000,\"high\" :629.5000,\"low\" :629.0000,\"open\" :629.0000,\"volume\" :200 }  \n" +
			", { \"Timestamp\" :1461156267,\"close\" :629.5000,\"high\" :629.5000,\"low\" :629.5000,\"open\" :629.5000,\"volume\" :0 }  \n" +
			", { \"Timestamp\" :1461156782,\"close\" :630.0000,\"high\" :630.0000,\"low\" :630.0000,\"open\" :630.0000,\"volume\" :2400 }  \n" +
			", { \"Timestamp\" :1461157117,\"close\" :630.0000,\"high\" :630.5000,\"low\" :630.0000,\"open\" :630.0000,\"volume\" :17400 }  \n" +
			", { \"Timestamp\" :1461157457,\"close\" :630.5500,\"high\" :630.5500,\"low\" :630.5000,\"open\" :630.5000,\"volume\" :17600 }  \n" +
			", { \"Timestamp\" :1461157780,\"close\" :630.5000,\"high\" :630.5000,\"low\" :630.5000,\"open\" :630.5000,\"volume\" :1100 }  \n" +
			", { \"Timestamp\" :1461158303,\"close\" :630.0000,\"high\" :630.0000,\"low\" :630.0000,\"open\" :630.0000,\"volume\" :0 }  \n" +
			", { \"Timestamp\" :1461158832,\"close\" :630.0000,\"high\" :630.0000,\"low\" :630.0000,\"open\" :630.0000,\"volume\" :0 }  \n" +
			", { \"Timestamp\" :1461159263,\"close\" :629.0000,\"high\" :629.0000,\"low\" :629.0000,\"open\" :629.0000,\"volume\" :1000 }  \n" +
			", { \"Timestamp\" :1461159802,\"close\" :628.5000,\"high\" :629.0000,\"low\" :628.5000,\"open\" :628.5000,\"volume\" :2400 }  \n" +
			", { \"Timestamp\" :1461160596,\"close\" :629.5000,\"high\" :629.5000,\"low\" :629.5000,\"open\" :629.5000,\"volume\" :19400 }  \n" +
			", { \"Timestamp\" :1461161372,\"close\" :629.0000,\"high\" :629.0000,\"low\" :629.0000,\"open\" :629.0000,\"volume\" :500 }  \n" +
			", { \"Timestamp\" :1461161971,\"close\" :628.5000,\"high\" :629.0000,\"low\" :628.5000,\"open\" :629.0000,\"volume\" :0 }  \n" +
			", { \"Timestamp\" :1461162219,\"close\" :629.0000,\"high\" :629.0000,\"low\" :628.5000,\"open\" :628.5000,\"volume\" :19200 }  \n" +
			", { \"Timestamp\" :1461162595,\"close\" :629.5000,\"high\" :629.5000,\"low\" :629.0000,\"open\" :629.0000,\"volume\" :0 }  \n" +
			", { \"Timestamp\" :1461162775,\"close\" :629.5000,\"high\" :629.5000,\"low\" :629.5000,\"open\" :629.5000,\"volume\" :2000 }  \n" +
			", { \"Timestamp\" :1461163419,\"close\" :629.0000,\"high\" :629.0000,\"low\" :629.0000,\"open\" :629.0000,\"volume\" :6500 }  \n" +
			", { \"Timestamp\" :1461163719,\"close\" :629.0000,\"high\" :629.0000,\"low\" :629.0000,\"open\" :629.0000,\"volume\" :3500 }  \n" +
			", { \"Timestamp\" :1461164019,\"close\" :629.0000,\"high\" :629.0500,\"low\" :629.0000,\"open\" :629.0500,\"volume\" :7000 }  \n" +
			", { \"Timestamp\" :1461164318,\"close\" :629.5000,\"high\" :629.5000,\"low\" :629.0000,\"open\" :629.0000,\"volume\" :7500 }  \n" +
			", { \"Timestamp\" :1461164644,\"close\" :629.5000,\"high\" :629.5000,\"low\" :629.0000,\"open\" :629.0000,\"volume\" :1300 }  \n" +
			", { \"Timestamp\" :1461164888,\"close\" :629.5000,\"high\" :630.0000,\"low\" :629.0000,\"open\" :629.0000,\"volume\" :8000 }  \n" +
			", { \"Timestamp\" :1461165219,\"close\" :630.0000,\"high\" :630.5000,\"low\" :629.5000,\"open\" :629.5000,\"volume\" :9700 }  \n" +
			", { \"Timestamp\" :1461165519,\"close\" :630.0000,\"high\" :630.5000,\"low\" :630.0000,\"open\" :630.5000,\"volume\" :2400 }  \n" +
			", { \"Timestamp\" :1461165841,\"close\" :631.5000,\"high\" :631.5000,\"low\" :631.5000,\"open\" :631.5000,\"volume\" :4200 }  \n" +
			", { \"Timestamp\" :1461166167,\"close\" :631.5000,\"high\" :632.0000,\"low\" :631.0000,\"open\" :631.0000,\"volume\" :7200 }  \n" +
			", { \"Timestamp\" :1461166200,\"close\" :632.0000,\"high\" :632.0000,\"low\" :632.0000,\"open\" :632.0000,\"volume\" :0 }  \n" +
			", { \"Timestamp\" :1461222233,\"close\" :632.5000,\"high\" :634.5000,\"low\" :632.0000,\"open\" :634.5000,\"volume\" :700 }  \n" +
			", { \"Timestamp\" :1461222797,\"close\" :633.5000,\"high\" :634.5000,\"low\" :633.5000,\"open\" :634.5000,\"volume\" :1000 }  \n" +
			", { \"Timestamp\" :1461223098,\"close\" :630.5000,\"high\" :631.5000,\"low\" :630.5000,\"open\" :631.5000,\"volume\" :2400 }  \n" +
			", { \"Timestamp\" :1461223245,\"close\" :630.0000,\"high\" :630.0000,\"low\" :630.0000,\"open\" :630.0000,\"volume\" :0 }  \n" +
			", { \"Timestamp\" :1461223621,\"close\" :627.5000,\"high\" :628.5000,\"low\" :627.5000,\"open\" :628.5000,\"volume\" :500 }  \n" +
			", { \"Timestamp\" :1461224027,\"close\" :630.0000,\"high\" :631.0000,\"low\" :628.5000,\"open\" :628.5000,\"volume\" :4400 }  \n" +
			", { \"Timestamp\" :1461224279,\"close\" :630.0000,\"high\" :630.0000,\"low\" :630.0000,\"open\" :630.0000,\"volume\" :500 }  \n" +
			", { \"Timestamp\" :1461224408,\"close\" :629.5000,\"high\" :629.5000,\"low\" :629.5000,\"open\" :629.5000,\"volume\" :100 }  \n" +
			", { \"Timestamp\" :1461224749,\"close\" :631.0000,\"high\" :631.0000,\"low\" :631.0000,\"open\" :631.0000,\"volume\" :1000 }  \n" +
			", { \"Timestamp\" :1461225195,\"close\" :630.0000,\"high\" :631.0000,\"low\" :630.0000,\"open\" :631.0000,\"volume\" :900 }  \n" +
			", { \"Timestamp\" :1461226337,\"close\" :631.0000,\"high\" :631.0000,\"low\" :631.0000,\"open\" :631.0000,\"volume\" :0 }  \n" +
			", { \"Timestamp\" :1461227014,\"close\" :630.0000,\"high\" :630.0000,\"low\" :629.5000,\"open\" :629.5000,\"volume\" :1400 }  \n" +
			", { \"Timestamp\" :1461227276,\"close\" :629.5000,\"high\" :629.5000,\"low\" :629.5000,\"open\" :629.5000,\"volume\" :0 }  \n" +
			", { \"Timestamp\" :1461227579,\"close\" :629.0000,\"high\" :629.0000,\"low\" :629.0000,\"open\" :629.0000,\"volume\" :0 }  \n" +
			", { \"Timestamp\" :1461227726,\"close\" :629.1000,\"high\" :629.1000,\"low\" :629.1000,\"open\" :629.1000,\"volume\" :1300 }  \n" +
			", { \"Timestamp\" :1461228565,\"close\" :630.0000,\"high\" :630.0000,\"low\" :630.0000,\"open\" :630.0000,\"volume\" :0 }  \n" +
			", { \"Timestamp\" :1461228712,\"close\" :629.1000,\"high\" :629.1000,\"low\" :629.1000,\"open\" :629.1000,\"volume\" :1800 }  \n" +
			", { \"Timestamp\" :1461229569,\"close\" :628.5000,\"high\" :628.5000,\"low\" :628.5000,\"open\" :628.5000,\"volume\" :100 }  \n" +
			", { \"Timestamp\" :1461230470,\"close\" :628.5000,\"high\" :628.5000,\"low\" :628.5000,\"open\" :628.5000,\"volume\" :1400 }  \n" +
			", { \"Timestamp\" :1461231122,\"close\" :628.0000,\"high\" :628.5800,\"low\" :628.0000,\"open\" :628.5800,\"volume\" :3900 }  \n" +
			", { \"Timestamp\" :1461231451,\"close\" :628.0000,\"high\" :628.0000,\"low\" :628.0000,\"open\" :628.0000,\"volume\" :4600 }  \n" +
			", { \"Timestamp\" :1461231884,\"close\" :628.0000,\"high\" :628.0000,\"low\" :628.0000,\"open\" :628.0000,\"volume\" :0 }  \n" +
			", { \"Timestamp\" :1461232446,\"close\" :628.0000,\"high\" :628.0000,\"low\" :627.5000,\"open\" :627.5000,\"volume\" :4400 }  \n" +
			", { \"Timestamp\" :1461232965,\"close\" :627.5000,\"high\" :627.5000,\"low\" :627.5000,\"open\" :627.5000,\"volume\" :4700 }  \n" +
			", { \"Timestamp\" :1461233344,\"close\" :627.0000,\"high\" :627.0000,\"low\" :627.0000,\"open\" :627.0000,\"volume\" :0 }  \n" +
			", { \"Timestamp\" :1461233616,\"close\" :627.0000,\"high\" :627.5000,\"low\" :627.0000,\"open\" :627.5000,\"volume\" :500 }  \n" +
			", { \"Timestamp\" :1461233821,\"close\" :625.5000,\"high\" :627.0000,\"low\" :625.5000,\"open\" :627.0000,\"volume\" :8600 }  \n" +
			", { \"Timestamp\" :1461234109,\"close\" :625.5000,\"high\" :625.5000,\"low\" :625.5000,\"open\" :625.5000,\"volume\" :2000 }  \n" +
			", { \"Timestamp\" :1461234485,\"close\" :627.0000,\"high\" :627.0000,\"low\" :626.0000,\"open\" :626.0000,\"volume\" :600 }  \n" +
			", { \"Timestamp\" :1461234613,\"close\" :627.0000,\"high\" :627.0000,\"low\" :627.0000,\"open\" :627.0000,\"volume\" :600 }  \n" +
			", { \"Timestamp\" :1461235035,\"close\" :627.0000,\"high\" :627.0000,\"low\" :626.5000,\"open\" :626.5000,\"volume\" :600 }  \n" +
			", { \"Timestamp\" :1461235440,\"close\" :627.0000,\"high\" :627.5000,\"low\" :627.0000,\"open\" :627.5000,\"volume\" :300 }  \n" +
			", { \"Timestamp\" :1461236149,\"close\" :627.5000,\"high\" :627.5000,\"low\" :627.5000,\"open\" :627.5000,\"volume\" :8000 }  \n" +
			", { \"Timestamp\" :1461236546,\"close\" :629.0000,\"high\" :629.0000,\"low\" :628.5000,\"open\" :629.0000,\"volume\" :0 }  \n" +
			", { \"Timestamp\" :1461236920,\"close\" :627.5000,\"high\" :628.5000,\"low\" :627.5000,\"open\" :628.5000,\"volume\" :2000 }  \n" +
			", { \"Timestamp\" :1461237057,\"close\" :628.0000,\"high\" :628.0000,\"low\" :628.0000,\"open\" :628.0000,\"volume\" :0 }  \n" +
			", { \"Timestamp\" :1461237336,\"close\" :627.5000,\"high\" :627.5000,\"low\" :627.5000,\"open\" :627.5000,\"volume\" :100 }  \n" +
			", { \"Timestamp\" :1461237873,\"close\" :628.0000,\"high\" :628.0000,\"low\" :628.0000,\"open\" :628.0000,\"volume\" :9500 }  \n" +
			", { \"Timestamp\" :1461238015,\"close\" :628.0000,\"high\" :628.0000,\"low\" :628.0000,\"open\" :628.0000,\"volume\" :0 }  \n" +
			", { \"Timestamp\" :1461238305,\"close\" :628.5000,\"high\" :628.5000,\"low\" :628.5000,\"open\" :628.5000,\"volume\" :200 }  \n" +
			", { \"Timestamp\" :1461238751,\"close\" :628.5000,\"high\" :628.5000,\"low\" :628.5000,\"open\" :628.5000,\"volume\" :2700 }  \n" +
			", { \"Timestamp\" :1461238920,\"close\" :629.0000,\"high\" :629.0000,\"low\" :628.5000,\"open\" :628.5000,\"volume\" :700 }  \n" +
			", { \"Timestamp\" :1461239352,\"close\" :628.5000,\"high\" :628.5000,\"low\" :628.5000,\"open\" :628.5000,\"volume\" :200 }  \n" +
			", { \"Timestamp\" :1461239674,\"close\" :627.5000,\"high\" :628.5000,\"low\" :627.5000,\"open\" :628.5000,\"volume\" :800 }  \n" +
			", { \"Timestamp\" :1461240181,\"close\" :629.0000,\"high\" :629.0000,\"low\" :628.0000,\"open\" :628.0000,\"volume\" :700 }  \n" +
			", { \"Timestamp\" :1461240412,\"close\" :628.5000,\"high\" :628.5000,\"low\" :628.5000,\"open\" :628.5000,\"volume\" :300 }  \n" +
			", { \"Timestamp\" :1461240794,\"close\" :629.5000,\"high\" :629.5000,\"low\" :629.0000,\"open\" :629.0000,\"volume\" :100 }  \n" +
			", { \"Timestamp\" :1461240903,\"close\" :629.0000,\"high\" :629.5000,\"low\" :629.0000,\"open\" :629.5000,\"volume\" :1500 }  \n" +
			", { \"Timestamp\" :1461241375,\"close\" :629.0000,\"high\" :629.0000,\"low\" :628.5000,\"open\" :628.5000,\"volume\" :2200 }  \n" +
			", { \"Timestamp\" :1461241796,\"close\" :629.5000,\"high\" :629.5000,\"low\" :628.5000,\"open\" :628.5000,\"volume\" :1900 }  \n" +
			", { \"Timestamp\" :1461241861,\"close\" :629.5000,\"high\" :629.5000,\"low\" :629.0000,\"open\" :629.0000,\"volume\" :2300 }  \n" +
			", { \"Timestamp\" :1461242384,\"close\" :630.0000,\"high\" :630.5000,\"low\" :630.0000,\"open\" :630.5000,\"volume\" :4000 }  \n" +
			", { \"Timestamp\" :1461242648,\"close\" :629.0000,\"high\" :630.0000,\"low\" :629.0000,\"open\" :630.0000,\"volume\" :4100 }  \n" +
			", { \"Timestamp\" :1461242984,\"close\" :628.5000,\"high\" :629.0000,\"low\" :628.5000,\"open\" :629.0000,\"volume\" :2300 }  \n" +
			", { \"Timestamp\" :1461243255,\"close\" :627.5000,\"high\" :627.5000,\"low\" :627.5000,\"open\" :627.5000,\"volume\" :1500 }  \n" +
			", { \"Timestamp\" :1461243856,\"close\" :627.5000,\"high\" :628.5000,\"low\" :627.5000,\"open\" :628.5000,\"volume\" :3200 }  \n" +
			", { \"Timestamp\" :1461244249,\"close\" :628.5000,\"high\" :628.5000,\"low\" :628.5000,\"open\" :628.5000,\"volume\" :1800 }  \n" +
			", { \"Timestamp\" :1461244767,\"close\" :628.5000,\"high\" :628.5000,\"low\" :628.5000,\"open\" :628.5000,\"volume\" :1900 }  \n" +
			", { \"Timestamp\" :1461244863,\"close\" :629.0000,\"high\" :629.0000,\"low\" :629.0000,\"open\" :629.0000,\"volume\" :130100 }  \n" +
			", { \"Timestamp\" :1461245374,\"close\" :627.5000,\"high\" :627.5000,\"low\" :627.5000,\"open\" :627.5000,\"volume\" :0 }  \n" +
			", { \"Timestamp\" :1461245622,\"close\" :628.0000,\"high\" :628.0000,\"low\" :627.5000,\"open\" :628.0000,\"volume\" :1600 }  \n" +
			", { \"Timestamp\" :1461245829,\"close\" :627.0000,\"high\" :627.0000,\"low\" :627.0000,\"open\" :627.0000,\"volume\" :2700 }  \n" +
			", { \"Timestamp\" :1461247761,\"close\" :626.5000,\"high\" :626.5000,\"low\" :626.5000,\"open\" :626.5000,\"volume\" :6900 }  \n" +
			", { \"Timestamp\" :1461248321,\"close\" :625.5000,\"high\" :626.0000,\"low\" :625.5000,\"open\" :626.0000,\"volume\" :4300 }  \n" +
			", { \"Timestamp\" :1461248649,\"close\" :627.0000,\"high\" :627.0000,\"low\" :626.5000,\"open\" :626.5000,\"volume\" :2800 }  \n" +
			", { \"Timestamp\" :1461249559,\"close\" :627.5000,\"high\" :628.0000,\"low\" :627.0000,\"open\" :627.0000,\"volume\" :12400 }  \n" +
			", { \"Timestamp\" :1461249680,\"close\" :626.5000,\"high\" :627.5000,\"low\" :626.5000,\"open\" :627.5000,\"volume\" :14200 }  \n" +
			", { \"Timestamp\" :1461250155,\"close\" :627.0000,\"high\" :627.0000,\"low\" :626.0000,\"open\" :626.0000,\"volume\" :2800 }  \n" +
			", { \"Timestamp\" :1461250754,\"close\" :624.5000,\"high\" :625.5000,\"low\" :624.5000,\"open\" :625.5000,\"volume\" :9700 }  \n" +
			", { \"Timestamp\" :1461250929,\"close\" :625.0000,\"high\" :625.0000,\"low\" :625.0000,\"open\" :625.0000,\"volume\" :3800 }  \n" +
			", { \"Timestamp\" :1461251644,\"close\" :626.0000,\"high\" :626.0000,\"low\" :624.5000,\"open\" :625.0000,\"volume\" :7800 }  \n" +
			", { \"Timestamp\" :1461251852,\"close\" :625.5000,\"high\" :625.5000,\"low\" :625.5000,\"open\" :625.5000,\"volume\" :8200 }  \n" +
			", { \"Timestamp\" :1461252229,\"close\" :624.0000,\"high\" :626.5000,\"low\" :624.0000,\"open\" :626.0000,\"volume\" :46600 }  \n" +
			", { \"Timestamp\" :1461252557,\"close\" :624.0000,\"high\" :624.5000,\"low\" :624.0000,\"open\" :624.0000,\"volume\" :21000 }  \n" +
			", { \"Timestamp\" :1461252600,\"close\" :525.0000,\"high\" :625.0000,\"low\" :625.0000,\"open\" :625.0000,\"volume\" :0 }  \n" +
			", { \"Timestamp\" :1461308400,\"close\" :623.0000,\"high\" :623.0000,\"low\" :623.0000,\"open\" :623.0000,\"volume\" :0 } \n" +
			", { \"Timestamp\" :1461308659,\"close\" :623.5000,\"high\" :625.0000,\"low\" :623.5000,\"open\" :625.0000,\"volume\" :3300 } \n" +
			", { \"Timestamp\" :1461308778,\"close\" :623.0000,\"high\" :623.0000,\"low\" :623.0000,\"open\" :623.0000,\"volume\" :200 } \n" +
			", { \"Timestamp\" :1461309078,\"close\" :622.0000,\"high\" :622.0000,\"low\" :622.0000,\"open\" :622.0000,\"volume\" :500 } \n" +
			", { \"Timestamp\" :1461309334,\"close\" :620.9900,\"high\" :620.9900,\"low\" :620.9900,\"open\" :620.9900,\"volume\" :10100 } \n" +
			", { \"Timestamp\" :1461309373,\"close\" :620.0000,\"high\" :621.5000,\"low\" :620.0000,\"open\" :621.5000,\"volume\" :600 } \n" +
			", { \"Timestamp\" :1461309640,\"close\" :621.5000,\"high\" :621.5000,\"low\" :621.0000,\"open\" :621.0000,\"volume\" :2100 } \n" +
			", { \"Timestamp\" :1461309750,\"close\" :621.0000,\"high\" :621.5000,\"low\" :621.0000,\"open\" :621.5000,\"volume\" :3100 } \n" +
			", { \"Timestamp\" :1461309861,\"close\" :621.0000,\"high\" :621.0000,\"low\" :621.0000,\"open\" :621.0000,\"volume\" :1400 } \n" +
			", { \"Timestamp\" :1461310151,\"close\" :622.0000,\"high\" :622.0000,\"low\" :621.5000,\"open\" :621.5000,\"volume\" :3000 } \n" +
			", { \"Timestamp\" :1461310215,\"close\" :622.5000,\"high\" :622.5000,\"low\" :622.5000,\"open\" :622.5000,\"volume\" :800 } \n" +
			", { \"Timestamp\" :1461310281,\"close\" :623.0000,\"high\" :623.5000,\"low\" :623.0000,\"open\" :623.0000,\"volume\" :3700 } \n" +
			", { \"Timestamp\" :1461310376,\"close\" :623.5000,\"high\" :623.5000,\"low\" :623.5000,\"open\" :623.5000,\"volume\" :200 } \n" +
			", { \"Timestamp\" :1461310391,\"close\" :622.5000,\"high\" :623.0000,\"low\" :622.5000,\"open\" :623.0000,\"volume\" :600 } \n" +
			", { \"Timestamp\" :1461310461,\"close\" :622.5000,\"high\" :622.5000,\"low\" :622.5000,\"open\" :622.5000,\"volume\" :500 } \n" +
			", { \"Timestamp\" :1461310546,\"close\" :622.5000,\"high\" :622.5000,\"low\" :622.5000,\"open\" :622.5000,\"volume\" :300 } \n" +
			", { \"Timestamp\" :1461310607,\"close\" :622.5000,\"high\" :622.5000,\"low\" :622.5000,\"open\" :622.5000,\"volume\" :600 } \n" +
			", { \"Timestamp\" :1461310734,\"close\" :621.5000,\"high\" :622.0000,\"low\" :621.5000,\"open\" :622.0000,\"volume\" :1900 } \n" +
			", { \"Timestamp\" :1461310827,\"close\" :622.0000,\"high\" :622.0000,\"low\" :622.0000,\"open\" :622.0000,\"volume\" :100 } \n" +
			", { \"Timestamp\" :1461311381,\"close\" :621.5000,\"high\" :621.5000,\"low\" :621.5000,\"open\" :621.5000,\"volume\" :100 } \n" +
			", { \"Timestamp\" :1461311460,\"close\" :621.5000,\"high\" :621.5000,\"low\" :621.5000,\"open\" :621.5000,\"volume\" :700 } \n" +
			", { \"Timestamp\" :1461311521,\"close\" :622.5000,\"high\" :622.5000,\"low\" :622.0000,\"open\" :622.0000,\"volume\" :700 } \n" +
			", { \"Timestamp\" :1461311704,\"close\" :622.5000,\"high\" :622.5000,\"low\" :622.5000,\"open\" :622.5000,\"volume\" :500 } \n" +
			", { \"Timestamp\" :1461311777,\"close\" :622.5000,\"high\" :622.5000,\"low\" :622.5000,\"open\" :622.5000,\"volume\" :200 } \n" +
			", { \"Timestamp\" :1461311861,\"close\" :622.0000,\"high\" :622.5000,\"low\" :622.0000,\"open\" :622.5000,\"volume\" :900 } \n" +
			", { \"Timestamp\" :1461312357,\"close\" :623.0000,\"high\" :623.0000,\"low\" :623.0000,\"open\" :623.0000,\"volume\" :800 } \n" +
			", { \"Timestamp\" :1461312557,\"close\" :623.5000,\"high\" :623.5000,\"low\" :623.5000,\"open\" :623.5000,\"volume\" :300 } \n" +
			", { \"Timestamp\" :1461312823,\"close\" :623.5000,\"high\" :623.5000,\"low\" :623.5000,\"open\" :623.5000,\"volume\" :200 } \n" +
			", { \"Timestamp\" :1461312924,\"close\" :623.5000,\"high\" :624.0000,\"low\" :623.5000,\"open\" :624.0000,\"volume\" :300 } \n" +
			", { \"Timestamp\" :1461313274,\"close\" :625.5000,\"high\" :625.5000,\"low\" :625.5000,\"open\" :625.5000,\"volume\" :300 } \n" +
			", { \"Timestamp\" :1461313470,\"close\" :625.0000,\"high\" :625.0000,\"low\" :625.0000,\"open\" :625.0000,\"volume\" :1400 } \n" +
			", { \"Timestamp\" :1461313599,\"close\" :625.5000,\"high\" :625.5000,\"low\" :625.5000,\"open\" :625.5000,\"volume\" :100 } \n" +
			", { \"Timestamp\" :1461314142,\"close\" :626.1000,\"high\" :626.5000,\"low\" :625.5000,\"open\" :625.5000,\"volume\" :10900 } \n" +
			", { \"Timestamp\" :1461314774,\"close\" :625.5000,\"high\" :626.5000,\"low\" :625.5000,\"open\" :626.5000,\"volume\" :0 } \n" +
			", { \"Timestamp\" :1461315031,\"close\" :625.0000,\"high\" :625.5000,\"low\" :625.0000,\"open\" :625.5000,\"volume\" :300 } \n" +
			", { \"Timestamp\" :1461315410,\"close\" :626.0000,\"high\" :626.0000,\"low\" :626.0000,\"open\" :626.0000,\"volume\" :400 } \n" +
			", { \"Timestamp\" :1461315660,\"close\" :626.0000,\"high\" :626.0000,\"low\" :626.0000,\"open\" :626.0000,\"volume\" :400 } \n" +
			", { \"Timestamp\" :1461316007,\"close\" :625.4000,\"high\" :625.4000,\"low\" :625.4000,\"open\" :625.4000,\"volume\" :14900 } \n" +
			", { \"Timestamp\" :1461316053,\"close\" :625.5000,\"high\" :625.5000,\"low\" :625.5000,\"open\" :625.5000,\"volume\" :0 } \n" +
			", { \"Timestamp\" :1461316120,\"close\" :626.0000,\"high\" :626.0000,\"low\" :625.5000,\"open\" :625.5000,\"volume\" :1500 } \n" +
			", { \"Timestamp\" :1461316170,\"close\" :626.0000,\"high\" :626.0000,\"low\" :626.0000,\"open\" :626.0000,\"volume\" :200 } \n" +
			", { \"Timestamp\" :1461316217,\"close\" :626.5000,\"high\" :626.5000,\"low\" :626.5000,\"open\" :626.5000,\"volume\" :600 } \n" +
			", { \"Timestamp\" :1461316274,\"close\" :626.5000,\"high\" :626.5000,\"low\" :626.5000,\"open\" :626.5000,\"volume\" :1100 } \n" +
			", { \"Timestamp\" :1461316412,\"close\" :626.5000,\"high\" :626.5000,\"low\" :626.5000,\"open\" :626.5000,\"volume\" :0 } \n" +
			", { \"Timestamp\" :1461316463,\"close\" :626.5000,\"high\" :626.5000,\"low\" :626.5000,\"open\" :626.5000,\"volume\" :700 } \n" +
			", { \"Timestamp\" :1461316763,\"close\" :628.5000,\"high\" :628.5000,\"low\" :628.5000,\"open\" :628.5000,\"volume\" :0 } \n" +
			", { \"Timestamp\" :1461316815,\"close\" :628.5000,\"high\" :628.5000,\"low\" :628.5000,\"open\" :628.5000,\"volume\" :0 } \n" +
			", { \"Timestamp\" :1461316948,\"close\" :629.0000,\"high\" :629.0000,\"low\" :629.0000,\"open\" :629.0000,\"volume\" :0 } \n" +
			", { \"Timestamp\" :1461317004,\"close\" :629.0000,\"high\" :629.0000,\"low\" :629.0000,\"open\" :629.0000,\"volume\" :100 } \n" +
			", { \"Timestamp\" :1461317128,\"close\" :629.0000,\"high\" :629.5000,\"low\" :629.0000,\"open\" :629.5000,\"volume\" :800 } \n" +
			", { \"Timestamp\" :1461317219,\"close\" :629.5000,\"high\" :629.5000,\"low\" :629.5000,\"open\" :629.5000,\"volume\" :800 } \n" +
			", { \"Timestamp\" :1461317422,\"close\" :629.5000,\"high\" :629.5000,\"low\" :629.5000,\"open\" :629.5000,\"volume\" :200 } \n" +
			", { \"Timestamp\" :1461317740,\"close\" :629.5000,\"high\" :629.5000,\"low\" :629.5000,\"open\" :629.5000,\"volume\" :1400 } \n" +
			", { \"Timestamp\" :1461317941,\"close\" :629.0000,\"high\" :629.0000,\"low\" :629.0000,\"open\" :629.0000,\"volume\" :300 } \n" +
			", { \"Timestamp\" :1461318414,\"close\" :628.0000,\"high\" :628.5000,\"low\" :628.0000,\"open\" :628.5000,\"volume\" :800 } \n" +
			", { \"Timestamp\" :1461318538,\"close\" :628.0000,\"high\" :628.0000,\"low\" :628.0000,\"open\" :628.0000,\"volume\" :500 } \n" +
			", { \"Timestamp\" :1461319215,\"close\" :628.0000,\"high\" :628.0000,\"low\" :628.0000,\"open\" :628.0000,\"volume\" :1500 } \n" +
			", { \"Timestamp\" :1461319425,\"close\" :629.0000,\"high\" :629.0000,\"low\" :629.0000,\"open\" :629.0000,\"volume\" :900 } \n" +
			", { \"Timestamp\" :1461319668,\"close\" :629.5000,\"high\" :629.5000,\"low\" :629.5000,\"open\" :629.5000,\"volume\" :900 } \n" +
			", { \"Timestamp\" :1461319897,\"close\" :630.0000,\"high\" :630.0000,\"low\" :630.0000,\"open\" :630.0000,\"volume\" :300 } \n" +
			", { \"Timestamp\" :1461319952,\"close\" :630.0000,\"high\" :630.0000,\"low\" :630.0000,\"open\" :630.0000,\"volume\" :200 } \n" +
			", { \"Timestamp\" :1461320353,\"close\" :629.0000,\"high\" :629.5000,\"low\" :629.0000,\"open\" :629.5000,\"volume\" :3500 } \n" +
			", { \"Timestamp\" :1461320431,\"close\" :629.0000,\"high\" :629.0000,\"low\" :629.0000,\"open\" :629.0000,\"volume\" :1000 } \n" +
			", { \"Timestamp\" :1461320833,\"close\" :629.0000,\"high\" :629.0000,\"low\" :629.0000,\"open\" :629.0000,\"volume\" :0 } \n" +
			", { \"Timestamp\" :1461320888,\"close\" :629.5000,\"high\" :629.5000,\"low\" :629.5000,\"open\" :629.5000,\"volume\" :100 } \n" +
			", { \"Timestamp\" :1461321051,\"close\" :630.0200,\"high\" :630.0200,\"low\" :630.0200,\"open\" :630.0200,\"volume\" :19100 } \n" +
			", { \"Timestamp\" :1461321197,\"close\" :629.5000,\"high\" :629.5000,\"low\" :629.5000,\"open\" :629.5000,\"volume\" :0 } \n" +
			", { \"Timestamp\" :1461321317,\"close\" :629.0000,\"high\" :629.0000,\"low\" :629.0000,\"open\" :629.0000,\"volume\" :200 } \n" +
			", { \"Timestamp\" :1461321365,\"close\" :628.5000,\"high\" :628.5000,\"low\" :628.5000,\"open\" :628.5000,\"volume\" :400 } \n" +
			", { \"Timestamp\" :1461321485,\"close\" :628.0000,\"high\" :628.0000,\"low\" :628.0000,\"open\" :628.0000,\"volume\" :500 } \n" +
			", { \"Timestamp\" :1461321709,\"close\" :628.0000,\"high\" :628.0000,\"low\" :628.0000,\"open\" :628.0000,\"volume\" :600 } \n" +
			", { \"Timestamp\" :1461321725,\"close\" :628.9000,\"high\" :628.9000,\"low\" :628.9000,\"open\" :628.9000,\"volume\" :19600 } \n" +
			", { \"Timestamp\" :1461321850,\"close\" :628.5000,\"high\" :628.5000,\"low\" :628.5000,\"open\" :628.5000,\"volume\" :0 } \n" +
			", { \"Timestamp\" :1461322250,\"close\" :628.5000,\"high\" :628.5000,\"low\" :628.5000,\"open\" :628.5000,\"volume\" :0 } \n" +
			", { \"Timestamp\" :1461322269,\"close\" :629.0000,\"high\" :629.0000,\"low\" :629.0000,\"open\" :629.0000,\"volume\" :0 } \n" +
			", { \"Timestamp\" :1461322373,\"close\" :629.0000,\"high\" :629.0000,\"low\" :629.0000,\"open\" :629.0000,\"volume\" :100 } \n" +
			", { \"Timestamp\" :1461322439,\"close\" :629.0000,\"high\" :629.0000,\"low\" :629.0000,\"open\" :629.0000,\"volume\" :100 } \n" +
			", { \"Timestamp\" :1461322441,\"close\" :630.0000,\"high\" :630.0000,\"low\" :629.5000,\"open\" :629.5000,\"volume\" :1800 } \n" +
			", { \"Timestamp\" :1461322647,\"close\" :630.0000,\"high\" :630.0000,\"low\" :629.5000,\"open\" :629.5000,\"volume\" :2200 } \n" +
			", { \"Timestamp\" :1461322716,\"close\" :629.0000,\"high\" :629.0000,\"low\" :629.0000,\"open\" :629.0000,\"volume\" :700 } \n" +
			", { \"Timestamp\" :1461322763,\"close\" :629.0000,\"high\" :629.0000,\"low\" :629.0000,\"open\" :629.0000,\"volume\" :0 } \n" +
			", { \"Timestamp\" :1461322942,\"close\" :625.5000,\"high\" :625.5000,\"low\" :625.5000,\"open\" :625.5000,\"volume\" :1000 } \n" +
			", { \"Timestamp\" :1461323003,\"close\" :627.5000,\"high\" :628.0000,\"low\" :627.5000,\"open\" :628.0000,\"volume\" :1300 } \n" +
			", { \"Timestamp\" :1461323133,\"close\" :628.5000,\"high\" :628.5000,\"low\" :628.5000,\"open\" :628.5000,\"volume\" :0 } \n" +
			", { \"Timestamp\" :1461323227,\"close\" :628.5000,\"high\" :628.5000,\"low\" :628.5000,\"open\" :628.5000,\"volume\" :400 } \n" +
			", { \"Timestamp\" :1461323641,\"close\" :629.0000,\"high\" :629.0000,\"low\" :629.0000,\"open\" :629.0000,\"volume\" :0 } \n" +
			", { \"Timestamp\" :1461323756,\"close\" :629.5000,\"high\" :629.5000,\"low\" :629.5000,\"open\" :629.5000,\"volume\" :300 } \n" +
			", { \"Timestamp\" :1461324864,\"close\" :629.5000,\"high\" :630.0000,\"low\" :629.5000,\"open\" :630.0000,\"volume\" :2600 } \n" +
			", { \"Timestamp\" :1461325035,\"close\" :629.5000,\"high\" :629.5000,\"low\" :629.2400,\"open\" :629.2400,\"volume\" :1100 } \n" +
			", { \"Timestamp\" :1461325340,\"close\" :629.0000,\"high\" :629.0000,\"low\" :629.0000,\"open\" :629.0000,\"volume\" :2500 } \n" +
			", { \"Timestamp\" :1461325466,\"close\" :629.5000,\"high\" :629.5000,\"low\" :629.5000,\"open\" :629.5000,\"volume\" :800 } \n" +
			", { \"Timestamp\" :1461326358,\"close\" :629.5000,\"high\" :629.5000,\"low\" :629.5000,\"open\" :629.5000,\"volume\" :700 } \n" +
			", { \"Timestamp\" :1461326645,\"close\" :629.5000,\"high\" :630.0000,\"low\" :629.5000,\"open\" :630.0000,\"volume\" :2800 } \n" +
			", { \"Timestamp\" :1461327155,\"close\" :629.5000,\"high\" :629.5000,\"low\" :629.5000,\"open\" :629.5000,\"volume\" :0 } \n" +
			", { \"Timestamp\" :1461327794,\"close\" :630.0000,\"high\" :630.0000,\"low\" :629.9000,\"open\" :629.9000,\"volume\" :200 } \n" +
			", { \"Timestamp\" :1461327899,\"close\" :630.0000,\"high\" :630.0000,\"low\" :630.0000,\"open\" :630.0000,\"volume\" :1300 } \n" +
			", { \"Timestamp\" :1461327900,\"close\" :630.0000,\"high\" :630.0000,\"low\" :630.0000,\"open\" :630.0000,\"volume\" :200 } \n" +
			", { \"Timestamp\" :1461328161,\"close\" :629.5000,\"high\" :629.5000,\"low\" :629.5000,\"open\" :629.5000,\"volume\" :800 } \n" +
			", { \"Timestamp\" :1461328601,\"close\" :630.0000,\"high\" :630.0000,\"low\" :630.0000,\"open\" :630.0000,\"volume\" :3700 } \n" +
			", { \"Timestamp\" :1461328626,\"close\" :629.5000,\"high\" :630.0000,\"low\" :629.5000,\"open\" :630.0000,\"volume\" :1300 } \n" +
			", { \"Timestamp\" :1461329001,\"close\" :630.0000,\"high\" :630.0000,\"low\" :630.0000,\"open\" :630.0000,\"volume\" :1200 } \n" +
			", { \"Timestamp\" :1461329081,\"close\" :630.0000,\"high\" :630.0000,\"low\" :630.0000,\"open\" :630.0000,\"volume\" :100 } \n" +
			", { \"Timestamp\" :1461329157,\"close\" :630.0000,\"high\" :630.0000,\"low\" :630.0000,\"open\" :630.0000,\"volume\" :300 } \n" +
			", { \"Timestamp\" :1461329257,\"close\" :630.0000,\"high\" :630.0000,\"low\" :630.0000,\"open\" :630.0000,\"volume\" :0 } \n" +
			", { \"Timestamp\" :1461329286,\"close\" :630.0000,\"high\" :630.0000,\"low\" :630.0000,\"open\" :630.0000,\"volume\" :100 } \n" +
			", { \"Timestamp\" :1461329349,\"close\" :630.0000,\"high\" :630.0000,\"low\" :630.0000,\"open\" :630.0000,\"volume\" :600 } \n" +
			", { \"Timestamp\" :1461329400,\"close\" :630.0000,\"high\" :630.0000,\"low\" :630.0000,\"open\" :630.0000,\"volume\" :400 } \n" +
			", { \"Timestamp\" :1461329486,\"close\" :630.0000,\"high\" :630.0000,\"low\" :630.0000,\"open\" :630.0000,\"volume\" :200 } \n" +
			", { \"Timestamp\" :1461329520,\"close\" :630.0000,\"high\" :630.0000,\"low\" :630.0000,\"open\" :630.0000,\"volume\" :600 } \n" +
			", { \"Timestamp\" :1461329767,\"close\" :630.0000,\"high\" :630.0000,\"low\" :630.0000,\"open\" :630.0000,\"volume\" :100 } \n" +
			", { \"Timestamp\" :1461329907,\"close\" :630.0000,\"high\" :630.0000,\"low\" :630.0000,\"open\" :630.0000,\"volume\" :400 } \n" +
			", { \"Timestamp\" :1461329968,\"close\" :630.0000,\"high\" :630.0000,\"low\" :630.0000,\"open\" :630.0000,\"volume\" :2300 } \n" +
			", { \"Timestamp\" :1461330123,\"close\" :629.5000,\"high\" :629.5000,\"low\" :629.5000,\"open\" :629.5000,\"volume\" :400 } \n" +
			", { \"Timestamp\" :1461330249,\"close\" :630.0000,\"high\" :630.0000,\"low\" :630.0000,\"open\" :630.0000,\"volume\" :500 } \n" +
			", { \"Timestamp\" :1461330325,\"close\" :629.5000,\"high\" :629.5000,\"low\" :629.5000,\"open\" :629.5000,\"volume\" :2100 } \n" +
			", { \"Timestamp\" :1461330469,\"close\" :629.0000,\"high\" :629.0000,\"low\" :629.0000,\"open\" :629.0000,\"volume\" :300 } \n" +
			", { \"Timestamp\" :1461330602,\"close\" :629.5000,\"high\" :629.5000,\"low\" :629.5000,\"open\" :629.5000,\"volume\" :100 } \n" +
			", { \"Timestamp\" :1461330804,\"close\" :628.5000,\"high\" :629.0000,\"low\" :628.5000,\"open\" :629.0000,\"volume\" :1700 } \n" +
			", { \"Timestamp\" :1461330872,\"close\" :627.5000,\"high\" :627.5000,\"low\" :627.5000,\"open\" :627.5000,\"volume\" :300 } \n" +
			", { \"Timestamp\" :1461331054,\"close\" :628.0000,\"high\" :628.0000,\"low\" :628.0000,\"open\" :628.0000,\"volume\" :0 } \n" +
			", { \"Timestamp\" :1461331130,\"close\" :628.0000,\"high\" :628.0000,\"low\" :628.0000,\"open\" :628.0000,\"volume\" :500 } \n" +
			", { \"Timestamp\" :1461331298,\"close\" :627.5000,\"high\" :627.5000,\"low\" :627.5000,\"open\" :627.5000,\"volume\" :700 } \n" +
			", { \"Timestamp\" :1461331432,\"close\" :627.5000,\"high\" :627.5000,\"low\" :627.5000,\"open\" :627.5000,\"volume\" :300 } \n" +
			", { \"Timestamp\" :1461331704,\"close\" :628.5000,\"high\" :628.5000,\"low\" :627.5000,\"open\" :627.5000,\"volume\" :400 } \n" +
			", { \"Timestamp\" :1461331755,\"close\" :628.5000,\"high\" :628.5000,\"low\" :628.5000,\"open\" :628.5000,\"volume\" :100 } \n" +
			", { \"Timestamp\" :1461331985,\"close\" :628.5000,\"high\" :628.5000,\"low\" :628.5000,\"open\" :628.5000,\"volume\" :5400 } \n" +
			", { \"Timestamp\" :1461332374,\"close\" :627.0000,\"high\" :627.0000,\"low\" :627.0000,\"open\" :627.0000,\"volume\" :1700 } \n" +
			", { \"Timestamp\" :1461332887,\"close\" :625.5000,\"high\" :625.5000,\"low\" :625.5000,\"open\" :625.5000,\"volume\" :3000 } \n" +
			", { \"Timestamp\" :1461334682,\"close\" :626.0000,\"high\" :626.0000,\"low\" :626.0000,\"open\" :626.0000,\"volume\" :13600 } \n" +
			", { \"Timestamp\" :1461334876,\"close\" :626.5000,\"high\" :626.5000,\"low\" :626.5000,\"open\" :626.5000,\"volume\" :2500 } \n" +
			", { \"Timestamp\" :1461334957,\"close\" :627.0000,\"high\" :627.0000,\"low\" :627.0000,\"open\" :627.0000,\"volume\" :1200 } \n" +
			", { \"Timestamp\" :1461335047,\"close\" :628.0000,\"high\" :628.0000,\"low\" :627.5000,\"open\" :627.5000,\"volume\" :5900 } \n" +
			", { \"Timestamp\" :1461335842,\"close\" :628.0000,\"high\" :628.0000,\"low\" :628.0000,\"open\" :628.0000,\"volume\" :5200 } \n" +
			", { \"Timestamp\" :1461335995,\"close\" :628.0000,\"high\" :628.0000,\"low\" :628.0000,\"open\" :628.0000,\"volume\" :200 } \n" +
			", { \"Timestamp\" :1461336183,\"close\" :627.5000,\"high\" :628.0000,\"low\" :627.5000,\"open\" :628.0000,\"volume\" :3700 } \n" +
			", { \"Timestamp\" :1461336376,\"close\" :627.0000,\"high\" :627.0000,\"low\" :627.0000,\"open\" :627.0000,\"volume\" :100 } \n" +
			", { \"Timestamp\" :1461336437,\"close\" :627.0000,\"high\" :627.0000,\"low\" :627.0000,\"open\" :627.0000,\"volume\" :28100 } \n" +
			", { \"Timestamp\" :1461336714,\"close\" :627.0000,\"high\" :627.0000,\"low\" :627.0000,\"open\" :627.0000,\"volume\" :0 } \n" +
			", { \"Timestamp\" :1461336829,\"close\" :627.5000,\"high\" :627.5000,\"low\" :627.5000,\"open\" :627.5000,\"volume\" :26700 } \n" +
			", { \"Timestamp\" :1461337030,\"close\" :628.0000,\"high\" :628.0000,\"low\" :628.0000,\"open\" :628.0000,\"volume\" :0 } \n" +
			", { \"Timestamp\" :1461337117,\"close\" :628.5000,\"high\" :628.5000,\"low\" :628.5000,\"open\" :628.5000,\"volume\" :0 } \n" +
			", { \"Timestamp\" :1461337198,\"close\" :628.0000,\"high\" :628.0000,\"low\" :628.0000,\"open\" :628.0000,\"volume\" :1100 } \n" +
			", { \"Timestamp\" :1461337387,\"close\" :628.0000,\"high\" :628.0000,\"low\" :628.0000,\"open\" :628.0000,\"volume\" :300 } \n" +
			", { \"Timestamp\" :1461337553,\"close\" :628.5000,\"high\" :629.0000,\"low\" :628.5000,\"open\" :628.5000,\"volume\" :2100 } \n" +
			", { \"Timestamp\" :1461337746,\"close\" :628.5000,\"high\" :629.0000,\"low\" :628.5000,\"open\" :629.0000,\"volume\" :2800 } \n" +
			", { \"Timestamp\" :1461338255,\"close\" :628.5000,\"high\" :628.5000,\"low\" :628.5000,\"open\" :628.5000,\"volume\" :3000 } \n" +
			", { \"Timestamp\" :1461338283,\"close\" :628.5000,\"high\" :628.5000,\"low\" :628.5000,\"open\" :628.5000,\"volume\" :300 } \n" +
			", { \"Timestamp\" :1461338470,\"close\" :629.0000,\"high\" :629.0000,\"low\" :628.5000,\"open\" :628.5000,\"volume\" :3300 } \n" +
			", { \"Timestamp\" :1461338573,\"close\" :628.5000,\"high\" :628.5000,\"low\" :628.5000,\"open\" :628.5000,\"volume\" :1100 } \n" +
			", { \"Timestamp\" :1461338607,\"close\" :628.5000,\"high\" :628.5000,\"low\" :628.0000,\"open\" :628.5000,\"volume\" :1000 } \n" +
			", { \"Timestamp\" :1461338880,\"close\" :629.0000,\"high\" :629.0000,\"low\" :629.0000,\"open\" :629.0000,\"volume\" :3200 } \n" +
			", { \"Timestamp\" :1461339000,\"close\" :629.0000,\"high\" :629.0000,\"low\" :629.0000,\"open\" :629.0000,\"volume\" :0 } \n" +
			"\n" +
			"]\n" +
			"}";
}
