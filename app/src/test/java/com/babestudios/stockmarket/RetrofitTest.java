package com.babestudios.stockmarket;


import com.babestudios.stockmarket.network.converters.AdvancedGsonConverterFactory;
import com.babestudios.stockmarket.network.interfaces.IGetEarningsDatesService;
import com.babestudios.stockmarket.network.interfaces.IGetIndexChangesService;
import com.babestudios.stockmarket.network.interfaces.IGetTickersService;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import rx.observers.TestSubscriber;

@RunWith(JUnit4.class)
public class RetrofitTest {

	@Test
	public void testGetEarningsDates() throws Exception {
		TestSubscriber<String> testSubscriber = new TestSubscriber<>();

		Retrofit r = new Retrofit.Builder()//
				.baseUrl(BuildConfig.STOCK_MARKET_RESEARCH_BASE_URL)//
				.addCallAdapterFactory(RxJavaCallAdapterFactory.create())//
				.addConverterFactory(AdvancedGsonConverterFactory.create())//
				.build();

		IGetEarningsDatesService earningsDatesService =  r.create(IGetEarningsDatesService.class);

		earningsDatesService.getEarningsDates()
				.map(e -> e[0].ticker)
				.subscribe(testSubscriber);
		testSubscriber.assertValue("icp.l");
		testSubscriber.assertNoErrors();

	}

	@Test
	public void testGetTickers() throws Exception {
		TestSubscriber<String> testSubscriber = new TestSubscriber<>();

		Retrofit r = new Retrofit.Builder()//
				.baseUrl(BuildConfig.STOCK_MARKET_RESEARCH_BASE_URL)//
				.addCallAdapterFactory(RxJavaCallAdapterFactory.create())//
				.addConverterFactory(AdvancedGsonConverterFactory.create())//
				.build();

		IGetTickersService getTickersService =  r.create(IGetTickersService.class);

		getTickersService.getTickers()
				.map(e -> e[0].ticker)
				.subscribe(testSubscriber);
		testSubscriber.assertValue("HSBA.L");
		testSubscriber.assertNoErrors();

	}

	@Test
	public void testGetIndexChanges() throws Exception {
		TestSubscriber<Integer> testSubscriber = new TestSubscriber<>();

		Retrofit r = new Retrofit.Builder()//
				.baseUrl(BuildConfig.STOCK_MARKET_RESEARCH_BASE_URL)//
				.addCallAdapterFactory(RxJavaCallAdapterFactory.create())//
				.addConverterFactory(AdvancedGsonConverterFactory.create())//
				.build();

		IGetIndexChangesService getIndexChangesService =  r.create(IGetIndexChangesService.class);

		getIndexChangesService.getIndexChanges()
				.map(e -> e[0].ticker_id)
				.subscribe(testSubscriber);
		testSubscriber.assertValue(1);
		testSubscriber.assertNoErrors();

	}
}
