package com.babestudios.stockmarket;

import android.content.Context;

import com.babestudios.stockmarket.model.dto.pojos.EarningsDateItem;
import com.babestudios.stockmarket.strategies.model.TradeResultObject;
import com.babestudios.stockmarket.utils.SharedPreferencesHelper;
import com.google.gson.Gson;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.Arrays;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.spy;

@RunWith(MockitoJUnitRunner.class)
public class SharedPreferencesHelperTest {

	public SharedPreferencesHelperTest(){

	}
	private Gson gson = new Gson();

	/*@Mock*/
	private SharedPreferencesHelper mockSharedPreferencesHelper;

	@Before
	public void initMocks() {
		mockSharedPreferencesHelper = createMockSharedPreferencesHelper();
	}

	private SharedPreferencesHelper createMockSharedPreferencesHelper() {
		TradeResultObject[] mockTradeResultObjects = gson.fromJson(openedTradesShort, TradeResultObject[].class);
		SharedPreferencesHelper realSharedPreferencesHelper = new SharedPreferencesHelper(Mockito.mock(Context.class));
		mockSharedPreferencesHelper = spy(realSharedPreferencesHelper);
		doReturn(new ArrayList<>(Arrays.asList(mockTradeResultObjects))).when(mockSharedPreferencesHelper).loadOpenedTrades();
		doNothing().when(mockSharedPreferencesHelper).saveTrades(any());
		return mockSharedPreferencesHelper;
	}

	@Test
	public void removeOpenedTest() {
		EarningsDateItem earningsDateItem = new EarningsDateItem();
		earningsDateItem.ticker = "sbry.l";
		TradeResultObject tradeResultObject = new TradeResultObject(2f);
		tradeResultObject.earningsDateItem = earningsDateItem;
		assertThat(mockSharedPreferencesHelper.loadOpenedTrades().size() == 1, is(true));
		ArrayList<TradeResultObject> tradeResultObjects = mockSharedPreferencesHelper.removeOpenedTrade(tradeResultObject);
		assertThat(tradeResultObjects.size() == 0, is(true));
	}

	private static String openedTradesShort = "[{\"closePriceItems\":[],\"earningsDateItem\":{\"ResultDate\":\"Jun 8, 2016 12:00:00 AM\",\"Country\":\"UK\",\"Ticker\":\"sbry.l\"},\"isClosed\":false,\"isLongTrade\":true,\"openPriceItem\":{\"Timestamp\":1465380230,\"close\":246.8,\"high\":247,\"low\":246.8,\"open\":247,\"volume\":0},\"series\":[{\"Timestamp\":1465196651,\"close\":248.1,\"high\":248.1,\"low\":246.2,\"open\":246.2,\"volume\":84500},{\"Timestamp\":1465196943,\"close\":247.6,\"high\":248.6,\"low\":247.5436,\"open\":248.1,\"volume\":13500}],\"stopPercentage\":-2}]";
}
