package com.babestudios.stockmarket;

import com.babestudios.stockmarket.activities.earningsclose.EarningsCloseAlarmUtil;
import com.babestudios.stockmarket.utils.DateUtil;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.util.Calendar;

@RunWith(JUnit4.class)
public class EarningsCloseAlarmUtilTest {

	@Test
	public void getNextStartInMillis(){
		Calendar cal = Calendar.getInstance();
		cal.set(Calendar.YEAR, 2016);
		cal.set(Calendar.MONTH, 4);
		cal.set(Calendar.DAY_OF_MONTH, 31);
		cal.set(Calendar.HOUR_OF_DAY, 20);
		cal.set(Calendar.MINUTE, 0);
		cal.set(Calendar.SECOND, 0);
		cal.set(Calendar.MILLISECOND, 0);
		long nextStartTime = EarningsCloseAlarmUtil.getNextStartInMillis(cal);
		Assert.assertEquals(DateUtil.formatLongDateFromTimeStampMillis(nextStartTime), "2016-06-01 08:00");
	}

}
