package com.babestudios.stockmarket;

import com.babestudios.stockmarket.network.interfaces.IGetEarningsDatesService;
import com.babestudios.stockmarket.network.interfaces.IYahooIntraDayService;

import javax.inject.Named;
import javax.inject.Singleton;

import dagger.Component;
import retrofit2.Retrofit;

@Singleton
@Component(modules = {TestApplicationModule.class})
public interface TestApplicationComponent extends ApplicationComponent {
	void inject(StockMarketResearchApplication application);

	@Named("YahooIntraDayRetrofit")
	Retrofit getYahooIntraDayRetrofit();

	IYahooIntraDayService getYahooIntraDayService();

	@Named("EarningsDatesRetrofit")
	Retrofit getEarningsDatesRetrofit();

	IGetEarningsDatesService getEarningsDatesService();
}
