package com.babestudios.stockmarket;

import com.babestudios.stockmarket.utils.DateUtil;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

@RunWith(JUnit4.class)
public class DateUtilTest {

	@Test
	public void easterSunday() {
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd", Locale.UK);

		Calendar calendar = Calendar.getInstance();
		calendar.set(2017, 3, 16);
		Date date =  calendar.getTime();

		String easterSundayString = formatter.format(date);
		String calculatedEasterSundayString = formatter.format(DateUtil.getEasterSundayDate(2017));
		Assert.assertEquals(easterSundayString, calculatedEasterSundayString);
	}

	@Test
	public void isUkBusinessDate_christmas() {
		Calendar calendar = Calendar.getInstance();
		calendar.set(2021, 11, 28);
		Assert.assertFalse(DateUtil.isUkBusinessDay(calendar));
	}

	@Test
	public void isUkBusinessDate_august_bank_holiday() {
		Calendar calendar = Calendar.getInstance();
		calendar.set(2017, 7, 28);
		Assert.assertFalse(DateUtil.isUkBusinessDay(calendar));
	}

}
