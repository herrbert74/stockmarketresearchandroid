package com.babestudios.stockmarket.activities.earningsstart;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.util.Pair;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ProgressBar;

import com.babestudios.stockmarket.R;
import com.babestudios.stockmarket.activities.chartpage.ChartActivity;
import com.babestudios.stockmarket.activities.ranking.DividerItemDecoration;
import com.babestudios.stockmarket.model.dto.YahooIntraDayStockResponse;
import com.babestudios.stockmarket.model.dto.pojos.EarningsDateItem;
import com.babestudios.stockmarket.strategies.model.TradeResultObject;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

public class EarningsStartActivity extends AppCompatActivity implements EarningsStartAdapter.RecyclerViewClickListener{

	@Bind(R.id.toolbar)
	Toolbar toolbar;

	@Bind(R.id.recycler_view)
	RecyclerView recyclerView;

	@Bind(R.id.progressbar)
	ProgressBar progressbar;

	private EarningsStartController earningsStartController;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		ButterKnife.bind(this);
		setSupportActionBar(toolbar);

		if (toolbar != null) {
			setSupportActionBar(toolbar);
			getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		}

		recyclerView = (RecyclerView) findViewById(R.id.recycler_view);
		recyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
		recyclerView.addItemDecoration(
				new DividerItemDecoration(this));

		progressbar.setVisibility(View.VISIBLE);

		earningsStartController = new EarningsStartController(this);
		earningsStartController.downloadEarningsDates();

	}

	void getResults(ArrayList<TradeResultObject> canOpenTradeResults, List<Pair<EarningsDateItem, YahooIntraDayStockResponse>> earningsItemPairs) {
		progressbar.setVisibility(View.GONE);
		EarningsStartAdapter earningsStartAdapter = new EarningsStartAdapter(EarningsStartActivity.this, canOpenTradeResults, earningsItemPairs);
		recyclerView.setAdapter(earningsStartAdapter);

	}

	@Override
	public void earningDateListClicked(View v, int position, String ticker) {
		Intent i = new Intent(this, ChartActivity.class);
		i.putExtra("Date", System.currentTimeMillis() / 1000L);
		i.putExtra("ticker", ticker);
		startActivity(i);
	}
}
