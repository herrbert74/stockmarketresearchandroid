package com.babestudios.stockmarket.activities.ranking;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.babestudios.stockmarket.R;
import com.babestudios.stockmarket.model.dto.pojos.RankingTickerItem;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Locale;

import butterknife.Bind;
import butterknife.ButterKnife;

public class RankingAdapter extends RecyclerView.Adapter {

	RecyclerViewClickListener mItemListener;

	ArrayList<RankingTickerItem> rankingTickerItems;


	public RankingAdapter(Context c, ArrayList<RankingTickerItem> rankingTickerItems) {
		mItemListener = (RecyclerViewClickListener) c;
		updateData(rankingTickerItems);
	}

	@Override
	public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int i) {
		View itemLayoutView = LayoutInflater.from(parent.getContext())
				.inflate(R.layout.ranking_list_item, parent, false);

		return new RankingViewHolder(itemLayoutView);
	}

	@Override
	public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int position) {
		RankingViewHolder vh = (RankingViewHolder) viewHolder;
		vh.lblRank.setText(String.format(Locale.ENGLISH, "%d.", position + 1));
		vh.lblTicker.setText(rankingTickerItems.get(position).ticker);
		vh.lblName.setText(rankingTickerItems.get(position).name);
		vh.lblPrice.setText(String.format(Locale.ENGLISH, "%.2fp", rankingTickerItems.get(position).price));
		String issuedShares = rankingTickerItems.get(position).issuedShares;
		long lIssuedShares = issuedShares.equals("N/A") ? 0 : Long.parseLong(issuedShares);
		vh.lblSharesOutstanding.setText(String.format(Locale.ENGLISH, "%dm shares", lIssuedShares / 1000000));
		vh.lblMarketCap.setText(String.format(Locale.ENGLISH, "£%.2fm", rankingTickerItems.get(position).marketCap / 1000000));
		vh.lblIndex.setText(rankingTickerItems.get(position).indexString);
	}

	public long getItemId(int position) {
		return position;
	}

	@Override
	public int getItemCount() {
		return rankingTickerItems.size();
	}

	public class RankingViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
		@Bind(R.id.lbl_rank)
		TextView lblRank;
		@Bind(R.id.lbl_ticker)
		TextView lblTicker;
		@Bind(R.id.lbl_name)
		TextView lblName;
		@Bind(R.id.lbl_price)
		TextView lblPrice;
		@Bind(R.id.lbl_shares_outstanding)
		TextView lblSharesOutstanding;
		@Bind(R.id.lbl_market_cap)
		TextView lblMarketCap;
		@Bind(R.id.lbl_index)
		TextView lblIndex;

		public RankingViewHolder(View itemView) {
			super(itemView);
			ButterKnife.bind(this, itemView);
			itemView.setOnClickListener(this);
		}

		@Override
		public void onClick(View v) {
			mItemListener.rankingListClicked(v, this.getLayoutPosition(), rankingTickerItems.get(getLayoutPosition()).ticker);
		}
	}

	public void updateData(ArrayList<RankingTickerItem> rankingTickerItems) {
		Collections.sort(rankingTickerItems);
		this.rankingTickerItems = rankingTickerItems;
	}

	public interface RecyclerViewClickListener {
		void rankingListClicked(View v, int position, String ticker);
	}

}
