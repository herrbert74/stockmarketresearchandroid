package com.babestudios.stockmarket.activities.ranking;

import android.os.AsyncTask;
import android.support.v4.util.Pair;

import com.babestudios.stockmarket.StockMarketResearchApplication;
import com.babestudios.stockmarket.model.base.GoogleStockPriceSeriesItem;
import com.babestudios.stockmarket.model.dto.pojos.IndexChangeItem;
import com.babestudios.stockmarket.model.dto.pojos.RankingTickerItem;
import com.babestudios.stockmarket.model.dto.pojos.TickerItem;
import com.babestudios.stockmarket.network.interfaces.IGetIndexChangesService;
import com.babestudios.stockmarket.network.interfaces.IGetTickersService;
import com.babestudios.stockmarket.network.interfaces.IYahooStatisticsService;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

import retrofit2.Retrofit;
import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class RankingController {
	RankingActivity rankingActivity;

	public RankingController(RankingActivity rankingActivity) {
		super();
		this.rankingActivity = rankingActivity;
	}

	@Inject
	@Named("GetIndexChangesRetrofit")
	Retrofit getIndexChangesRetrofit;

	@Inject
	@Named("GetTickersRetrofit")
	Retrofit getTickersRetrofit;

	@Inject
	@Named("YahooStatisticsRetrofit")
	Retrofit yahooStatisticsRetroFit;

	@Inject
	IGetTickersService getTickersService;

	@Inject
	IGetIndexChangesService getIndexChangesService;

	@Inject
	IYahooStatisticsService yahooStatisticsService;

	public void downloadRankingData() {
		((StockMarketResearchApplication) rankingActivity.getApplication()).getRankingComponent().inject(this);
		Observable.combineLatest(getTickersService.getTickers(), getIndexChangesService.getIndexChanges(), this::fillRankingListWithIndexData)
				.map(list -> new Pair<>(list, getTickersFormList(list)))
				.flatMap(pair -> yahooStatisticsService.getYahooStatistics(pair.second, "sl1", "csv").retry(), (pair, detail) -> updateList(pair.first, detail))
				.subscribeOn(Schedulers.from(AsyncTask.THREAD_POOL_EXECUTOR))
				.observeOn(AndroidSchedulers.mainThread())
				.subscribe(rankingActivity);
	}

	private List<RankingTickerItem> updateList(List<RankingTickerItem> list, String csvResponse) {
		String[] lines = csvResponse.split("\n");
		HashMap<String, Double> priceMap = new HashMap<>();
		List<RankingTickerItem> result = new ArrayList<>();
		for (String line : lines) {
			String[] fields = line.split(",");
			String re = fields[0].replace("\"", "");
			priceMap.put(re, Double.parseDouble(fields[1]));
		}
		for (RankingTickerItem listItem : list) {
			listItem.setPrice(priceMap.get(listItem.ticker));
			result.add(listItem);
		}
		return result;
	}

	private String getTickersFormList(List<RankingTickerItem> list) {
		StringBuilder builder = new StringBuilder();
		for (RankingTickerItem item : list) {
			builder.append(item.ticker + ",");
		}
		builder.setLength(builder.length() - 1);
		return builder.toString();
	}


	private List<RankingTickerItem> fillRankingListWithIndexData(TickerItem[] tickers, IndexChangeItem[] indexChanges) {
		HashSet[] sets = new HashSet[4];
		sets[0] = new HashSet<>();
		sets[1] = new HashSet<>();
		sets[2] = new HashSet<>();
		sets[3] = new HashSet<>();

		List<RankingTickerItem> rankingTickerItems = new ArrayList<>();

		for (IndexChangeItem change : indexChanges) {
			if (change.addition) {
				sets[change.index_id].add(change.ticker_id);
			} else {
				sets[change.index_id].remove(change.ticker_id);
			}
		}
		for (TickerItem item : tickers) {
			int index = 3; //default to ALL SHARE
			if (sets[0].contains(item.id)) {
				index = 0;
			} else if (sets[1].contains(item.id)) {
				index = 1;
			} else if (sets[2].contains(item.id)) {
				index = 2;
			} else if (sets[3].contains(item.id)) {
				index = 3;
			}
			//Add to list only if it's not delisted
			if (index > 0) {
				RankingTickerItem rankingItem = new RankingTickerItem(item, index);
				rankingTickerItems.add(rankingItem);
			}
		}

		return rankingTickerItems;
	}

	private static double getLatestPrice(GoogleStockPriceSeriesItem[] googleStockPriceSeriesItems) {
		if (!(googleStockPriceSeriesItems == null) && googleStockPriceSeriesItems.length > 0) {
			return googleStockPriceSeriesItems[googleStockPriceSeriesItems.length - 1].close;
		} else {
			return 0;
		}
	}

	private static String convertTicker(String ticker) {
		switch (ticker) {
			case "BT-A.L":
				ticker = "BT.L";
				break;
			case "IMT.L":
				ticker = "IMB.L";
				break;
			case "CAPC.L":
				ticker = "CAPC";
				break;
		}
		return ticker;
	}

	private static GoogleStockPriceSeriesItem[] getDailyPrices(String response) {
		String[] lines = response.split("\n");
		if (lines.length > 1) {
			GoogleStockPriceSeriesItem[] items = new GoogleStockPriceSeriesItem[lines.length - 2];
			for (int i = lines.length - 2; i > 0; i--) {

				//Process row
				String[] fields = lines[i].split(",");
				GoogleStockPriceSeriesItem item = new GoogleStockPriceSeriesItem();
				item.Date = fields[0];
				//Always good?!
				item.close = Double.parseDouble(fields[4]);
				if (fields[1].equals("-")) {
					//Take previous close, which is in the next line (i+1), but items is offset by one in other direction (i-1), so we need i
					item.open = items[i].close;
				} else {
					item.open = Double.parseDouble(fields[1]);
				}
				if (fields[2].equals("-")) {
					//Data is not displayed, because it is either equals to the opening or the closing price
					item.high = Math.max(item.open, item.close);
				} else {
					item.high = Double.parseDouble(fields[2]);
				}
				if (fields[3].equals("-")) {
					//Data is not displayed, because it is either equals to the opening or the closing price
					item.low = Math.min(item.open, item.close);
				} else {
					item.low = Double.parseDouble(fields[3]);
				}

				item.volume = Integer.parseInt(fields[5]);
				items[i - 1] = item;
			}
			return items;
		} else {
			return null;
		}
	}
}
