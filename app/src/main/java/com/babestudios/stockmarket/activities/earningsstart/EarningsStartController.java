package com.babestudios.stockmarket.activities.earningsstart;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v4.util.Pair;
import android.util.Log;

import com.babestudios.stockmarket.StockMarketResearchApplication;
import com.babestudios.stockmarket.activities.earningsclose.EarningsCloseService;
import com.babestudios.stockmarket.model.dto.YahooIntraDayStockResponse;
import com.babestudios.stockmarket.model.dto.pojos.EarningsDateItem;
import com.babestudios.stockmarket.network.interfaces.IGetEarningsDatesService;
import com.babestudios.stockmarket.network.interfaces.IYahooIntraDayService;
import com.babestudios.stockmarket.strategies.model.TradeResultObject;
import com.babestudios.stockmarket.strategies.multiclose.MultiCloseStrategy;
import com.babestudios.stockmarket.strategies.multiclose.MultiCloseStrategyParameters;
import com.babestudios.stockmarket.utils.DateUtil;
import com.babestudios.stockmarket.utils.SharedPreferencesHelper;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.inject.Inject;

import rx.Observable;
import rx.Observer;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class EarningsStartController implements Observer<List<Pair<EarningsDateItem, YahooIntraDayStockResponse>>> {
	private EarningsStartActivity earningsStartActivity;

	public EarningsStartController(EarningsStartActivity earningsStartActivity) {
		super();
		this.earningsStartActivity = earningsStartActivity;
	}

	@Inject
	public IGetEarningsDatesService earningsDatesService;

	@Inject
	IYahooIntraDayService yahooIntraDayService;

	@Inject
	SharedPreferencesHelper sharedPreferencesHelper;

	void downloadEarningsDates() {
		((StockMarketResearchApplication) earningsStartActivity.getApplication()).getEarningsStartComponent().inject(this);

		earningsDatesService.getEarningsDates()
				.concatMap(Observable::from)
				.filter(this::isToday)
				.flatMap(earningsDateItem -> yahooIntraDayService.getIntraDayStockPrices(earningsDateItem.ticker, "3"), (Pair::new))
				.toList()
				.subscribeOn(Schedulers.io())
				.observeOn(AndroidSchedulers.mainThread())
				.subscribe(this);
	}

	public boolean isToday(EarningsDateItem t) {
		String today = DateUtil.formatShortDateFromTimeStampMillis(System.currentTimeMillis());
		String earningsDate = DateUtil.formatShortDateFromTimeStampMillis(t.date.getTime());
		return earningsDate.equals(today);
	}

	@Override
	public void onCompleted() {

	}

	@Override
	public void onError(Throwable e) {
		Log.e("test", "onError: " + e.getLocalizedMessage());
	}

	@Override
	public void onNext(List<Pair<EarningsDateItem, YahooIntraDayStockResponse>> earningsItemPairs) {
		ArrayList<TradeResultObject> canOpenTradeResults = getCanOpenTradeResults(earningsItemPairs);
		earningsStartActivity.getResults(canOpenTradeResults, earningsItemPairs);
		sharedPreferencesHelper.addOpenedTrades(canOpenTradeResults);

	}

	@NonNull
	private ArrayList<TradeResultObject> getCanOpenTradeResults(List<Pair<EarningsDateItem, YahooIntraDayStockResponse>> earningsItemPairs) {

		ArrayList<TradeResultObject> canOpenTradeResults = new ArrayList<>();
		for (Pair<EarningsDateItem, YahooIntraDayStockResponse> p : earningsItemPairs) {
			YahooIntraDayStockResponse y = p.second;
			TradeResultObject tradeResult = new TradeResultObject(MultiCloseStrategyParameters.STOP_PERCENTAGE);
			tradeResult.earningsDateItem = p.first;
			tradeResult.series = new ArrayList<>(Arrays.asList(y.series));
			MultiCloseStrategy strategy = new MultiCloseStrategy(tradeResult);

			TradeResultObject canOpenObject = strategy.canOpenTrade();
			canOpenTradeResults.add(canOpenObject);
			if (canOpenObject.openPriceItem != null && canOpenObject.openPriceItem.Timestamp > 0) {
				earningsStartActivity.getApplicationContext().startService(new Intent(earningsStartActivity.getApplicationContext(), EarningsCloseService.class));
			}
		}
		return canOpenTradeResults;
	}
}