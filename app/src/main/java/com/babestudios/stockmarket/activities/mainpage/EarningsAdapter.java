package com.babestudios.stockmarket.activities.mainpage;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.babestudios.stockmarket.R;
import com.babestudios.stockmarket.model.dto.pojos.EarningsDateItem;

import java.text.SimpleDateFormat;
import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;

public class EarningsAdapter extends RecyclerView.Adapter {

	RecyclerViewClickListener mItemListener;

	ArrayList<EarningsDateItem> earningsDateItems;

	public EarningsAdapter(Context c, ArrayList<EarningsDateItem> earningsDateItems) {
		mItemListener = (RecyclerViewClickListener) c;
		updateData(earningsDateItems);
	}

	@Override
	public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int i) {
		View itemLayoutView = LayoutInflater.from(parent.getContext())
				.inflate(R.layout.main_list_item, parent, false);

		return new EarningsViewHolder(itemLayoutView);
	}

	@Override
	public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int position) {
		EarningsViewHolder vh = (EarningsViewHolder) viewHolder;
		SimpleDateFormat ft = new SimpleDateFormat("yyyy-MM-dd");
		vh.lblDate.setText(ft.format(earningsDateItems.get(position).date));
		vh.lblTicker.setText(earningsDateItems.get(position).ticker);
		vh.lblExchange.setText(earningsDateItems.get(position).index);
	}

	public long getItemId(int position) {
		return position;
	}

	@Override
	public int getItemCount() {
		return earningsDateItems.size();
	}

	public class EarningsViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
		@Bind(R.id.lblTicker)
		TextView lblTicker;
		@Bind(R.id.lblEarningsDate)
		TextView lblDate;
		@Bind(R.id.lblExchange)
		TextView lblExchange;

		public EarningsViewHolder(View itemView) {
			super(itemView);
			ButterKnife.bind(this, itemView);
			itemView.setOnClickListener(this);
		}

		@Override
		public void onClick(View v) {
			mItemListener.earningDateListClicked(v, this.getLayoutPosition(), earningsDateItems.get(getLayoutPosition()).ticker);
		}
	}

	public void updateData(ArrayList<EarningsDateItem> earningsDateItems) {
		this.earningsDateItems = earningsDateItems;
	}

	public interface RecyclerViewClickListener {
		void earningDateListClicked(View v, int position, String ticker);
	}

	public ArrayList<EarningsDateItem> getEarningsDateItems() {
		return earningsDateItems;
	}

}
