package com.babestudios.stockmarket.activities.earningsclose;

import com.babestudios.stockmarket.ApplicationComponent;
import com.babestudios.stockmarket.activities.mainpage.MainController;
import com.babestudios.stockmarket.activities.mainpage.MainPageScope;

import dagger.Component;

@EarningsClosePageScope
@Component(dependencies = ApplicationComponent.class)
public interface EarningsCloseComponent {
	void inject(EarningsCloseService earningsCloseService);
	void inject(EarningsCloseAlarmReceiver earningsCloseAlarmReceiver);
}
