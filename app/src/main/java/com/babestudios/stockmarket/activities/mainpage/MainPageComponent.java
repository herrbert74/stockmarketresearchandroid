package com.babestudios.stockmarket.activities.mainpage;

import com.babestudios.stockmarket.ApplicationComponent;

import dagger.Component;

@MainPageScope
@Component(dependencies = ApplicationComponent.class)
public interface MainPageComponent {
	void inject(MainController mainController);
	void inject(MainActivity mainActivity);
}
