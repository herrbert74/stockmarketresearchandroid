package com.babestudios.stockmarket.activities.openedtrades;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ProgressBar;

import com.babestudios.stockmarket.R;
import com.babestudios.stockmarket.activities.chartpage.ChartActivity;
import com.babestudios.stockmarket.activities.ranking.DividerItemDecoration;

import butterknife.Bind;
import butterknife.ButterKnife;

public class OpenedTradesActivity extends AppCompatActivity implements OpenedTradesAdapter.RecyclerViewClickListener {

	@Bind(R.id.toolbar)
	Toolbar toolbar;

	@Bind(R.id.recycler_view)
	RecyclerView recyclerView;

	private OpenedTradesAdapter openedTradesAdapter;
	@Bind(R.id.progressbar)
	ProgressBar progressbar;

	OpenedTradesController openedTradesController;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		ButterKnife.bind(this);
		setSupportActionBar(toolbar);

		if (toolbar != null) {
			setSupportActionBar(toolbar);
			getSupportActionBar().setDisplayHomeAsUpEnabled(true);
			toolbar.setNavigationOnClickListener(v -> onBackPressed());
		}

		recyclerView = (RecyclerView) findViewById(R.id.recycler_view);
		recyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
		recyclerView.addItemDecoration(
				new DividerItemDecoration(this));

		progressbar.setVisibility(View.VISIBLE);

		openedTradesController = new OpenedTradesController(this);
		openedTradesController.updateTradeResults();

	}

	void updateTradeResultsFinished() {
		progressbar.setVisibility(View.GONE);
		openedTradesAdapter = new OpenedTradesAdapter(OpenedTradesActivity.this);
		recyclerView.setAdapter(openedTradesAdapter);

	}

	@Override
	public void openedTradesListClicked(View v, int position, String ticker) {
		Intent i = new Intent(this, ChartActivity.class);
		i.putExtra("Date", System.currentTimeMillis() / 1000L);
		i.putExtra("ticker", ticker);

		startActivity(i);
	}
}
