package com.babestudios.stockmarket.activities.mainpage;

import android.Manifest;
import android.annotation.TargetApi;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.PowerManager;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.babestudios.stockmarket.R;
import com.babestudios.stockmarket.StockMarketResearchApplication;
import com.babestudios.stockmarket.activities.chartpage.ChartActivity;
import com.babestudios.stockmarket.activities.earningsclose.EarningsCloseAlarmUtil;
import com.babestudios.stockmarket.activities.openedtrades.OpenedTradesActivity;
import com.babestudios.stockmarket.activities.ranking.DividerItemDecoration;
import com.babestudios.stockmarket.activities.ranking.RankingActivity;
import com.babestudios.stockmarket.model.dto.pojos.EarningsDateItem;
import com.babestudios.stockmarket.strategies.model.TradeResultObject;
import com.babestudios.stockmarket.utils.DozeReceiver;
import com.babestudios.stockmarket.utils.SharedPreferencesHelper;
import com.sasnee.scribo.DebugHelper;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;
import rx.Observer;

import static android.Manifest.permission.WRITE_EXTERNAL_STORAGE;

public class MainActivity extends AppCompatActivity implements Observer<EarningsDateItem[]>, EarningsAdapter.RecyclerViewClickListener {

	@Inject
	SharedPreferencesHelper sharedPreferencesHelper;

	@Bind(R.id.toolbar)
	Toolbar toolbar;

	@Bind(R.id.recycler_view)
	RecyclerView recyclerView;

	private EarningsAdapter earningsAdapter;
	@Bind(R.id.progressbar)
	ProgressBar progressbar;

	private MainController mainController;

	private MenuItem setAlarmMenuItem;
	private MenuItem alarmOffMenuItem;

	private static final int REQUEST_WRITE_STORAGE = 0;

	private DozeReceiver receiver;

	@Override
	protected void onPostCreate(@Nullable Bundle savedInstanceState) {
		super.onPostCreate(savedInstanceState);
		ArrayList<TradeResultObject> openedTrades = sharedPreferencesHelper.loadOpenedTrades();
		Toast.makeText(this, "trades size: " + (openedTrades == null ? 0 : openedTrades.size()), Toast.LENGTH_LONG).show();
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		DebugHelper.init(this, "CapturedLogs.txt", false);
		((StockMarketResearchApplication) StockMarketResearchApplication.getContext()).getMainPageComponent().inject(this);
		setContentView(R.layout.activity_main);
		ButterKnife.bind(this);
		setSupportActionBar(toolbar);

		createLogFile();

		recyclerView = (RecyclerView) findViewById(R.id.recycler_view);
		recyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
		recyclerView.addItemDecoration(
				new DividerItemDecoration(this));

		progressbar.setVisibility(View.VISIBLE);

		startCloseAlarm();

		setBroadcastReceiver();

		Intent intent = new Intent();
		String packageName = getPackageName();
		PowerManager pm = (PowerManager) getSystemService(Context.POWER_SERVICE);
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
			if (!pm.isIgnoringBatteryOptimizations(packageName)) {
				intent.setAction(Settings.ACTION_REQUEST_IGNORE_BATTERY_OPTIMIZATIONS);
				intent.setData(Uri.parse("package:" + packageName));
				startActivity(intent);
			}
		}
	}

	private void startCloseAlarm() {
		EarningsCloseAlarmUtil.setAlarm(this);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.menu_main, menu);
		setAlarmMenuItem = menu.findItem(R.id.action_set_alarm);
		alarmOffMenuItem = menu.findItem(R.id.action_alarm_off);

		mainController = new MainController();
		mainController.downloadEarningsDates(this);

		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		int id = item.getItemId();

		if (id == R.id.action_ranking) {
			Intent i = new Intent(this, RankingActivity.class);
			startActivity(i);
			return true;
		}
		if (id == R.id.action_opened_trades) {
			Intent i = new Intent(this, OpenedTradesActivity.class);
			startActivity(i);
			return true;
		} else if (id == R.id.action_set_alarm) {
			if (ContextCompat.checkSelfPermission(MainActivity.this, WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
				setAlarm();
			} else {
				ActivityCompat.requestPermissions(MainActivity.this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, REQUEST_WRITE_STORAGE);
			}
			return true;
		} else if (id == R.id.action_alarm_off) {
			mainController.stopAlarm();
			updateAlarmMenu(false);
			return true;
		}

		return super.onOptionsItemSelected(item);
	}

	private void setAlarm() {
		if (mainController.setAlarm(earningsAdapter.getEarningsDateItems())) {
			updateAlarmMenu(true);
		} else {
			Toast.makeText(this, R.string.no_future_dates_in_database, Toast.LENGTH_LONG).show();
		}
	}

	@Override
	public void onCompleted() {

	}

	@Override
	public void onError(Throwable e) {
		DebugHelper.logRequest("test", "DATES ERROR: " + e.toString());
		progressbar.setVisibility(View.GONE);
		Toast.makeText(this, "Error downloading data", Toast.LENGTH_LONG).show();
	}

	@Override
	public void onNext(EarningsDateItem[] earningsDateItems) {
		earningsAdapter = new EarningsAdapter(MainActivity.this, new ArrayList<>(Arrays.asList(earningsDateItems)));
		recyclerView.setAdapter(earningsAdapter);
		progressbar.setVisibility(View.GONE);

		updateAlarmMenu(mainController.isAlarmSet());

	}

	private void updateAlarmMenu(boolean isAlarmSet) {
		if (setAlarmMenuItem != null) {
			setAlarmMenuItem.setVisible(!isAlarmSet);
			setAlarmMenuItem.setShowAsAction(isAlarmSet ? MenuItem.SHOW_AS_ACTION_NEVER : MenuItem.SHOW_AS_ACTION_ALWAYS);
			alarmOffMenuItem.setVisible(isAlarmSet);
			alarmOffMenuItem.setShowAsAction(isAlarmSet ? MenuItem.SHOW_AS_ACTION_ALWAYS : MenuItem.SHOW_AS_ACTION_NEVER);
		}
	}

	@Override
	public void earningDateListClicked(View v, int position, String ticker) {
		Intent i = new Intent(this, ChartActivity.class);
		i.putExtra("Date", System.currentTimeMillis() / 1000L);
		i.putExtra("ticker", ticker);
		startActivity(i);
	}

	@Override
	public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
		//Checking the request code of our request
		if (requestCode == REQUEST_WRITE_STORAGE) {

			//If permission is granted
			if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
				setAlarm();
				//Displaying a toast
				Toast.makeText(this, "Permission granted now you can read the storage", Toast.LENGTH_LONG).show();

			} else {
				//Displaying another toast if permission is not granted
				Toast.makeText(this, "The logs won't be saved to the SD card.", Toast.LENGTH_LONG).show();
			}
		}
	}

	void createLogFile() {
		File file = new File(Environment.getExternalStorageDirectory() + File.separator + "CapturedLogs.txt");
		try {
			if (!file.exists()) {
				file.createNewFile();
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@TargetApi(Build.VERSION_CODES.M)
	private void setBroadcastReceiver() {
		receiver = new DozeReceiver();
		IntentFilter filter = new IntentFilter();
		filter.addAction(PowerManager.ACTION_DEVICE_IDLE_MODE_CHANGED);
		registerReceiver(receiver, filter);
	}

	@Override
	protected void onDestroy() {
		unregisterReceiver(receiver);
		super.onDestroy();
	}
}
