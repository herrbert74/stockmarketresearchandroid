package com.babestudios.stockmarket.activities.earningsclose;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.util.Log;

import com.babestudios.stockmarket.utils.DateUtil;
import com.sasnee.scribo.DebugHelper;

import java.util.Calendar;

public class EarningsCloseAlarmUtil {

	private static int EARNINGS_CLOSE_ALARM = 5689558;
	static int EARNINGS_CLOSE_NOTIFICATION = 5689559;
	public static int ALARM_TRIGGER_DAY = 0;
	private static int ALARM_TRIGGER_HOUR = 8;
	private static int ALARM_TRIGGER_MINUTE = 0;

	public static boolean setAlarm(Context context) {

		AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
		Intent intent = new Intent(context, EarningsCloseAlarmReceiver.class);
		PendingIntent pendingIntent = PendingIntent.getBroadcast(context, EARNINGS_CLOSE_ALARM, intent, 0);

		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
			alarmManager.setExactAndAllowWhileIdle(AlarmManager.RTC, getNextStartInMillis(Calendar.getInstance()), pendingIntent);
		} else {
			alarmManager.setExact(AlarmManager.RTC, getNextStartInMillis(Calendar.getInstance()), pendingIntent);
		}
		DebugHelper.logRequest("test", "Starting close alarm: " + DateUtil.formatLongDateFromTimeStampMillis(getNextStartInMillis(Calendar.getInstance())));
		return false;
	}

	/*
	Trigger the alarm at 8 o'clock next time, either today or tomorrow.
	 */
	public static long getNextStartInMillis(Calendar calCurrentTime) {
		int dayAddition = 1;
		if (calCurrentTime.get(Calendar.HOUR_OF_DAY) < ALARM_TRIGGER_HOUR) {
			dayAddition = 0;
		}
		Calendar cal = Calendar.getInstance();
		cal.set(Calendar.YEAR, calCurrentTime.get(Calendar.YEAR));
		cal.set(Calendar.MONTH, calCurrentTime.get(Calendar.MONTH));
		cal.set(Calendar.DAY_OF_MONTH, calCurrentTime.get(Calendar.DAY_OF_MONTH) + dayAddition);
		cal.set(Calendar.HOUR_OF_DAY, ALARM_TRIGGER_HOUR);
		cal.set(Calendar.MINUTE, ALARM_TRIGGER_MINUTE);
		cal.set(Calendar.SECOND, 0);
		cal.set(Calendar.MILLISECOND, 0);
		return cal.getTimeInMillis();
	}
}
