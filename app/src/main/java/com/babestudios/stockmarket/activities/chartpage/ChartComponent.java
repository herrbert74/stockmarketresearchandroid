package com.babestudios.stockmarket.activities.chartpage;

import com.babestudios.stockmarket.ApplicationComponent;

import dagger.Component;

@ChartScope
@Component(dependencies = ApplicationComponent.class)
public interface ChartComponent {
	void inject(ChartController chartController);
}
