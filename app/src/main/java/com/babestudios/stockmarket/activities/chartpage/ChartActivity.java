package com.babestudios.stockmarket.activities.chartpage;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MotionEvent;
import android.widget.SeekBar;

import com.babestudios.stockmarket.R;
import com.babestudios.stockmarket.model.base.StockPriceSeriesItem;
import com.babestudios.stockmarket.model.dto.YahooIntraDayStockResponse;
import com.github.mikephil.charting.animation.Easing;
import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.LimitLine;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.listener.OnChartGestureListener;
import com.github.mikephil.charting.listener.OnChartValueSelectedListener;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import butterknife.Bind;
import butterknife.ButterKnife;
import rx.Observer;

public class ChartActivity extends AppCompatActivity implements SeekBar.OnSeekBarChangeListener,
		OnChartGestureListener, OnChartValueSelectedListener, Observer<YahooIntraDayStockResponse>{

	LineChart mChart;

	StockPriceSeriesItem[] seriesItems;
	Date resultDate;

	String ticker;

	@Bind(R.id.toolbar)
	Toolbar toolbar;
	@Bind(R.id.chart1)
	LineChart chart1;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.activity_charting);
		ButterKnife.bind(this);

		Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
		setSupportActionBar(toolbar);

		resultDate = new Date(getIntent().getLongExtra("Date", 0L));
		ticker = getIntent().getStringExtra("ticker");

		ChartController chartController = new ChartController(this);
		chartController.downloadPrices(ticker);

	}

	private void buildChart(StockPriceSeriesItem[] seriesItems, String companyName) {
		mChart = (LineChart) findViewById(R.id.chart1);
		mChart.setOnChartGestureListener(this);
		mChart.setOnChartValueSelectedListener(this);
		mChart.setDrawGridBackground(false);

		// no description text
		mChart.setDescription("");
		mChart.setNoDataTextDescription("You need to provide data for the chart.");

		// enable value highlighting
		mChart.setHighlightEnabled(true);

		// enable touch gestures
		mChart.setTouchEnabled(true);

		// enable scaling and dragging
		mChart.setDragEnabled(true);
		mChart.setScaleEnabled(true);
		// mChart.setScaleXEnabled(true);
		// mChart.setScaleYEnabled(true);

		// if disabled, scaling can be done on x- and y-axis separately
		mChart.setPinchZoom(true);

		// set an alternative background color
		// mChart.setBackgroundColor(Color.GRAY);

		// x-axis limit line
		/*LimitLine llXAxis = new LimitLine(10f, "Index 10");
        llXAxis.setLineWidth(4f);
        llXAxis.enableDashedLine(10f, 10f, 0f);
        llXAxis.setLabelPosition(LimitLine.LimitLabelPosition.POS_RIGHT);
        llXAxis.setTextSize(10f);

        XAxis xAxis = mChart.getXAxis();
        //xAxis.addLimitLine(llXAxis); // add x-axis limit line

        LimitLine ll1 = new LimitLine(130f, "Upper Limit");
        ll1.setLineWidth(4f);
        ll1.enableDashedLine(10f, 10f, 0f);
        ll1.setLabelPosition(LimitLine.LimitLabelPosition.POS_RIGHT);
        ll1.setTextSize(10f);

        LimitLine ll2 = new LimitLine(-30f, "Lower Limit");
        ll2.setLineWidth(4f);
        ll2.enableDashedLine(10f, 10f, 0f);
        ll2.setLabelPosition(LimitLine.LimitLabelPosition.POS_RIGHT);
        ll2.setTextSize(10f);*/

		YAxis leftAxis = mChart.getAxisLeft();
		leftAxis.removeAllLimitLines(); // reset all limit lines to avoid overlapping lines
		//leftAxis.addLimitLine(ll1);
		//leftAxis.addLimitLine(ll2);
		leftAxis.setAxisMaxValue(getMaxValue());
		leftAxis.setAxisMinValue(getMinValue());
		leftAxis.setStartAtZero(false);
		//leftAxis.setYOffset(20f);
		leftAxis.enableGridDashedLine(10f, 10f, 0f);

		// limit lines are drawn behind data (and not on top)
		leftAxis.setDrawLimitLinesBehindData(true);

		mChart.getAxisRight().setEnabled(false);

		// add data
		setData(companyName);

//        mChart.setVisibleXRange(20);
//        mChart.setVisibleYRange(20f, AxisDependency.LEFT);
//        mChart.centerViewTo(20, 50, AxisDependency.LEFT);

		mChart.animateX(2500, Easing.EasingOption.EaseInOutQuart);
//        mChart.invalidate();

		// get the legend (only possible after setting data)
		Legend l = mChart.getLegend();

		// modify the legend ...
		// l.setPosition(LegendPosition.LEFT_OF_CHART);
		l.setForm(Legend.LegendForm.LINE);

		// // dont forget to refresh the drawing
		// mChart.invalidate();


	}

	private float getMaxValue() {
		double result = seriesItems[0].close;
		for (int i = 0; i < seriesItems.length; i++) {
			if (seriesItems[i].close > result) {
				result = seriesItems[i].close;
			}
		}
		return (float) result;
	}

	private float getMinValue() {
		double result = seriesItems[0].close;
		for (int i = 0; i < seriesItems.length; i++) {
			if (seriesItems[i].close < result) {
				result = seriesItems[i].close;
			}
		}
		return (float) result;
	}

	private void setData(String companyName) {

		ArrayList<String> xVals = new ArrayList<>();
		for (int i = 0; i < seriesItems.length; i++) {
			Date time = new Date(seriesItems[i].Timestamp * 1000);
			xVals.add(new SimpleDateFormat("MM-dd\nHHmm").format(time));
		}

		ArrayList<Entry> yVals = new ArrayList<>();

		float limitdate = 0f;
		boolean isLimitDateSet = false;

		for (int i = 0; i < seriesItems.length; i++) {


			float val = (float) (seriesItems[i].close);
			yVals.add(new Entry(val, i));

			if (seriesItems[i].Timestamp > (resultDate.getTime() / 1000) && !isLimitDateSet) {
				limitdate = i;
				isLimitDateSet = true;
			}
		}

		//Limit line with the result date
		if (isLimitDateSet) {
			XAxis xAxis = mChart.getXAxis();
			LimitLine ll = new LimitLine(limitdate, "Result date");
			ll.setLineColor(Color.RED);
			ll.setLineWidth(1f);
			ll.setTextColor(Color.BLACK);
			ll.setTextSize(12f);
			xAxis.addLimitLine(ll);
		}

		// create a dataset and give it a type
		LineDataSet set1 = new LineDataSet(yVals, companyName);

		// set1.setFillAlpha(110);

		// set the line to be drawn like this "- - - - - -"
		//set1.enableDashedLine(10f, 5f, 0f);
		//set1.enableDashedHighlightLine(10f, 5f, 0f);
		set1.setColor(R.color.colorPrimaryDark);
		set1.setDrawFilled(true);
		set1.setHighLightColor(Color.WHITE);
		set1.setDrawCircles(false);
		//set1.setCircleColor(getResources().getColor(R.color.primary_dark_green));
		//set1.setBarSpacePercent(0f);
		set1.setFillColor(Color.WHITE);
//        set1.setDrawFilled(true);
		// set1.setShader(new LinearGradient(0, 0, 0, mChart.getHeight(),
		// Color.BLACK, Color.WHITE, Shader.TileMode.MIRROR));

		ArrayList<LineDataSet> dataSets = new ArrayList<>();
		dataSets.add(set1); // add the datasets

		// create a data object with the datasets
		LineData data = new LineData(xVals, dataSets);


		// set data
		mChart.setData(data);
	}

	@Override
	public void onChartLongPressed(MotionEvent me) {

	}

	@Override
	public void onChartDoubleTapped(MotionEvent me) {

	}

	@Override
	public void onChartSingleTapped(MotionEvent me) {

	}

	@Override
	public void onChartFling(MotionEvent me1, MotionEvent me2, float velocityX, float velocityY) {

	}

	@Override
	public void onChartScale(MotionEvent me, float scaleX, float scaleY) {

	}

	@Override
	public void onChartTranslate(MotionEvent me, float dX, float dY) {

	}

	@Override
	public void onValueSelected(Entry e, int dataSetIndex, Highlight h) {

	}

	@Override
	public void onNothingSelected() {

	}

	@Override
	public void onProgressChanged(SeekBar seekBar, int i, boolean b) {

	}

	@Override
	public void onStartTrackingTouch(SeekBar seekBar) {

	}

	@Override
	public void onStopTrackingTouch(SeekBar seekBar) {

	}

	@Override
	public void onCompleted() {

	}

	@Override
	public void onError(Throwable e) {

	}

	@Override
	public void onNext(YahooIntraDayStockResponse stockResponse) {
		seriesItems = stockResponse.series;
		buildChart(stockResponse.series, stockResponse.meta.companyName);
	}
}