package com.babestudios.stockmarket.activities.openedtrades;

import android.support.v4.util.Pair;
import android.util.Log;

import com.babestudios.stockmarket.R;
import com.babestudios.stockmarket.StockMarketResearchApplication;
import com.babestudios.stockmarket.activities.earningsclose.EarningsCloseService;
import com.babestudios.stockmarket.model.dto.YahooIntraDayStockResponse;
import com.babestudios.stockmarket.network.interfaces.IYahooIntraDayService;
import com.babestudios.stockmarket.strategies.model.TradeResultObject;
import com.babestudios.stockmarket.strategies.multiclose.MultiCloseStrategy;
import com.babestudios.stockmarket.utils.SharedPreferencesHelper;
import com.babestudios.stockmarket.utils.StockPriceSeriesUtil;
import com.babestudios.stockmarket.utils.rxjava.RetryWithDelay;
import com.sasnee.scribo.DebugHelper;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;

import javax.inject.Inject;

import rx.Observable;
import rx.Observer;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

class OpenedTradesController implements Observer<List<Pair<TradeResultObject, YahooIntraDayStockResponse>>> {
	private OpenedTradesActivity openedTradesActivity;

	@Inject
	SharedPreferencesHelper sharedPreferencesHelper;

	OpenedTradesController(OpenedTradesActivity openedTradesActivity) {
		super();
		this.openedTradesActivity = openedTradesActivity;
	}

	@Inject
	IYahooIntraDayService yahooIntraDayService;

	void updateTradeResults() {
		((StockMarketResearchApplication) openedTradesActivity.getApplication()).getOpenedTradesComponent().inject(this);
		ArrayList<TradeResultObject> tradeResults = sharedPreferencesHelper.loadOpenedTrades();
		Calendar cal = Calendar.getInstance();

		if (cal.get(Calendar.HOUR_OF_DAY) > 16 || (cal.get(Calendar.HOUR_OF_DAY) > 15 && cal.get(Calendar.MINUTE) > 30)) {
			Observable.from(tradeResults)
					.flatMap(tradeResultObject -> yahooIntraDayService.getIntraDayStockPrices(tradeResultObject.earningsDateItem.ticker, "5"), Pair::new)
					.retryWhen(new RetryWithDelay(10, 6000))
					.toList()
					.subscribeOn(Schedulers.io())
					.observeOn(AndroidSchedulers.mainThread())
					.subscribe(this);
		} else {
			openedTradesActivity.updateTradeResultsFinished();
		}
	}

	@Override
	public void onCompleted() {

	}

	@Override
	public void onError(Throwable e) {
		DebugHelper.logRequest("test", "onError: " + Arrays.toString(e.getStackTrace()));
	}

	@Override
	public void onNext(List<Pair<TradeResultObject, YahooIntraDayStockResponse>> list) {
		ArrayList<TradeResultObject> refreshedTradeResultObjects = new ArrayList<>();
		for (int i = 0; i < list.size(); i++) {
			list.get(i).first.series = StockPriceSeriesUtil.addAllNonDuplicates(list.get(i).first.series, new ArrayList<>(Arrays.asList(list.get(i).second.series)));
			MultiCloseStrategy multiCloseStrategy = new MultiCloseStrategy(list.get(i).first);
			TradeResultObject tradeResultObject = multiCloseStrategy.canCloseTrade();
			refreshedTradeResultObjects.add(tradeResultObject);
			if (tradeResultObject.isClosed) {
				EarningsCloseService.sendStopNotification( openedTradesActivity, openedTradesActivity.getString(R.string.trade_closed) + " " + tradeResultObject.earningsDateItem.ticker, tradeResultObject);
			} else if (tradeResultObject.closePriceItems.size() > list.get(i).first.closePriceItems.size()) {
				EarningsCloseService.sendStopNotification( openedTradesActivity, openedTradesActivity.getString(R.string.limit_hit), tradeResultObject);
				Log.d("test", "onNext: not closed");
			}
		}
		sharedPreferencesHelper.saveTrades(refreshedTradeResultObjects);
		openedTradesActivity.updateTradeResultsFinished();
	}
}