package com.babestudios.stockmarket.activities.ranking;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ProgressBar;

import com.babestudios.stockmarket.R;
import com.babestudios.stockmarket.activities.chartpage.ChartActivity;
import com.babestudios.stockmarket.model.dto.pojos.RankingTickerItem;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;
import rx.Observer;

public class RankingActivity extends AppCompatActivity implements Observer<Object>, RankingAdapter.RecyclerViewClickListener {

	@Bind(R.id.toolbar)
	Toolbar toolbar;
	@Bind(R.id.recycler_view)
	RecyclerView recyclerView;

	RankingAdapter rankingAdapter;
	@Bind(R.id.progressbar)
	ProgressBar progressbar;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.activity_ranking);
		ButterKnife.bind(this);

		Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
		setSupportActionBar(toolbar);
		if (toolbar != null) {
			setSupportActionBar(toolbar);
			getSupportActionBar().setDisplayHomeAsUpEnabled(true);
			toolbar.setNavigationOnClickListener(v -> onBackPressed());
		}

		recyclerView = (RecyclerView) findViewById(R.id.recycler_view);
		recyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
		recyclerView.addItemDecoration(
				new DividerItemDecoration(this));

		progressbar.setVisibility(View.VISIBLE);

		RankingController rankingController = new RankingController(this);
		rankingController.downloadRankingData();
	}

	@Override
	public void onCompleted() {
	}

	@Override
	public void onError(Throwable e) {
		e.printStackTrace();
	}

	@Override
	public void onNext(Object stockResponse) {
		rankingAdapter = new RankingAdapter(RankingActivity.this, (ArrayList<RankingTickerItem>) stockResponse);
		recyclerView.setAdapter(rankingAdapter);
		progressbar.setVisibility(View.GONE);
	}

	@Override
	public void rankingListClicked(View v, int position, String ticker) {
		Intent i = new Intent(this, ChartActivity.class);
		i.putExtra("Date", System.currentTimeMillis() / 1000L);
		i.putExtra("ticker", ticker);

		startActivity(i);
	}
}