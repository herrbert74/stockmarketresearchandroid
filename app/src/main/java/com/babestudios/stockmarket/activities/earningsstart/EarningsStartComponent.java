package com.babestudios.stockmarket.activities.earningsstart;

import com.babestudios.stockmarket.ApplicationComponent;

import dagger.Component;

@EarningsStartScope
@Component(dependencies = ApplicationComponent.class)
public interface EarningsStartComponent {
	void inject(EarningsStartController earningsStartController);
}
