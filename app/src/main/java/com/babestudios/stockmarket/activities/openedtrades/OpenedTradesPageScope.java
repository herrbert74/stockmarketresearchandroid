package com.babestudios.stockmarket.activities.openedtrades;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import javax.inject.Scope;

@Scope
@Retention(RetentionPolicy.RUNTIME)
public @interface OpenedTradesPageScope {
}
