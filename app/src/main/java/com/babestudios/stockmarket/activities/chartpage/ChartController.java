package com.babestudios.stockmarket.activities.chartpage;

import com.babestudios.stockmarket.StockMarketResearchApplication;
import com.babestudios.stockmarket.network.interfaces.IYahooIntraDayService;

import javax.inject.Inject;
import javax.inject.Named;

import retrofit2.Retrofit;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class ChartController {
	ChartActivity chartActivity;

	public ChartController(ChartActivity chartActivity) {
		super();
		this.chartActivity = chartActivity;
	}

	@Inject
	@Named("YahooIntraDayRetrofit")
	Retrofit yahooIntraDayRetrofit;

	@Inject
	IYahooIntraDayService yahooIntraDayService;

	public void downloadPrices(String t) {
		((StockMarketResearchApplication) chartActivity.getApplication()).getChartComponent().inject(this);
		yahooIntraDayService.getIntraDayStockPrices(t, "10")
				.subscribeOn(Schedulers.io())
				.observeOn(AndroidSchedulers.mainThread())
				.subscribe(chartActivity);
	}

}
