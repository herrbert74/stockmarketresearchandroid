package com.babestudios.stockmarket.activities.earningsclose;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.widget.TextView;

import com.babestudios.stockmarket.R;
import com.babestudios.stockmarket.strategies.model.TradeResultObject;

import java.util.Locale;

import butterknife.Bind;
import butterknife.ButterKnife;

public class EarningsCloseActivity extends AppCompatActivity {

	@Bind(R.id.textview)
	TextView textview;

	@Bind(R.id.toolbar)
	Toolbar toolbar;

	@Override
	protected void onCreate(@Nullable Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_close);
		ButterKnife.bind(this);
		setSupportActionBar(toolbar);
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		Bundle b = getIntent().getExtras();
		if (b != null) {
			TradeResultObject tradeResult = (TradeResultObject) b.get("tradeResult");
			if(tradeResult != null) {
				textview.setText(String.format(Locale.UK, "%s %.2f%% %.2f%%", tradeResult.earningsDateItem.ticker, tradeResult.openPriceItem.close, tradeResult.closePriceItems.get(tradeResult.closePriceItems.size() - 1).close));
			}
		}
	}
}
