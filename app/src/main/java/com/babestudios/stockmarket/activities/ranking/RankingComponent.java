package com.babestudios.stockmarket.activities.ranking;

import com.babestudios.stockmarket.network.interfaces.IGetIndexChangesService;
import com.babestudios.stockmarket.network.interfaces.IGetTickersService;
import com.babestudios.stockmarket.network.interfaces.IYahooStatisticsService;

import javax.inject.Named;
import javax.inject.Singleton;

import dagger.Component;
import retrofit2.Retrofit;

@Singleton
@Component(modules = {RankingModule.class})
public interface RankingComponent {
	void inject(RankingController rankingController);

	@Named("GetTickersRetrofit")
	Retrofit getTickersRetrofit();

	IGetTickersService getTickersService();

	@Named("GetIndexChangesRetrofit")
	Retrofit getIndexChangesRetrofit();

	IGetIndexChangesService getIndexChangesService();

	/*@Named("GoogleDailyRetrofit")
	Retrofit getGoogleDailyRetrofit();

	IGoogleDailyService googleDailyService();
*/
	@Named("YahooStatisticsRetrofit")
	Retrofit getYahooStatisticsRetrofit();

	IYahooStatisticsService yahooStatisticsService();
}
