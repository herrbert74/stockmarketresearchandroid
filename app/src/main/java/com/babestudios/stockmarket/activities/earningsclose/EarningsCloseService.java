package com.babestudios.stockmarket.activities.earningsclose;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Handler;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.TaskStackBuilder;
import android.support.v4.util.Pair;
import android.util.Log;

import com.babestudios.stockmarket.R;
import com.babestudios.stockmarket.StockMarketResearchApplication;
import com.babestudios.stockmarket.model.dto.YahooIntraDayStockResponse;
import com.babestudios.stockmarket.network.interfaces.IYahooIntraDayService;
import com.babestudios.stockmarket.strategies.model.TradeResultObject;
import com.babestudios.stockmarket.strategies.multiclose.MultiCloseStrategy;
import com.babestudios.stockmarket.utils.SharedPreferencesHelper;
import com.babestudios.stockmarket.utils.StockPriceSeriesUtil;
import com.sasnee.scribo.DebugHelper;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;
import java.util.UUID;

import javax.inject.Inject;

import rx.Observable;
import rx.Observer;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class EarningsCloseService extends Service implements Observer<List<Pair<TradeResultObject, YahooIntraDayStockResponse>>> {

	private static final int NETWORK_INTERVAL_SECONDS = 30;
	@Inject
	IYahooIntraDayService yahooIntraDayService;

	@Inject
	SharedPreferencesHelper sharedPreferencesHelper;

	private Handler handler = new Handler();

	@Override
	public void onCreate() {
		super.onCreate();
		((StockMarketResearchApplication) StockMarketResearchApplication.getContext()).getEarningsCloseComponent().inject(this);
		closeTrade();
	}

	private void closeTrade() {
		ArrayList<TradeResultObject> tradeResults = sharedPreferencesHelper.loadOpenedTrades();
		if (tradeResults == null || tradeResults.size() == 0) {
			DebugHelper.logRequest("test", "Stopping close service. No trades.");
			stopSelf();
		} else {
			Calendar cal = Calendar.getInstance();
			if (cal.get(Calendar.HOUR_OF_DAY) > 16 || (cal.get(Calendar.HOUR_OF_DAY) > 15 && cal.get(Calendar.MINUTE) > 30)) {
				DebugHelper.logRequest("test", "Stopping close service. Time is up.");
				stopSelf();
			} else {
				Observable.from(tradeResults)
						.flatMap(tradeResultObject -> yahooIntraDayService.getIntraDayStockPrices(tradeResultObject.earningsDateItem.ticker, "1"), Pair::new)
						.toList()
						.subscribeOn(Schedulers.io())
						.observeOn(AndroidSchedulers.mainThread())
						.subscribe(this);
			}
		}
	}

	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		// If we get killed, after returning from here, restart
		return START_STICKY;
	}

	@Nullable
	@Override
	public IBinder onBind(Intent intent) {
		return null;
	}

	@Override
	public void onCompleted() {

	}

	@Override
	public void onError(Throwable e) {
		DebugHelper.logRequest("test", "EarningsCloseService onError: " + e.getLocalizedMessage());
		//Log.e("test", "onError: " + e.getLocalizedMessage());
		handler.postDelayed(this::closeTrade, NETWORK_INTERVAL_SECONDS * 1000);
	}

	@Override
	public void onNext(List<Pair<TradeResultObject, YahooIntraDayStockResponse>> list) {
		ArrayList<TradeResultObject> refreshedTradeResultObjects = new ArrayList<>();
		for (int i = 0; i < list.size(); i++) {
			list.get(i).first.series = StockPriceSeriesUtil.addAllNonDuplicates(list.get(i).first.series, new ArrayList<>(Arrays.asList(list.get(i).second.series)));
			MultiCloseStrategy multiCloseStrategy = new MultiCloseStrategy(list.get(i).first);
			TradeResultObject tradeResultObject = multiCloseStrategy.canCloseTrade();
			refreshedTradeResultObjects.add(tradeResultObject);
			if (tradeResultObject.isClosed) {
				sendStopNotification(this, getString(R.string.trade_closed) + " " + tradeResultObject.earningsDateItem.ticker, tradeResultObject);
				DebugHelper.logRequest("test", "EarningsCloseService closed: " + tradeResultObject.earningsDateItem.ticker);
			} else if (tradeResultObject.closePriceItems.size() > list.get(i).first.closePriceItems.size()) {
				sendStopNotification(this, getString(R.string.limit_hit), tradeResultObject);
				DebugHelper.logRequest("test", "EarningsCloseService limit hit: " + tradeResultObject.earningsDateItem.ticker);
			} else {
				DebugHelper.logRequest("test", "EarningsCloseService not closed: " + tradeResultObject.earningsDateItem.ticker);
			}
		}
		sharedPreferencesHelper.saveTrades(refreshedTradeResultObjects);
		handler.postDelayed(this::closeTrade, NETWORK_INTERVAL_SECONDS * 1000);
	}

	public static void sendStopNotification(Context context, String message, TradeResultObject tradeResultObject) {
		Intent intent = new Intent(context, EarningsCloseActivity.class);

		// Needed for getting extras when there are more than one pending intents. See http://stackoverflow.com/a/3128271/2162226
		intent.setAction(UUID.randomUUID().toString());
		intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
		intent.putExtra("tradeResult", tradeResultObject);

		TaskStackBuilder stackBuilder = TaskStackBuilder.create(context);
		// Adds the back stack
		stackBuilder.addParentStack(EarningsCloseActivity.class);
		// Adds the Intent to the top of the stack
		stackBuilder.addNextIntent(intent);

		PendingIntent pendingIntent = stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);

		Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
		final String packageName = context.getPackageName();
		NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(context)
				.setSmallIcon(R.drawable.ic_event_available_black_24px)
				.setLargeIcon(BitmapFactory.decodeResource(null, R.drawable.ic_event_available_black_24px))
				.setContentTitle(context.getResources().getString(R.string.app_name))
				.setContentText(message)
				.setStyle(new NotificationCompat.BigTextStyle().bigText(message))
				.setAutoCancel(true)
				//.setSound(Uri.parse("android.resource://" + packageName + "/" + R.raw.cash_register_purchase))
				.setContentIntent(pendingIntent);
		NotificationManager notificationManager =
				(NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
		notificationManager.notify(EarningsCloseAlarmUtil.EARNINGS_CLOSE_NOTIFICATION, notificationBuilder.build());
	}
}
