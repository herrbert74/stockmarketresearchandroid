package com.babestudios.stockmarket.activities.chartpage;

import com.babestudios.stockmarket.BuildConfig;
import com.babestudios.stockmarket.network.converters.AdvancedGsonConverterFactory;
import com.babestudios.stockmarket.network.interfaces.IYahooIntraDayService;

import javax.inject.Named;
import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;

@Module
public class ChartModule {

	@Provides
	@Singleton
	@Named("YahooIntraDayRetrofit")
	Retrofit provideYahooIntraDayRetrofit() {
		return new Retrofit.Builder()//
				.baseUrl(BuildConfig.YAHOO_INTRA_DAY_BASE_URL)//
				.addCallAdapterFactory(RxJavaCallAdapterFactory.create())//
				.addConverterFactory(AdvancedGsonConverterFactory.create())//
				.build();
	}

	@Provides
	@Singleton
	IYahooIntraDayService provideYahooIntraDayService(@Named("YahooIntraDayRetrofit") Retrofit retroFit) {
		return retroFit.create(IYahooIntraDayService.class);
	}
}
