package com.babestudios.stockmarket.activities.mainpage;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.util.Log;

import com.babestudios.stockmarket.model.dto.pojos.EarningsDateItem;
import com.babestudios.stockmarket.utils.DateUtil;
import com.babestudios.stockmarket.utils.IPredicate;
import com.babestudios.stockmarket.utils.Predicate;
import com.sasnee.scribo.DebugHelper;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;

public class EarningsStartAlarmUtil {

	private static int EARNINGS_ALARM = 5689556;
	static int EARNINGS_NOTIFICATION = 5689557;
	private static int ALARM_TRIGGER_DAY = 0;
	public static int ALARM_TRIGGER_HOUR = 11;
	private static int ALARM_TRIGGER_MINUTE = 0;


	static boolean setAlarm(Context context, ArrayList<EarningsDateItem> earningsDateItems, boolean test) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTimeInMillis(System.currentTimeMillis());
		AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
		Intent intent = new Intent(context, EarningsAlarmReceiver.class);
		PendingIntent pendingIntent = PendingIntent.getBroadcast(context, EARNINGS_ALARM, intent, 0);
		long nextDateInMillis = getNextDateInMillis(earningsDateItems);
		if (test) {
			calendar.setTimeInMillis(System.currentTimeMillis() + 60 * 1000);
			alarmManager.set(AlarmManager.RTC, calendar.getTimeInMillis(), pendingIntent);
			return true;
		} else if (nextDateInMillis > 0) {
			DebugHelper.logRequest("test", "Scheduling start alarm: " + DateUtil.formatLongDateFromTimeStampMillis(nextDateInMillis + ALARM_TRIGGER_HOUR * 60 * 60 * 1000 + ALARM_TRIGGER_MINUTE * 60 * 1000));

			calendar.setTimeInMillis(nextDateInMillis + ALARM_TRIGGER_HOUR * 60 * 60 * 1000 + ALARM_TRIGGER_MINUTE * 60 * 1000);
			if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
				alarmManager.setExactAndAllowWhileIdle(AlarmManager.RTC, calendar.getTimeInMillis(), pendingIntent);
			} else {
				alarmManager.setExact(AlarmManager.RTC, calendar.getTimeInMillis(), pendingIntent);
			}
			return true;
		} else {
			return false;
		}
	}

	static long getNextDateInMillis(ArrayList<EarningsDateItem> earningsDateItems) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTimeInMillis(System.currentTimeMillis());
		ArrayList<EarningsDateItem> filteredEarningsDateItems = new ArrayList<>(getDateAfter(earningsDateItems, calendar.getTimeInMillis() + ALARM_TRIGGER_DAY * 24 * 60 * 60 * 1000 - ALARM_TRIGGER_HOUR * 60 * 60 * 1000));
		if (filteredEarningsDateItems.size() > 0) {
			return filteredEarningsDateItems.get(0).date.getTime();
		} else {
			return 0;
		}
	}

	public static boolean isAlarmSet(Context context) {
		Intent intent = new Intent(context, EarningsAlarmReceiver.class);
		PendingIntent i = PendingIntent.getBroadcast(context, EARNINGS_ALARM, intent, PendingIntent.FLAG_NO_CREATE);
		return (i != null);
	}

	public static void stopAlarm(Context context) {
		AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
		Intent intent = new Intent(context, EarningsAlarmReceiver.class);
		PendingIntent pendingIntent = PendingIntent.getBroadcast(context, EARNINGS_ALARM, intent, PendingIntent.FLAG_UPDATE_CURRENT);
		alarmManager.cancel(pendingIntent);
		pendingIntent.cancel();
	}

	public static Collection getDateAfter(ArrayList<EarningsDateItem> dateItems, final long date) {
		return Predicate.filter(dateItems, getEarningsDateItemPredicate(date));
	}

	public static IPredicate getEarningsDateItemPredicate(final Long date) {
		return new IPredicate<EarningsDateItem>() {
			@Override
			public boolean apply(EarningsDateItem input) {
				return input.date.getTime() > date;
			}
		};
	}

}
