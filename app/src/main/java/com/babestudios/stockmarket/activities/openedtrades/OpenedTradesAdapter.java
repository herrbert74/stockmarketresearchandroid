package com.babestudios.stockmarket.activities.openedtrades;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.babestudios.stockmarket.R;
import com.babestudios.stockmarket.StockMarketResearchApplication;
import com.babestudios.stockmarket.model.base.StockPriceSeriesItem;
import com.babestudios.stockmarket.strategies.model.TradeResultObject;
import com.babestudios.stockmarket.utils.DateUtil;
import com.babestudios.stockmarket.utils.SharedPreferencesHelper;
import com.babestudios.stockmarket.utils.StockPriceSeriesUtil;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Locale;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;

class OpenedTradesAdapter extends RecyclerView.Adapter<OpenedTradesAdapter.EarningsViewHolder> {

	@Inject
	SharedPreferencesHelper sharedPreferencesHelper;

	private RecyclerViewClickListener mItemListener;

	private ArrayList<TradeResultObject> tradeResults = new ArrayList<>();

	OpenedTradesAdapter(Context c) {
		((StockMarketResearchApplication) ((OpenedTradesActivity)c).getApplication()).getOpenedTradesComponent().inject(this);
		mItemListener = (RecyclerViewClickListener) c;
		tradeResults = sharedPreferencesHelper.loadOpenedTrades();
	}

	@Override
	public EarningsViewHolder onCreateViewHolder(ViewGroup parent, int i) {
		View itemLayoutView = LayoutInflater.from(parent.getContext())
				.inflate(R.layout.opened_trades_list_item, parent, false);

		return new EarningsViewHolder(itemLayoutView);
	}

	@Override
	public void onBindViewHolder(EarningsViewHolder viewHolder, int position) {
		SimpleDateFormat ft = new SimpleDateFormat("yyyy-MM-dd", Locale.UK);

		ArrayList<StockPriceSeriesItem> stockPriceSeries = tradeResults.get(position).series;
		viewHolder.lblEarningsDate.setText(ft.format(tradeResults.get(position).earningsDateItem.date));
		viewHolder.lblTicker.setText(tradeResults.get(position).earningsDateItem.ticker);

		long resultDate = DateUtil.convertToTimestamp(tradeResults.get(position).earningsDateItem.date);
		StockPriceSeriesItem previousClose = StockPriceSeriesUtil.lastBefore(stockPriceSeries, resultDate);
		StockPriceSeriesItem earningsDayStartPrice = StockPriceSeriesUtil.after(stockPriceSeries, resultDate).get(0);
		ArrayList<StockPriceSeriesItem> stockPriceSeriesItemsAfterStart = StockPriceSeriesUtil.after(stockPriceSeries, tradeResults.get(position).openPriceItem.Timestamp);

		viewHolder.lblPreviousClose.setText(String.format(Locale.UK, "%.2f", previousClose.close));
		viewHolder.lblEarningsDayStart.setText(String.format(Locale.UK, "%.2f", earningsDayStartPrice.close));
		viewHolder.lblEarningsDayGap.setText(String.format(Locale.US, "%.2f%%", ((earningsDayStartPrice.close / previousClose.close) - 1) * 100));
		viewHolder.lblOpenPrice.setText(String.format(Locale.UK, "%.2f", stockPriceSeriesItemsAfterStart.get(0).close));
		viewHolder.lblOpenPriceTime.setText(DateUtil.formatLongDateFromTimeStampMillis(stockPriceSeriesItemsAfterStart.get(0).Timestamp * 1000));
		viewHolder.lblCurrentPrice.setText(String.format(Locale.UK, "%.2f", stockPriceSeriesItemsAfterStart.get(stockPriceSeriesItemsAfterStart.size() - 1).close));
		double openPercentageChange = ((stockPriceSeriesItemsAfterStart.get(0).close / previousClose.close) - 1) * 100;
		viewHolder.lblOpenPriceGap.setText(String.format(Locale.US, "%.2f%%", openPercentageChange));
		double currentPriceProfit = ((stockPriceSeriesItemsAfterStart.get(stockPriceSeriesItemsAfterStart.size() - 1).close / stockPriceSeriesItemsAfterStart.get(0).close) - 1) * 100;
		viewHolder.lblCurrentPriceTime.setText(DateUtil.formatLongDateFromTimeStampMillis(stockPriceSeriesItemsAfterStart.get(stockPriceSeriesItemsAfterStart.size() - 1).Timestamp * 1000));
		viewHolder.lblCurrentPriceProfit.setText(String.format(Locale.US, "%.2f%%", currentPriceProfit));
	}

	public long getItemId(int position) {
		return position;
	}

	@Override
	public int getItemCount() {
		return tradeResults.size();
	}

	class EarningsViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
		@Bind(R.id.lblTicker)
		TextView lblTicker;
		@Bind(R.id.lblEarningsDate)
		TextView lblEarningsDate;
		@Bind(R.id.lblPreviousClose)
		TextView lblPreviousClose;
		@Bind(R.id.lblEarningsDayStart)
		TextView lblEarningsDayStart;
		@Bind(R.id.lblEarningsDayGap)
		TextView lblEarningsDayGap;
		@Bind(R.id.lblOpenPrice)
		TextView lblOpenPrice;
		@Bind(R.id.lblOpenPriceTime)
		TextView lblOpenPriceTime;
		@Bind(R.id.lblOpenPriceGap)
		TextView lblOpenPriceGap;
		@Bind(R.id.lblCurrentPrice)
		TextView lblCurrentPrice;
		@Bind(R.id.lblCurrentPriceTime)
		TextView lblCurrentPriceTime;
		@Bind(R.id.lblCurrentProfit)
		TextView lblCurrentPriceProfit;

		EarningsViewHolder(View itemView) {
			super(itemView);
			ButterKnife.bind(this, itemView);
			itemView.setOnClickListener(this);
		}

		@Override
		public void onClick(View v) {
			mItemListener.openedTradesListClicked(v, this.getLayoutPosition(), tradeResults.get(getLayoutPosition()).earningsDateItem.ticker);
		}
	}

	interface RecyclerViewClickListener {
		void openedTradesListClicked(View v, int position, String ticker);
	}
}
