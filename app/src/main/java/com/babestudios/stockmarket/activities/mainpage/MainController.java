package com.babestudios.stockmarket.activities.mainpage;

import android.os.AsyncTask;

import com.babestudios.stockmarket.StockMarketResearchApplication;
import com.babestudios.stockmarket.model.dto.pojos.EarningsDateItem;
import com.babestudios.stockmarket.network.interfaces.IGetEarningsDatesService;
import com.babestudios.stockmarket.utils.rxjava.RetryWithDelay;

import java.util.ArrayList;

import javax.inject.Inject;

import rx.Observer;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class MainController {

	@Inject
	IGetEarningsDatesService earningsDatesService;

	void downloadEarningsDates(Observer<EarningsDateItem[]> observer) {
		((StockMarketResearchApplication) StockMarketResearchApplication.getContext()).getMainPageComponent().inject(this);

		earningsDatesService.getEarningsDates()
				.retryWhen(new RetryWithDelay(10, 6000))
				.subscribeOn(Schedulers.from(AsyncTask.THREAD_POOL_EXECUTOR))
				.observeOn(AndroidSchedulers.mainThread())
				.subscribe(observer);
	}

	boolean setAlarm(ArrayList<EarningsDateItem> earningsDateItems) {
		return EarningsStartAlarmUtil.setAlarm(StockMarketResearchApplication.getContext(), earningsDateItems, false);
	}

	boolean isAlarmSet() {
		return EarningsStartAlarmUtil.isAlarmSet(StockMarketResearchApplication.getContext());
	}

	void stopAlarm() {
		EarningsStartAlarmUtil.stopAlarm(StockMarketResearchApplication.getContext());
	}
}