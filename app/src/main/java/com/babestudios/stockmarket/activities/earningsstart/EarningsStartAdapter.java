package com.babestudios.stockmarket.activities.earningsstart;

import android.content.Context;
import android.support.v4.util.Pair;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.babestudios.stockmarket.R;
import com.babestudios.stockmarket.model.base.StockPriceSeriesItem;
import com.babestudios.stockmarket.model.dto.YahooIntraDayStockResponse;
import com.babestudios.stockmarket.model.dto.pojos.EarningsDateItem;
import com.babestudios.stockmarket.strategies.model.TradeResultObject;
import com.babestudios.stockmarket.utils.DateUtil;
import com.babestudios.stockmarket.utils.StockPriceSeriesUtil;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;

import butterknife.Bind;
import butterknife.ButterKnife;

public class EarningsStartAdapter extends RecyclerView.Adapter {

	RecyclerViewClickListener mItemListener;

	ArrayList<EarningsDateItem> earningsDateItems = new ArrayList<>();
	ArrayList<TradeResultObject> tradeResults = new ArrayList<>();
	ArrayList<YahooIntraDayStockResponse> intraDayStockResponses = new ArrayList<>();

	public EarningsStartAdapter(Context c, ArrayList<TradeResultObject> canOpenTradeResults, List<Pair<EarningsDateItem, YahooIntraDayStockResponse>> earningsItemPairs) {
		mItemListener = (RecyclerViewClickListener) c;
		updateData(canOpenTradeResults, earningsItemPairs);
	}

	@Override
	public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int i) {
		View itemLayoutView = LayoutInflater.from(parent.getContext())
				.inflate(R.layout.earnings_start_list_item, parent, false);

		return new EarningsViewHolder(itemLayoutView);
	}

	@Override
	public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int position) {
		EarningsViewHolder vh = (EarningsViewHolder) viewHolder;
		SimpleDateFormat ft = new SimpleDateFormat("yyyy-MM-dd", Locale.UK);

		ArrayList<StockPriceSeriesItem> stockPriceSeries = new ArrayList<>(Arrays.asList(intraDayStockResponses.get(position).series));
		vh.lblEarningsDate.setText(ft.format(earningsDateItems.get(position).date));
		vh.lblTicker.setText(earningsDateItems.get(position).ticker);
		long resultDate = DateUtil.convertToTimestamp(earningsDateItems.get(position).date);
		StockPriceSeriesItem previousClose = StockPriceSeriesUtil.lastBefore(stockPriceSeries, resultDate);
		ArrayList<StockPriceSeriesItem> stockPriceSeriesItemsAfterStart = StockPriceSeriesUtil.after(stockPriceSeries, resultDate);

		vh.lblPreviousClose.setText(String.format(Locale.UK, "%f",previousClose.close));
		vh.lblOpen.setText(String.format(Locale.UK, "%f",stockPriceSeriesItemsAfterStart.get(0).close));
		vh.lblCurrentPrice.setText(String.format(Locale.UK, "%f",stockPriceSeriesItemsAfterStart.get(stockPriceSeriesItemsAfterStart.size() - 1).close));
		double openPercentageChange = ((stockPriceSeriesItemsAfterStart.get(0).close / previousClose.close) - 1) * 100;
		vh.lblOpenPercentageChange.setText(String.format(Locale.UK, "%.2f%%", openPercentageChange));
		double currentPercentageChange = ((stockPriceSeriesItemsAfterStart.get(stockPriceSeriesItemsAfterStart.size() - 1).close / previousClose.close) - 1) * 100;
		vh.lblCurrentPercentageChange.setText(String.format(Locale.UK, "%.2f%%", currentPercentageChange));
	}

	public long getItemId(int position) {
		return position;
	}

	@Override
	public int getItemCount() {
		return earningsDateItems.size();
	}

	public class EarningsViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
		@Bind(R.id.lblTicker)
		TextView lblTicker;
		@Bind(R.id.lblEarningsDate)
		TextView lblEarningsDate;
		@Bind(R.id.lblPreviousClose)
		TextView lblPreviousClose;
		@Bind(R.id.lblOpen)
		TextView lblOpen;
		@Bind(R.id.lblCurrentPrice)
		TextView lblCurrentPrice;
		@Bind(R.id.lblOpenPercentageChange)
		TextView lblOpenPercentageChange;
		@Bind(R.id.lblCurrentPercentageChange)
		TextView lblCurrentPercentageChange;

		public EarningsViewHolder(View itemView) {
			super(itemView);
			ButterKnife.bind(this, itemView);
			itemView.setOnClickListener(this);
		}

		@Override
		public void onClick(View v) {
			mItemListener.earningDateListClicked(v, this.getLayoutPosition(), earningsDateItems.get(getLayoutPosition()).ticker);
		}
	}

	public void updateData(ArrayList<TradeResultObject> canOpenTradeResults, List<Pair<EarningsDateItem, YahooIntraDayStockResponse>> earningsItemPairs) {
		for(int i = 0; i < canOpenTradeResults.size(); i++){
			earningsDateItems.add(earningsItemPairs.get(i).first);
			intraDayStockResponses.add(earningsItemPairs.get(i).second);
			this.tradeResults.add(canOpenTradeResults.get(i));
		}
	}

	public interface RecyclerViewClickListener {
		void earningDateListClicked(View v, int position, String ticker);
	}

	public ArrayList<EarningsDateItem> getEarningsDateItems() {
		return earningsDateItems;
	}

}
