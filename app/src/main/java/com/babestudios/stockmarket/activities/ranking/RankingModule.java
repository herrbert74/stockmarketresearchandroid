package com.babestudios.stockmarket.activities.ranking;

import com.babestudios.stockmarket.BuildConfig;
import com.babestudios.stockmarket.network.converters.ToStringConverterFactory;
import com.babestudios.stockmarket.network.converters.AdvancedGsonConverterFactory;
import com.babestudios.stockmarket.network.interfaces.IGetIndexChangesService;
import com.babestudios.stockmarket.network.interfaces.IGetTickersService;
import com.babestudios.stockmarket.network.interfaces.IYahooStatisticsService;

import javax.inject.Named;
import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;

@Module
public class RankingModule {

	@Provides
	@Singleton
	@Named("GetTickersRetrofit")
	Retrofit provideGetTickersRetrofit() {
		return new Retrofit.Builder()//
				.baseUrl(BuildConfig.STOCK_MARKET_RESEARCH_BASE_URL)//
				.addCallAdapterFactory(RxJavaCallAdapterFactory.create())//
				.addConverterFactory(AdvancedGsonConverterFactory.create())//
				.build();
	}

	@Provides
	@Singleton
	IGetTickersService provideGetTickersService(@Named("GetTickersRetrofit") Retrofit retroFit) {
		return retroFit.create(IGetTickersService.class);
	}

	@Provides
	@Singleton
	@Named("GetIndexChangesRetrofit")
	Retrofit provideGetIndexChangesRetrofit() {
		return new Retrofit.Builder()//
				.baseUrl(BuildConfig.STOCK_MARKET_RESEARCH_BASE_URL)//
				.addCallAdapterFactory(RxJavaCallAdapterFactory.create())//
				.addConverterFactory(AdvancedGsonConverterFactory.create())//
				.build();
	}

	@Provides
	@Singleton
	IGetIndexChangesService provideGetIndexChangesService(@Named("GetIndexChangesRetrofit") Retrofit retroFit) {
		return retroFit.create(IGetIndexChangesService.class);
	}
/*
	@Provides
	@Singleton
	@Named("GoogleDailyRetrofit")
	Retrofit provideGoogleDailyRetrofit() {
		return new Retrofit.Builder()//
				.baseUrl(BuildConfig.GOOGLE_DAILY_BASE_URL)//
				.addCallAdapterFactory(RxJavaCallAdapterFactory.create())
				.addConverterFactory(ToStringConverterFactory.create())//
				.build();
	}

	@Provides
	@Singleton
	IGoogleDailyService provideGoogleDailyService(@Named("GoogleDailyRetrofit") Retrofit retroFit) {
		return retroFit.create(IGoogleDailyService.class);
	}
*/
	@Provides
	@Singleton
	@Named("YahooStatisticsRetrofit")
	Retrofit provideYahooStatisticsRetrofit() {
		return new Retrofit.Builder()//
				.baseUrl(BuildConfig.YAHOO_STATISTICS_BASE_URL)//
				.addCallAdapterFactory(RxJavaCallAdapterFactory.create())//
				.addConverterFactory(ToStringConverterFactory.create())//
				.build();
	}

	@Provides
	@Singleton
	IYahooStatisticsService provideYahooStatisticsService(@Named("YahooStatisticsRetrofit") Retrofit retroFit) {
		return retroFit.create(IYahooStatisticsService.class);
	}
}
