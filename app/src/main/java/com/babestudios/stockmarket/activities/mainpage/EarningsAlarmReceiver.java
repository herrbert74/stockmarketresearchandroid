package com.babestudios.stockmarket.activities.mainpage;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.media.RingtoneManager;
import android.net.Uri;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.TaskStackBuilder;
import android.util.Log;

import com.babestudios.stockmarket.R;
import com.babestudios.stockmarket.StockMarketResearchApplication;
import com.babestudios.stockmarket.activities.earningsclose.EarningsCloseService;
import com.babestudios.stockmarket.activities.earningsstart.EarningsStartActivity;
import com.babestudios.stockmarket.model.dto.pojos.EarningsDateItem;
import com.babestudios.stockmarket.utils.DateUtil;
import com.sasnee.scribo.DebugHelper;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;

import rx.Observer;

public class EarningsAlarmReceiver extends BroadcastReceiver implements Observer<EarningsDateItem[]> {

	private MainController mainController;

	private Context context;

	@Override
	public void onReceive(Context context, Intent intent) {
		this.context = context;
		sendNotification(context, "New earnings results announced");
	}

	private void sendNotification(Context context, String message) {
		Intent intent = new Intent(context, EarningsStartActivity.class);

		TaskStackBuilder stackBuilder = TaskStackBuilder.create(context);
		// Adds the back stack
		stackBuilder.addParentStack(EarningsStartActivity.class);
		// Adds the Intent to the top of the stack
		stackBuilder.addNextIntent(intent);

		PendingIntent pendingIntent = stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);

		intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
		//PendingIntent pendingIntent = PendingIntent.getActivity(context, (int) System.currentTimeMillis(), intent, PendingIntent.FLAG_ONE_SHOT);

		Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
		final String packageName = context.getPackageName();
		NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(context)
				.setSmallIcon(R.drawable.ic_event_available_black_24px)
				.setLargeIcon(BitmapFactory.decodeResource(null, R.drawable.ic_event_available_black_24px))
				.setContentTitle(context.getResources().getString(R.string.app_name))
				.setContentText(message)
				.setStyle(new NotificationCompat.BigTextStyle().bigText(message))
				.setAutoCancel(true)
				//.setSound(Uri.parse("android.resource://" + packageName + "/" + R.raw.cash_register_purchase))
				.setContentIntent(pendingIntent);
		NotificationManager notificationManager =
				(NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
		notificationManager.notify(EarningsStartAlarmUtil.EARNINGS_NOTIFICATION, notificationBuilder.build());

		//Download earnings dates and restart alarm if needed in the callback

		mainController = new MainController();
		mainController.downloadEarningsDates(this);
	}

	@Override
	public void onCompleted() {

	}

	@Override
	public void onError(Throwable e) {

	}

	@Override
	public void onNext(EarningsDateItem[] earningsDateItems) {
		//Restart alarm if needed
		ArrayList<EarningsDateItem> earningsDateItemsList = new ArrayList<>(Arrays.asList(earningsDateItems));
		//This is just for testing when the date is in the past, it will restart endlessly
		if(earningsDateItemsList.size() != 0 && EarningsStartAlarmUtil.getNextDateInMillis(earningsDateItemsList) > Calendar.getInstance().getTimeInMillis()) {
			DebugHelper.logRequest("test", "receiver onNext: restarting alarm");
			EarningsStartAlarmUtil.setAlarm(StockMarketResearchApplication.getContext(), earningsDateItemsList, false);
		} else {
			DebugHelper.logRequest("test", "receiver onNext: not restarting alarm");
		}
	}
}
