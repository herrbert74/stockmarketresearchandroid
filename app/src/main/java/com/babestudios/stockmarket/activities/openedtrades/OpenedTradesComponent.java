package com.babestudios.stockmarket.activities.openedtrades;

import com.babestudios.stockmarket.ApplicationComponent;
import com.babestudios.stockmarket.activities.earningsclose.EarningsClosePageScope;
import com.babestudios.stockmarket.activities.earningsclose.EarningsCloseService;

import dagger.Component;

@OpenedTradesPageScope
@Component(dependencies = ApplicationComponent.class)
public interface OpenedTradesComponent {
	void inject(OpenedTradesController openedTradesController);
	void inject(OpenedTradesAdapter openedTradesAdapter);
}
