package com.babestudios.stockmarket.activities.earningsclose;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.babestudios.stockmarket.StockMarketResearchApplication;
import com.babestudios.stockmarket.utils.DateUtil;
import com.babestudios.stockmarket.utils.SharedPreferencesHelper;

import java.util.Calendar;

import javax.inject.Inject;


public class EarningsCloseAlarmReceiver extends BroadcastReceiver {

	@Inject
	SharedPreferencesHelper sharedPreferencesHelper;

	@Override
	public void onReceive(Context context, Intent intent) {
		((StockMarketResearchApplication) StockMarketResearchApplication.getContext()).getEarningsCloseComponent().inject(this);
		EarningsCloseAlarmUtil.setAlarm(context);
		startService(context);
	}

	private void startService(Context context) {
		if (DateUtil.isUkBusinessDay(Calendar.getInstance()) && sharedPreferencesHelper.isAnyPositionOpen()) {
			context.startService(new Intent(context, EarningsCloseService.class));
		}
	}
}
