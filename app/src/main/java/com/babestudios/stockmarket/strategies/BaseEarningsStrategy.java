package com.babestudios.stockmarket.strategies;

import com.babestudios.stockmarket.strategies.model.TradeResultObject;
import com.babestudios.stockmarket.strategies.rules.IRule;
import com.babestudios.stockmarket.strategies.rules.RuleParameters;

import java.util.ArrayList;
import java.util.List;

public abstract class BaseEarningsStrategy implements IEarningsStrategy {
	public List<IRule<RuleParameters>> openRules = new ArrayList<>();

	public abstract TradeResultObject canOpenTrade();

	public abstract TradeResultObject canCloseTrade();

	@Override
	public TradeResultObject calculateResult() {
		return null;
	}

	@Override
	public long validateAllRules(List<IRule<RuleParameters>> rules) {
		if(rules.size() == 0){
			return 0;
		}
		for (IRule<RuleParameters> rule : rules) {
			if (!rule.validate()) {
				return 0;
			}
		}
		return 1;
	}
}
