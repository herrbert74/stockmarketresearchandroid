package com.babestudios.stockmarket.strategies.rules;

public class BooleanRule implements IRule<RuleParameters> {
	public boolean first;

	public BooleanRule(boolean first) {
		this.first = first;
	}

	@Override
	public boolean validate() {
		return first;
	}
}