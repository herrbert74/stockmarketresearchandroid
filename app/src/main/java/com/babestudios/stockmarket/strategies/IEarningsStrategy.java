package com.babestudios.stockmarket.strategies;

import com.babestudios.stockmarket.strategies.model.TradeResultObject;
import com.babestudios.stockmarket.strategies.rules.IRule;
import com.babestudios.stockmarket.strategies.rules.RuleParameters;

import java.util.List;

public interface IEarningsStrategy {
	TradeResultObject canOpenTrade();
	TradeResultObject canCloseTrade();
	TradeResultObject calculateResult();

	long validateAllRules(List<IRule<RuleParameters>> rules);
}
