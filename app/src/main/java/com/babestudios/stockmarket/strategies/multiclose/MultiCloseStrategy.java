package com.babestudios.stockmarket.strategies.multiclose;

import com.babestudios.stockmarket.activities.mainpage.EarningsStartAlarmUtil;
import com.babestudios.stockmarket.model.base.StockPriceSeriesItem;
import com.babestudios.stockmarket.strategies.BaseEarningsStrategy;
import com.babestudios.stockmarket.strategies.model.TradeResultObject;
import com.babestudios.stockmarket.strategies.rules.LargerThanRule;
import com.babestudios.stockmarket.utils.DateUtil;
import com.babestudios.stockmarket.utils.StockPriceSeriesUtil;

import java.util.ArrayList;
import java.util.List;

public class MultiCloseStrategy extends BaseEarningsStrategy {

	private StockPriceSeriesItem previousClose;
	private ArrayList<StockPriceSeriesItem> stockPriceSeriesItemsAfterStart;
	private TradeResultObject tradeResult = new TradeResultObject(MultiCloseStrategyParameters.STOP_PERCENTAGE);

	public MultiCloseStrategy(TradeResultObject tradeResult) {
		this.tradeResult = tradeResult;

		long resultDate = DateUtil.convertToTimestamp(tradeResult.earningsDateItem.date);
		previousClose = StockPriceSeriesUtil.lastBefore(tradeResult.series, resultDate);
		stockPriceSeriesItemsAfterStart = StockPriceSeriesUtil.after(tradeResult.series, resultDate);
		//There is a gap
		if (stockPriceSeriesItemsAfterStart.size() > 0) {
			openRules.add(new LargerThanRule(stockPriceSeriesItemsAfterStart.get(0).close, previousClose.close * (1 + (MultiCloseStrategyParameters.START_PERCENTAGE / 100))));
			//at the last item there is still a gap
			openRules.add(new LargerThanRule(stockPriceSeriesItemsAfterStart.get(stockPriceSeriesItemsAfterStart.size() - 1).close, previousClose.close * (1 + (MultiCloseStrategyParameters.OPEN_PERCENTAGE / 100))));
		}
	}

	@Override
	public TradeResultObject canOpenTrade() {
		if (previousClose == null) {
			return tradeResult;
		}
		tradeResult.isLongTrade = true;

		if (validateAllRules(openRules) > 0) {
			long openTimestamp = DateUtil.convertToTimestamp(tradeResult.earningsDateItem.date) + EarningsStartAlarmUtil.ALARM_TRIGGER_HOUR * 60 * 60;
			ArrayList<StockPriceSeriesItem> stockPriceSeriesItemsAfterOpen = StockPriceSeriesUtil.after(tradeResult.series, openTimestamp);
			tradeResult.openPriceItem = stockPriceSeriesItemsAfterOpen.get(0);
		} else {
			tradeResult.openPriceItem = null;
		}

		return tradeResult;
	}

	@Override
	public TradeResultObject canCloseTrade() {
		StockPriceSeriesItem latest = tradeResult.series.get(tradeResult.series.size() - 1);
		if (latest.close < tradeResult.openPriceItem.close * (100 + tradeResult.stopPercentage) / 100 //Stop hit
				|| tradeResult.openPriceItem.Timestamp + MultiCloseStrategyParameters.MAX_HOLDING_PERIOD < latest.Timestamp) {//Max holding period hit {
			tradeResult.closePriceItems.add(latest);
			tradeResult.isClosed = true;
		}
		//Limit hit
		else if (tradeResult.closePriceItems.size() < MultiCloseStrategyParameters.LIMIT_PERCENTAGES.length) {
			if (latest.close > tradeResult.openPriceItem.close * (100 + MultiCloseStrategyParameters.LIMIT_PERCENTAGES[tradeResult.closePriceItems.size()]) / 100) {
				tradeResult.stopPercentage = MultiCloseStrategyParameters.LIMIT_PERCENTAGES[tradeResult.closePriceItems.size()] + MultiCloseStrategyParameters.TRAILING_STOP_PERCENTAGE;
				tradeResult.closePriceItems.add(latest);
				if(tradeResult.closePriceItems.size() == MultiCloseStrategyParameters.LIMIT_PERCENTAGES.length) {
					tradeResult.isClosed = true;
				}
			}
		}
		return tradeResult;
	}
}
