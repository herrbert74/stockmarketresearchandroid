package com.babestudios.stockmarket.strategies.multiclose;

import com.babestudios.stockmarket.strategies.IStrategyParameters;

public class MultiCloseStrategyParameters implements IStrategyParameters {
	public static final int OPEN_INTERVAL = 48; //not used here
	public static final double[] LIMIT_PERCENTAGES = { 3, 6, 9, 12 };
	public static final double START_PERCENTAGE = 1;
	public static final double OPEN_PERCENTAGE = 1;
	public static final double STOP_PERCENTAGE = -2;
	public static final double TRAILING_STOP_PERCENTAGE = -3;
	public static final long MAX_HOLDING_PERIOD = 10 * 24 * 60 * 60;
}
