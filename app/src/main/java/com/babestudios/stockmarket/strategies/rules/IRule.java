package com.babestudios.stockmarket.strategies.rules;

public interface IRule<T> {
	boolean validate();
}
