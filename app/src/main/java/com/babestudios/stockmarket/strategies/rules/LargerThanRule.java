package com.babestudios.stockmarket.strategies.rules;

public class LargerThanRule implements IRule<RuleParameters> {

	public double first, second;

	public LargerThanRule(double first, double second) {
		this.first = first;
		this.second = second;
	}

	@Override
	public boolean validate() {
		return first > second;
	}
}
