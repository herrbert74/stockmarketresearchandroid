package com.babestudios.stockmarket.strategies.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.babestudios.stockmarket.model.base.StockPriceSeriesItem;
import com.babestudios.stockmarket.model.dto.pojos.EarningsDateItem;
import com.babestudios.stockmarket.strategies.multiclose.MultiCloseStrategyParameters;

import java.util.ArrayList;
import java.util.List;

public class TradeResultObject implements Parcelable {

	public TradeResultObject(double stopPercentage) {
		this.stopPercentage = stopPercentage;
	}

	/**
	 * In the desktop app this is in the close part of the strategy. Moved it here because it has to be persisted between calls to closeStrategy.
	 */
	public double stopPercentage;

	//Earnings Date
	public EarningsDateItem earningsDateItem;
	//Open
	public StockPriceSeriesItem openPriceItem;
	public boolean isLongTrade;
	//Close
	public List<StockPriceSeriesItem> closePriceItems = new ArrayList<>();
	//series
	public ArrayList<StockPriceSeriesItem> series;

	public boolean isOpened() {
		return openPriceItem != null;
	}

	public boolean isClosed = false;


	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeDouble(this.stopPercentage);
		dest.writeParcelable(this.earningsDateItem, flags);
		dest.writeParcelable(this.openPriceItem, flags);
		dest.writeByte(this.isLongTrade ? (byte) 1 : (byte) 0);
		dest.writeList(this.closePriceItems);
		dest.writeList(this.series);
		dest.writeByte(this.isClosed ? (byte) 1 : (byte) 0);
	}

	protected TradeResultObject(Parcel in) {
		this.stopPercentage = in.readDouble();
		this.earningsDateItem = in.readParcelable(EarningsDateItem.class.getClassLoader());
		this.openPriceItem = in.readParcelable(StockPriceSeriesItem.class.getClassLoader());
		this.isLongTrade = in.readByte() != 0;
		this.closePriceItems = new ArrayList<StockPriceSeriesItem>();
		in.readList(this.closePriceItems, StockPriceSeriesItem.class.getClassLoader());
		this.series = new ArrayList<StockPriceSeriesItem>();
		in.readList(this.series, StockPriceSeriesItem.class.getClassLoader());
		this.isClosed = in.readByte() != 0;
	}

	public static final Creator<TradeResultObject> CREATOR = new Creator<TradeResultObject>() {
		@Override
		public TradeResultObject createFromParcel(Parcel source) {
			return new TradeResultObject(source);
		}

		@Override
		public TradeResultObject[] newArray(int size) {
			return new TradeResultObject[size];
		}
	};

	@Override
	public boolean equals(Object object)
	{
		boolean sameSame = false;

		if (object != null && object instanceof TradeResultObject)
		{
			sameSame = this.earningsDateItem.equals(((TradeResultObject) object).earningsDateItem);
		}

		return sameSame;
	}
}
