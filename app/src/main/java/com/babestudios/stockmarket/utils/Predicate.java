package com.babestudios.stockmarket.utils;

import java.util.ArrayList;
import java.util.Collection;

public class Predicate {
	public static Object predicateParams;

	public static <E> Collection<E> filter(Collection<E> target, IPredicate<E> predicate) {
		Collection<E> result = new ArrayList<E>();
		for (E element : target) {
			if (predicate.apply(element)) {
				result.add(element);
			}
		}
		return result;
	}

	public static <T> T select(Collection<T> target, IPredicate<T> predicate) {
		T result = null;
		for (T element : target) {
			if (!predicate.apply(element))
				continue;
			result = element;
			break;
		}
		return result;
	}

	public static <T> T select(Collection<T> target, IPredicate<T> predicate, T defaultValue) {
		T result = defaultValue;
		for (T element : target) {
			if (!predicate.apply(element))
				continue;
			result = element;
			break;
		}
		return result;
	}
}
