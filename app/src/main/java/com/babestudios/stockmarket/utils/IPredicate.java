package com.babestudios.stockmarket.utils;

public interface IPredicate<T> { boolean apply(T type); }