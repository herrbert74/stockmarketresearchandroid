package com.babestudios.stockmarket.utils;

import com.babestudios.stockmarket.model.base.StockPriceSeriesItem;

import java.util.ArrayList;
import java.util.Iterator;

public class StockPriceSeriesUtil {

	public static ArrayList<StockPriceSeriesItem> before(ArrayList<StockPriceSeriesItem> series, long timeStamp) {
		ArrayList<StockPriceSeriesItem> newSeries = new ArrayList<>(series);
		Iterator<StockPriceSeriesItem> it = newSeries.iterator();
		while ( it.hasNext() ) {
			if (it.next().Timestamp > timeStamp)
				it.remove();
		}
		return newSeries;
	}

	public static StockPriceSeriesItem lastBefore(ArrayList<StockPriceSeriesItem> series, long timeStamp) {
		ArrayList<StockPriceSeriesItem> newSeries = before(series, timeStamp);
		return newSeries.get(newSeries.size() - 1);
	}

	public static ArrayList<StockPriceSeriesItem> after(ArrayList<StockPriceSeriesItem> series, long timeStamp) {
		ArrayList<StockPriceSeriesItem> newSeries = new ArrayList<>(series);
		Iterator<StockPriceSeriesItem> it = newSeries.iterator();
		while ( it.hasNext() ) {
			if (it.next().Timestamp < timeStamp)
				it.remove();
		}
		return newSeries;
	}

	public static ArrayList<StockPriceSeriesItem> between(ArrayList<StockPriceSeriesItem> series, long timeStampAfter, long timeStampBefore ) {
		ArrayList<StockPriceSeriesItem> newSeries = after(series, timeStampAfter);
		return before(newSeries, timeStampBefore);
	}

	public static ArrayList<StockPriceSeriesItem> addAllNonDuplicates(ArrayList<StockPriceSeriesItem> series, ArrayList<StockPriceSeriesItem> addition){
		long latest = series.get(series.size()-1).Timestamp;
		ArrayList<StockPriceSeriesItem> newSeries = new ArrayList<>(series);
		newSeries.addAll(after(addition, latest));
		return newSeries;
	}

}
