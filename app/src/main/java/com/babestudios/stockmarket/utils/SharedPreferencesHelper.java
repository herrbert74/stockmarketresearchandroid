package com.babestudios.stockmarket.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.util.Log;

import com.babestudios.stockmarket.strategies.model.TradeResultObject;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.sasnee.scribo.DebugHelper;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Objects;

public class SharedPreferencesHelper {

	private Context context;
	private static final String OPENED_TRADES = "opened_trades";
	private static final String CLOSED_TRADES = "closed_trades";
	private SharedPreferences settings;
	private SharedPreferences.Editor edit;
	private Gson gson = new Gson();

	public SharedPreferencesHelper(Context context) {
		this.context = context;
		settings = PreferenceManager.getDefaultSharedPreferences(context);
		edit = settings.edit();

	}

	//TODO Write test for "earning.contains..." (same and different ticker and date)
	public void addOpenedTrades(ArrayList<TradeResultObject> tradeResults) {
		ArrayList<TradeResultObject> openedTrades = loadOpenedTrades();
		if (openedTrades == null) {
			openedTrades = new ArrayList<>();
		}
		for (int i = 0; i < tradeResults.size(); i++) {
			if (tradeResults.get(i).openPriceItem != null) {
				if (!openedTrades.contains(tradeResults.get(i))) {
					openedTrades.add(tradeResults.get(i));
				}
			}
		}
		String jsonOpenedTrades = gson.toJson(openedTrades);
		Log.d("test", "addOpenedTrades: " + jsonOpenedTrades);
		edit.putString(OPENED_TRADES, jsonOpenedTrades);
		edit.apply();
	}

	public void saveTrades(ArrayList<TradeResultObject> updatedTradeResults) {
		//Add new trades that might have added to opened trades while the update to the updated trades
		ArrayList<TradeResultObject> openedTrades = loadOpenedTrades();
		if (openedTrades != null) {
			for (int i = 0; i < openedTrades.size(); i++) {
				if (!updatedTradeResults.contains(openedTrades.get(i))) {
					updatedTradeResults.add(openedTrades.get(i));
				}
			}
		}
		//Split updated trades to opened and closed trades
		ArrayList<TradeResultObject> openedTradeResults = new ArrayList<>();
		ArrayList<TradeResultObject> closedTradeResults = loadClosedTrades();
		for (int i = 0; i < updatedTradeResults.size(); i++) {
			if (updatedTradeResults.get(i).isClosed) {
				closedTradeResults.add(updatedTradeResults.get(i));
			} else {
				openedTradeResults.add(updatedTradeResults.get(i));
			}
		}
		String jsonOpenedTrades = gson.toJson(openedTradeResults);
		edit.putString(OPENED_TRADES, jsonOpenedTrades);
		edit.apply();
		String jsonClosedTrades = gson.toJson(closedTradeResults);
		edit.putString(CLOSED_TRADES, jsonClosedTrades);
		edit.apply();
	}

	public boolean isAnyPositionOpen() {
		return loadOpenedTrades().size() > 0;
	}

	public ArrayList<TradeResultObject> loadOpenedTrades() {
		String openedTradesString = settings.getString(OPENED_TRADES, "");
		ArrayList<TradeResultObject> tradeResultObjects = gson.fromJson(openedTradesString, new TypeToken<List<TradeResultObject>>() {
		}.getType());
		return tradeResultObjects != null ? tradeResultObjects : new ArrayList<>();
	}

	private ArrayList<TradeResultObject> loadClosedTrades() {
		String closedTradesString = settings.getString(CLOSED_TRADES, "");
		ArrayList<TradeResultObject> tradeResultObjects = gson.fromJson(closedTradesString, new TypeToken<List<TradeResultObject>>() {
		}.getType());
		return tradeResultObjects != null ? tradeResultObjects : new ArrayList<>();
	}

	public ArrayList<TradeResultObject> removeOpenedTrade(TradeResultObject tradeResultObject) {
		ArrayList<TradeResultObject> tradeResults = loadOpenedTrades();
		Iterator<TradeResultObject> it = tradeResults.iterator();
		while (it.hasNext()) {
			if (Objects.equals(it.next().earningsDateItem.ticker, tradeResultObject.earningsDateItem.ticker))
				it.remove();
		}
		saveTrades(tradeResults);
		return tradeResults;
	}
}
