package com.babestudios.stockmarket;

import android.content.Context;

import com.babestudios.stockmarket.network.converters.AdvancedGsonConverterFactory;
import com.babestudios.stockmarket.network.interfaces.IGetEarningsDatesService;
import com.babestudios.stockmarket.network.interfaces.IYahooIntraDayService;
import com.babestudios.stockmarket.utils.SharedPreferencesHelper;

import javax.inject.Named;
import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;

@Module
class ApplicationModule {

	private final Context context;

	ApplicationModule(Context context) {
		this.context = context;
	}

	@Provides
	public Context context() {
		return context;
	}

	@Provides
	@Singleton
	@Named("YahooIntraDayRetrofit")
	Retrofit provideYahooIntraDayRetrofit() {
		return new Retrofit.Builder()//
				.baseUrl(BuildConfig.YAHOO_INTRA_DAY_BASE_URL)//
				.addCallAdapterFactory(RxJavaCallAdapterFactory.create())//
				.addConverterFactory(AdvancedGsonConverterFactory.create())//
				.build();
	}

	@Provides
	@Singleton
	IYahooIntraDayService provideYahooIntraDayService(@Named("YahooIntraDayRetrofit") Retrofit retroFit) {
		return retroFit.create(IYahooIntraDayService.class);
	}

	@Provides
	@Singleton
	@Named("EarningsDatesRetrofit")
	Retrofit provideEarningsDatesRetrofit() {
		return new Retrofit.Builder()//
				.baseUrl(BuildConfig.STOCK_MARKET_RESEARCH_BASE_URL)//
				.addCallAdapterFactory(RxJavaCallAdapterFactory.create())//
				.addConverterFactory(AdvancedGsonConverterFactory.create())//
				.build();
	}

	@Provides
	@Singleton
	IGetEarningsDatesService provideEarningsDatesService(@Named("EarningsDatesRetrofit") Retrofit retroFit) {
		return retroFit.create(IGetEarningsDatesService.class);
	}

	@Provides
	@Singleton
	SharedPreferencesHelper provideSharedPreferencesHelper(Context context) {
		return new SharedPreferencesHelper(context);
	}
}
