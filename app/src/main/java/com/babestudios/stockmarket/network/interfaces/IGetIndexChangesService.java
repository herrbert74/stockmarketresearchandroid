package com.babestudios.stockmarket.network.interfaces;

import com.babestudios.stockmarket.BuildConfig;
import com.babestudios.stockmarket.model.dto.pojos.IndexChangeItem;

import retrofit2.http.GET;
import rx.Observable;

public interface IGetIndexChangesService {
	@GET(BuildConfig.STOCK_MARKET_RESEARCH_GET_INDEX_CHANGES_ENDPOINT)
	Observable<IndexChangeItem[]> getIndexChanges();
}
