package com.babestudios.stockmarket.network.interfaces;

import com.babestudios.stockmarket.BuildConfig;

import retrofit2.http.GET;
import retrofit2.http.Query;
import rx.Observable;

public interface IYahooStatisticsService {
	@GET(BuildConfig.YAHOO_STATISTICS_ENDPOINT)
	Observable<String> getYahooStatistics(@Query("s") String tickers,
										  @Query("f") String tags,
										  @Query("e") String format);
}
