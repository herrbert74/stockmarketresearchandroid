package com.babestudios.stockmarket.network.interfaces;

import com.babestudios.stockmarket.BuildConfig;
import com.babestudios.stockmarket.model.dto.pojos.TickerItem;

import java.util.List;

import retrofit2.http.GET;
import rx.Observable;

public interface IGetTickersService {
	@GET(BuildConfig.STOCK_MARKET_RESEARCH_GET_TICKERS_ENDPOINT)
	Observable<TickerItem[]> getTickers();
}
