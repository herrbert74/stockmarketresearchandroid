package com.babestudios.stockmarket.network.interfaces;

import com.babestudios.stockmarket.BuildConfig;
import com.babestudios.stockmarket.model.dto.pojos.EarningsDateItem;

import retrofit2.http.GET;
import rx.Observable;

public interface IGetEarningsDatesService {
	@GET(BuildConfig.STOCK_MARKET_RESEARCH_GET_EARNINGS_DATES_ENDPOINT)
	Observable<EarningsDateItem[]> getEarningsDates();
}
