package com.babestudios.stockmarket.network.interfaces;

import com.babestudios.stockmarket.BuildConfig;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;
import rx.Observable;

public interface IGoogleDailyService {
	@GET(BuildConfig.GOOGLE_DAILY_ENDPOINT)
	Observable<String> getGoogleDailyStockPrices(@Query("q") String ticker, @Query("output") String fileType);
}
