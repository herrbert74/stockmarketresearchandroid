package com.babestudios.stockmarket;

import android.content.Context;

import com.babestudios.stockmarket.network.interfaces.IGetEarningsDatesService;
import com.babestudios.stockmarket.network.interfaces.IYahooIntraDayService;
import com.babestudios.stockmarket.utils.SharedPreferencesHelper;

import javax.inject.Named;
import javax.inject.Singleton;

import dagger.Component;
import retrofit2.Retrofit;

@Singleton
@Component(modules = {ApplicationModule.class})
public interface ApplicationComponent {
	Context context();
	void inject(StockMarketResearchApplication application);

	@Named("YahooIntraDayRetrofit")
	Retrofit getYahooIntraDayRetrofit();

	IYahooIntraDayService getYahooIntraDayService();

	@Named("EarningsDatesRetrofit")
	Retrofit getEarningsDatesRetrofit();

	IGetEarningsDatesService getEarningsDatesService();

	SharedPreferencesHelper getSharedPreferencesHelper();
}
