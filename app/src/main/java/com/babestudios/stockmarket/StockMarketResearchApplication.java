package com.babestudios.stockmarket;

import android.app.Application;
import android.content.Context;

import com.babestudios.stockmarket.activities.chartpage.ChartComponent;
import com.babestudios.stockmarket.activities.chartpage.DaggerChartComponent;
import com.babestudios.stockmarket.activities.earningsclose.DaggerEarningsCloseComponent;
import com.babestudios.stockmarket.activities.earningsclose.EarningsCloseComponent;
import com.babestudios.stockmarket.activities.earningsstart.DaggerEarningsStartComponent;
import com.babestudios.stockmarket.activities.earningsstart.EarningsStartComponent;
import com.babestudios.stockmarket.activities.mainpage.DaggerMainPageComponent;
import com.babestudios.stockmarket.activities.mainpage.MainPageComponent;
import com.babestudios.stockmarket.activities.openedtrades.DaggerOpenedTradesComponent;
import com.babestudios.stockmarket.activities.openedtrades.OpenedTradesComponent;
import com.babestudios.stockmarket.activities.ranking.DaggerRankingComponent;
import com.babestudios.stockmarket.activities.ranking.RankingComponent;
import com.babestudios.stockmarket.activities.ranking.RankingModule;

public class StockMarketResearchApplication extends Application {

	private ChartComponent mChartComponent;
	private MainPageComponent mMainPageComponent;
	private EarningsStartComponent mEarningsStartComponent;
	private EarningsCloseComponent mEarningsCloseComponent;
	private OpenedTradesComponent mOpenedTradesComponent;
	private RankingComponent mRankingComponent;

	private static StockMarketResearchApplication instance;

	public StockMarketResearchApplication() {
		instance = this;
	}

	@Override
	public void onCreate() {
		super.onCreate();
		ApplicationComponent mApplicationComponent = DaggerApplicationComponent.builder()
				.applicationModule(new ApplicationModule(this))
				.build();
		mRankingComponent = DaggerRankingComponent.builder()
				.rankingModule(new RankingModule())
				.build();
		mMainPageComponent = DaggerMainPageComponent.builder()
				.applicationComponent(mApplicationComponent)
				.build();
		mEarningsStartComponent = DaggerEarningsStartComponent.builder()
				.applicationComponent(mApplicationComponent)
				.build();
		mEarningsCloseComponent = DaggerEarningsCloseComponent.builder()
				.applicationComponent(mApplicationComponent)
				.build();
		mOpenedTradesComponent = DaggerOpenedTradesComponent.builder()
				.applicationComponent(mApplicationComponent)
				.build();
		mChartComponent = DaggerChartComponent.builder()
				.applicationComponent(mApplicationComponent)
				.build();
	}

	public MainPageComponent getMainPageComponent() {
		return mMainPageComponent;
	}

	public EarningsStartComponent getEarningsStartComponent() {
		return mEarningsStartComponent;
	}

	public EarningsCloseComponent getEarningsCloseComponent() {
		return mEarningsCloseComponent;
	}

	public OpenedTradesComponent getOpenedTradesComponent() {
		return mOpenedTradesComponent;
	}

	public RankingComponent getRankingComponent() {
		return mRankingComponent;
	}

	public ChartComponent getChartComponent() {
		return mChartComponent;
	}

	public static Context getContext() {
		return instance;
	}
}
