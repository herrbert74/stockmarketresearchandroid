package com.babestudios.stockmarket.model.dto.pojos;

import android.support.annotation.NonNull;

public class RankingTickerItem extends TickerItem implements Comparable<RankingTickerItem>{
	public double price;
	public double marketCap;
	public int index;
	public String indexString;

	public RankingTickerItem() {

	}

	public void setPrice(double price){
		this.price = price;
		long lIssuedShares = issuedShares.equals("N/A") || issuedShares == null ? 0 : Long.parseLong(issuedShares);
		this.marketCap = lIssuedShares * this.price / 100;
	}

	public RankingTickerItem(TickerItem tickerItem) {
		this();
		this.id = tickerItem.id;
		this.ticker = tickerItem.ticker;
		this.name = tickerItem.name;
		this.issuedShares = tickerItem.issuedShares;
	}


	public RankingTickerItem(TickerItem tickerItem, int index) {
		this(tickerItem);
		this.index = index;
		switch (index) {
			case 0:
				indexString = "DELISTED";
				break;
			case 1:
				indexString = "FTSE100";
				break;
			case 2:
				indexString = "FTSE250";
				break;
			case 3:
				indexString = "ALL_SHARE";
				break;
			default:
				indexString = "DELISTED";
				break;
		}
	}

	@Override
	public int compareTo(@NonNull RankingTickerItem rankingTickerItem) {
		if(rankingTickerItem.marketCap > marketCap){
			return 1;
		}else if(rankingTickerItem.marketCap < marketCap){
			return -1;
		}
		return 0;
	}
}
