package com.babestudios.stockmarket.model.dto.pojos;

import android.os.Parcel;
import android.os.Parcelable;

import com.babestudios.stockmarket.utils.DateUtil;
import com.google.gson.annotations.SerializedName;

import java.util.Date;

public class EarningsDateItem implements Parcelable {

	@SerializedName("ResultDate")
	public Date date;
	@SerializedName("Ticker")
	public String ticker;
	@SerializedName("Country")
	public String index;

	public EarningsDateItem() {

	}

	public EarningsDateItem(Date date, String ticker, String index) {
		this.date = date;
		this.ticker = ticker;
		this.index = index;
	}

	public EarningsDateItem(EarningsDateItem earningsDateItem) {
		this.date = earningsDateItem.date;
		this.ticker = earningsDateItem.ticker;
		this.index = earningsDateItem.index;
	}

	public static final Creator<EarningsDateItem> CREATOR = new Creator<EarningsDateItem>() {
		@Override
		public EarningsDateItem createFromParcel(Parcel in) {
			return new EarningsDateItem(in);
		}

		@Override
		public EarningsDateItem[] newArray(int size) {
			return new EarningsDateItem[size];
		}
	};

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeLong(date != null ? date.getTime() : -1);
		dest.writeString(this.ticker);
		dest.writeString(this.index);
	}

	protected EarningsDateItem(Parcel in) {
		long tmpDate = in.readLong();
		this.date = tmpDate == -1 ? null : new Date(tmpDate);
		this.ticker = in.readString();
		this.index = in.readString();
	}

	@Override
	public boolean equals(Object object) {
		boolean sameSame = false;

		if (object != null && object instanceof EarningsDateItem) {
			EarningsDateItem other = (EarningsDateItem) object;
			sameSame = DateUtil.formatShortDateFromTimeStampMillis(this.date.getTime()).equals(DateUtil.formatShortDateFromTimeStampMillis(other.date.getTime())) && this.ticker.equals(other.ticker);
		}

		return sameSame;
	}

}
