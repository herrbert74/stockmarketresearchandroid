package com.babestudios.stockmarket.model;

/**
 * Created by ZBertalan on 11/09/2015.
 */
public class PriceRanges {
	public PriceRange close;
	public PriceRange high;
	public PriceRange low;
	public PriceRange open;
}
