package com.babestudios.stockmarket.model.base;

import android.os.Parcel;
import android.os.Parcelable;

public class StockPriceSeriesItem implements Parcelable {
		public long Timestamp;
		public double close;
		public double high;
		public double low;
		public double open;
		public int volume;

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeLong(this.Timestamp);
		dest.writeDouble(this.close);
		dest.writeDouble(this.high);
		dest.writeDouble(this.low);
		dest.writeDouble(this.open);
		dest.writeInt(this.volume);
	}

	public StockPriceSeriesItem() {
	}

	protected StockPriceSeriesItem(Parcel in) {
		this.Timestamp = in.readLong();
		this.close = in.readDouble();
		this.high = in.readDouble();
		this.low = in.readDouble();
		this.open = in.readDouble();
		this.volume = in.readInt();
	}

	public static final Parcelable.Creator<StockPriceSeriesItem> CREATOR = new Parcelable.Creator<StockPriceSeriesItem>() {
		@Override
		public StockPriceSeriesItem createFromParcel(Parcel source) {
			return new StockPriceSeriesItem(source);
		}

		@Override
		public StockPriceSeriesItem[] newArray(int size) {
			return new StockPriceSeriesItem[size];
		}
	};
}
