package com.babestudios.stockmarket.model.dto.pojos;

import com.google.gson.annotations.SerializedName;

public class TickerItem {
	public int id;
	@SerializedName("Ticker")
	public String ticker;
	@SerializedName("Name")
	public String name;
	@SerializedName("IssuedShares")
	public String issuedShares;
}
