package com.babestudios.stockmarket.model.dto.pojos;

public class IndexChangeItem {
	public long id;
	public int ticker_id;
	public String date;
	public int index_id;
	public boolean addition;
}
