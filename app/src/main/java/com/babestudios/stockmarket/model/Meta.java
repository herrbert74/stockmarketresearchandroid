package com.babestudios.stockmarket.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by wolverine on 10/09/2015.
 */
public class Meta {
	public String uri;
	public String ticker;
	@SerializedName("Company-Name")
	public String companyName;
	@SerializedName("Exchange-name")
	public String exchangeName;
	public String unit;
	public String timezone;
	public String currency;
	public int gmtoffset;
	public double previous_close;
}
