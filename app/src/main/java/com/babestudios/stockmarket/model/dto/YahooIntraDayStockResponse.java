package com.babestudios.stockmarket.model.dto;

import com.babestudios.stockmarket.model.Meta;
import com.babestudios.stockmarket.model.PriceRanges;
import com.babestudios.stockmarket.model.TimeRange;
import com.babestudios.stockmarket.model.TimeStampRange;
import com.babestudios.stockmarket.model.base.StockPriceSeriesItem;
import com.google.gson.annotations.SerializedName;

public class YahooIntraDayStockResponse {
	public Meta meta;
	@SerializedName("TimeStamp-Ranges")
	public TimeStampRange[] timeStampRanges;
	public TimeRange Timestamp;
	public long[] labels;
	public PriceRanges ranges;
	public StockPriceSeriesItem[] series;
}
