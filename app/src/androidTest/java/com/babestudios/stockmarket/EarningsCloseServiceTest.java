package com.babestudios.stockmarket;

import android.content.Intent;
import android.support.test.InstrumentationRegistry;
import android.support.test.rule.ServiceTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.test.suitebuilder.annotation.MediumTest;

import com.babestudios.stockmarket.activities.earningsclose.EarningsCloseService;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.concurrent.TimeoutException;

@RunWith(AndroidJUnit4.class)
@MediumTest
public class EarningsCloseServiceTest {

	@Rule
	public final ServiceTestRule mServiceRule = new ServiceTestRule();

	@Test
	public void testWithStartedService() {
		try {
			mServiceRule.startService(
					new Intent(InstrumentationRegistry.getTargetContext(), EarningsCloseService.class));

		} catch (TimeoutException e) {
			e.printStackTrace();

		}
		// test code
	}


}
