package com.babestudios.stockmarket;

import android.support.test.espresso.contrib.RecyclerViewActions;
import android.support.test.rule.ActivityTestRule;

import com.babestudios.stockmarket.activities.mainpage.MainActivity;

import org.junit.Rule;
import org.junit.Test;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static com.babestudios.stockmarket.TestUtils.withRecyclerView;

public class MainActivityTest {

	@Rule
	public ActivityTestRule<MainActivity> activityTestRule =
			new ActivityTestRule<>(MainActivity.class);

	@Test
	public void isChartShowingOnMainListClick() {
		onView(withId(R.id.recycler_view)).perform(RecyclerViewActions.actionOnItemAtPosition(2, click()));
		onView(withId(R.id.chart1)).check(matches(isDisplayed()));
	}

	@Test
	public void isRankingShowingOnMenuClick() {
		onView(withId(R.id.action_ranking)).perform(click());
		onView(withRecyclerView(R.id.recycler_view).atPositionOnView(1, R.id.lbl_index)).check(matches(withText("FTSE100")));
	}

}