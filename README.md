# README #

Stock Market Research app.

### Features ###

* Using Dagger 2, RxJava, Wire (a protobuf library), Retrofit 2.
* Download intra day data from Yahoo Finance and display charts.

### Current features (will be deleted) ###

* Add announcement dates manually.
* Earning strategy category management.

### Features under development ###

* [Ranking] Ranking of FTSE stocks based on data from MySQL, updated with desktop application and data from Google Finance. Chart on click.
* [Earnings Strategy] Background service started by AlarmManager, based on Earnings date data from MySQL to look for favourable situations.
* [Earnings Strategy] Shows device notification when it finds attractive positions.
* [Earnings Strategy] Shows charts to help decision making.

### List of Retrofit network calls ###

* Yahoo Finance intra day data
* Google Finance daily data
* Proprietary
    * Ticker list (needed for Ranking).
    * Index constituent changes(needed for Ranking).
    * Earnings dates.

### Wire plugin ###

In order to use the app you need the wire plugin installed locally (or on the build server). This is not publicly available, so you have to build the plugin and publish it to your local maven repository. Download it from here:

https://github.com/square/wire-gradle-plugin

Alternative solution is to delete the plugin and usages (I will delete them later probably).